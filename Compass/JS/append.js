$(document).ready(function(){ 
    
    $("body").on("click","#addlevelbtn", function (){ 
         

        parent = document.querySelector('#levelssection');
        var index = parent.children.length;


        $("#levelssection").append(`
        <div class="accordionlevels accordion" id="accordionlevel${index}" name="niveles">      
        <!--Card de nivel-->
        <div class="col-md-12 p-0 courselevel card">
            <div class="col-md-12 form_field_outer p-0">
                <div class="row form_field_outer_row">
                
                    <div class="form-group col-md-12 pb-2 card-header" id="headinglvl${index}">
                        <div class="form-group col-md-12 ">
                                <h4 class="text-white" style="padding-right: 25px;">Nivel ${index+1} </h4>
                                
                        </div>
                    </div>

                    <div id="lvl${index}" class="collapse show col-md-12 p-0" aria-labelledby="headinglvl${index}" data-parent="#accordionlevels">
                        <div class="card-body">
                            <div class="form-group col-md-12 pt-3">
                                <div class="col-md-12 form_field_outer p-0">
                                    <div class="row form_field_outer_row pb-3">    
                                        <div class="form-group" style="display: flex; padding-left: 10px;">
                                            <h6 class="text-white m-0 p-0">Nombre del nivel:  </h6>                                 
                                        </div>    
                                        <div class="form-group col-md-5 p-0">
                                            <input name="level_title[]" id="lvl${index}level_title" type="text" class="form-control w_90" placeholder="nivel"  style="margin:0 10px;"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <section class="formsection pb-3 col-md-12 p-0">
                                <div class="col-md-12 form_field_outer">
                                    <div class="row form_field_outer_row pb-2">    
                                        <div class="form-group" style="display: flex; padding-left: 10px;">
                                            <h6 class="text-white m-0 p-0">Multimedia del nivel:  </h6>                                 
                                        </div>    
                                    </div>
                                </div>

                                <div class="col-md-11 form_field_outer m-auto p-0">    
                                    <div class="noth">
                                        <div class="accordion" id="accordionlvl${index}">
                                            <div class="card">
                                                <div class="card-header p-0 p-0" id="headingOne0">
                                                    <h5 class="mb-0">
                                                       <h3>Video</h3>
                                                    </h5>
                                                </div>
                                        
                                                <div id="collapseOne0" class="collapse show " aria-labelledby="headingOne0" data-parent="#accordionlvl${index}">
                                                    <div class="card-body">
                                                        <div class="levelcontent form-group col-md-12">  
                                                            <div class="levelmedia" id="lvl${index}vids">
                                                                <div class="form-group col-md-12">
                                                                        <input type="file" name="videolevel[]" id="lvl${index}videolevel"  accept="video/*">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="card">
                                                <div class="card-header p-0" id="headingTwo0">
                                                    <h5 class="mb-0">
                                                            <h3>Archivos</h3>
                                                    </h5>
                                                </div>
                                                <div class="collapse show" aria-labelledby="headingTwo0" data-parent="#accordionlvl${index}">
                                                    <div class="card-body">
                                                        <div class="levelcontent form-group col-md-12">
                                                            <div class="levelmedia" id="lvl${index}files" data-lvl="${index}">
                                                                
                                                            </div>
                                                            <div class="form-group col-md-12"> 
                                                                <div class="flexdiv" style="display: flex;">
                                                                    <button  type="button" class="btn btn-outline-lite py-0 add_new_frm_field_btn ml-auto addfilebtn" data-parent="#lvl${index}files"><i class="fas fa-plus add_icon ml-auto" ></i> Añadir Archivo</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                
                                            <div class="card">
                                                <div class="card-header p-0" id="headingThree">
                                                    <h5 class="mb-0">
                                                          <h3>PDFs</h3>
                                                    </h5>
                                                </div>
                                                <div class="collapse show" aria-labelledby="headingThree" data-parent="#accordionlvl${index}">
                                                    <div class="card-body">
                                                        <div class="levelcontent form-group col-md-12">
                                                            <div class="levelmedia" id="lvl${index}pdfs" data-lvl="${index}">  
                                                                
                                                                   
                                                                
                                                            </div>
                                                            <div class="form-group col-md-12"> 
                                                                <div class="flexdiv" style="display: flex;">
                                                                        <button  type="button" class="btn btn-outline-lite py-0 add_new_frm_field_btn ml-auto addpdfbtn" data-parent="#lvl${index}pdfs"><i class="fas fa-plus add_icon ml-auto" ></i> Añadir PDf</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                
                                            <div class="card">
                                                <div class="card-header p-0" id="headingFour">
                                                    <h5 class="mb-0">
                                                       <h3>Enlaces</h3>
                                                    </h5>
                                                </div>
                                                <div class="collapse show" aria-labelledby="headingFour" data-parent="#accordionlvl${index}">
                                                    <div class="card-body">
                                                        <div class="levelcontent form-group col-md-12">
                                                            <div class="levelmedia" id="lvl${index}links" data-lvl="${index}">
                                                                
                                                            </div>
                                                            <div class="form-group col-md-12"> 
                                                                <div class="flexdiv" style="display: flex;">
                                                                        <button type="button"  class="btn btn-outline-lite py-0 add_new_frm_field_btn ml-auto addlinkbtn"  data-parent="#lvl${index}links"><i class="fas fa-plus add_icon ml-auto" ></i> Añadir Enlace</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                
                                
                                            <div class="card">
                                                <div class="card-header p-0" id="headingFive">
                                                    <h5 class="mb-0">
                                                       <h3>Imágenes</h3>
                                                    </h5>
                                                </div>
                                                <div  class="collapse show card-body" aria-labelledby="headingFive" data-parent="#accordionlvl${index}">
                                                    <div id="lvl${index}imgs" data-lvl="${index}">
                                                       
                                                    </div>
                                                     <div class="form-group col-md-12"> 
                                                        <div class="flexdiv" style="display: flex;">
                                                            <button  type="button" class="btn btn-outline-lite py-0 add_new_frm_field_btn ml-auto addimgbtn"  data-parent="#lvl${index}imgs"><i class="fas fa-plus add_icon ml-auto" ></i> Añadir Imagen</button>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                            </div> 
                                
                                            <div class="card">
                                                <div class="card-header p-0" id="headingSix">
                                                    <h5 class="mb-0">
                                                          <h3>Texto</h3>
                                                    </h5>
                                                </div>
                                        
                                                <div class="collapse show" aria-labelledby="headingSix" data-parent="#accordionlvl${index}">
                                                    <div class="card-body">
                                                        
                                                                <div id="lvl${index}texts" data-lvl="${index}">
                                                                   
                                                                </div>
                                                                <div class="form-group col-md-12"> 
                                                                    <div class="flexdiv" style="display: flex;">
                                                                            <button  type="button" class="btn btn-outline-lite py-0 add_new_frm_field_btn ml-auto addtextbtn"  data-parent="#lvl${index}texts"><i class="fas fa-plus add_icon ml-auto" ></i> Añadir Texto</button>
                                                                    </div>
                                                                </div>
                                                            
                                                        
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div> 
                                </div>  
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>                     
    </div>
    `); 

        $(".form_field_outer").find(".remove_node_btn_frm_field:not(:first)").prop("disabled", false);
        $(".form_field_outer").find(".remove_node_btn_frm_field").first().prop("disabled", true); 
    }); 

    $("body").on("click",".addfilebtn", function (){ 
        
        var aux=document.querySelector(this.dataset.parent);
        var id = aux.children.length;
        var lvl= aux.dataset.lvl;
          
        $(this.dataset.parent).append(`
        <div class="separator">
            <div class="form-group col-md-12" style="display: flex;">
                <input type="file" name="file${lvl}[]" id ="lvl${lvl}file${id}" class="form-group col-md-6 p-0" >
                <div class="remove_node_btn_frm_field ml-auto col-md-6 p-0" style="display: inline-flex;">
                    <button  type="button" class="btn btn-outline-lite py-0   ml-auto" data-parent="#lvl${lvl}files"><i class="fa fa-trash add_icon ml-auto"></i>Eliminar</button>
                </div>
            </div>
        </div>
        
        
        `);
    });

    $("body").on("click",".addpdfbtn", function (){ 
        var aux=document.querySelector(this.dataset.parent);
        var id = aux.children.length;
        var lvl= aux.dataset.lvl;
        $(this.dataset.parent).append(`
        <div class="separator">
            <div class="form-group col-md-12" style="display: flex;">
                <input type="file" name="pdf${lvl}[]" id ="lvl${lvl}pdf${id}" class="form-group col-md-6 p-0" accept=".pdf">
                <div class="remove_node_btn_frm_field ml-auto col-md-6 p-0" style="display: inline-flex;">
                    <button  type="button" class="btn btn-outline-lite py-0   ml-auto" data-parent="#lvl${lvl}pdfs"><i class="fa fa-trash add_icon ml-auto"></i>Eliminar</button>
                </div>
            </div>
            
        </div>
        `);
    });

    $("body").on("click",".addlinkbtn", function (){ 
        var aux=document.querySelector(this.dataset.parent);
        var id = aux.children.length;
        var lvl= aux.dataset.lvl;
        $(this.dataset.parent).append(`  <div class="separator">
                                                                                                                                                                                       
        <div class="col-md-12 form_field_outer p-0 pb-3">
            <div class="row form_field_outer_row text-white">
                   
                <div class="row col-md-12 pb-2"style="display: inline-flex;">
                        <div class="form-group col-md-2">
                            <h6>Título:</h6>
                        </div>
                        <div class="form-group col-md-5">
                            <input type="text" class="form-control w_90" name = "title-enlace${lvl}[]" id="lvl${lvl}titlelink${id}" placeholder="Título descriptivo del enlace." />
                        </div>

                    <div class="remove_node_btn_frm_field ml-auto col-md-5 p-0" style="display: inline-flex;">
                        <button  type="button" class="btn btn-outline-lite py-0   ml-auto"><i class="fa fa-trash add_icon ml-auto"></i>Eliminar</button>
                    </div>
                </div>
                <div class="row col-md-12 pb-2">
                    <div class="form-group col-md-2">
                        <h6>Enlace:</h6>
                    </div>
                    <div class="form-group col-md-7">
                        <input type="text" class="form-control w_90" name="enlace${lvl}[]" id="lvl${lvl}enlace${id}"placeholder="Insertar dirección URL del enalce." />
                    </div>
                </div>
            </div>
        </div>
    </div>`);
    });

    $("body").on("click", ".addimgbtn", function (){
        var aux=document.querySelector(this.dataset.parent);
        var id = aux.children.length;
        var lvl= aux.dataset.lvl;
        $(this.dataset.parent).append(` 
        <div class="separator">
        <div class="col-md-12 form_field_outer p-0 pb-3 ">
            <div class="row form_field_outer_row">
                <div class="form-group col-md-12" style="display: inline-flex;">
                    <input type="file" name="img${lvl}[]" id="lvl${lvl}img${id}" class="col-md-7 p-0"  style="display: flex;" accept="image/*">
                    <div class="remove_node_btn_frm_field ml-auto col-md-5 p-0" style="display: inline-flex;">
                        <button  type="button" class="btn btn-outline-lite py-0   ml-auto"><i class="fa fa-trash add_icon ml-auto"></i>Eliminar</button>
                     </div>
                </div>
                <div class="form-group col-md-12 mt-1">
                    <textarea name="desc-img${lvl}[]" id="" cols="1" rows="1" class="form-control w_90" id="lvl${lvl}imgdescr${id}"style="resize: none;" placeholder="Descripción de la imagen."></textarea>
                </div>
            </div>
        </div>
    </div>`);
    });
    
    $("body").on("click", ".addtextbtn", function (){
        var aux=document.querySelector(this.dataset.parent);
        var id = aux.children.length;
        var lvl= aux.dataset.lvl;
        $(this.dataset.parent).append(` <div class="separator">
        <div class="col-md-12 form_field_outer p-0 pb-3">
            <div class="row form_field_outer_row text-white">
                <div class="row col-md-12 pb-2" style="display: inline-flex;">
                    <div class="form-group col-md-2">
                        <h6>Título:</h6>
                    </div>
                    <div class="form-group col-md-5">
                        <input type="text" class="form-control w_90" name = "title-texto${lvl}[]" id = "lvl${lvl}titletext${id}" placeholder="Título del texto" />
                    </div>
                    <div class="remove_node_btn_frm_field ml-auto col-md-5 p-0" style="display: inline-flex;">
                        <button  type="button" class="btn btn-outline-lite py-0   ml-auto"><i class="fa fa-trash add_icon ml-auto"></i>Eliminar</button>
                    </div>
                </div>
                <div class="form-group col-md-12 mt-1">
                    <textarea name="textbody${lvl}[]" id="lvl${lvl}textbody${id}" cols="1" rows="5" class="form-control w_90" placeholder="Ingrese su texto."></textarea>
                </div>
            </div>
        </div>
    </div>`);
    });


    $("body").on("click", "#aver", function (){
       

        
        let CourseBasicInfo = new FormData();
        CourseBasicInfo.append("sp_Titulo", document.getElementById("course_name").value);
        CourseBasicInfo.append("sp_Descripcion_C", document.getElementById("course_shortdesc").value);
        CourseBasicInfo.append("sp_Descripcion_L", document.getElementById("course_largedesc").value);
        CourseBasicInfo.append("sp_Costo", document.getElementById("course_costo").value);
        //CourseBasicInfo.append("sp_fk_Creador", $_SESSION['User']);
        
        $.ajax({
            url: "/7/Compass/course/insertcourse",
            type: "POST",
            data: CourseBasicInfo,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function(response){
              console.log(response);
              

                var category = document.querySelectorAll("li[class='select2-selection__choice']");

                category.forEach(
                    function(currentValue, indexlink, listObj) {
                    

                    let formCat = new FormData();
                    formCat.append("cat", category.item(indexlink).title);
                    
                    $.ajax({
                        url: "/7/Compass/course/insertcoursecat",
                        type: "POST",
                        data: formCat,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        success: function(response){
                        console.log(response);
                        },
                        error: function(response){
                        
                            console.log(response);
                        }

                    });

                });


                var levels = document.querySelectorAll("div[name='niveles']");
                //REGISTRAR CADA NIVEL
                levels.forEach(
                    function(currentValue, indexlvl, listObj) {
                        let insertlevel = new FormData();
                        
                        //levels.item(indexlvl).querySelectorAll("input[name='videolevel']").item(0).value;
                       // levels.item(indexlvl).querySelectorAll("input[name='videolevel']").item(0).files[0].name; 
                        insertlevel.append("opc", "1");
                        insertlevel.append("sp_Nombre",levels.item(indexlvl).querySelectorAll("input[name='level_title']").item(0).value);
                        insertlevel.append("sp_Video", "cambiameamediumblob");
                        
                        $.ajax({
                            url: "/7/Compass/course/insertcourselevel",
                            type: "POST",
                            data: insertlevel,
                            cache: false,
                            contentType: false,
                            processData: false,
                            dataType: "json",
                            success: function(response){
                                console.log(response);
                                //REGISTRAR MEDIA DE CADA NIVEL DESPUÉS DE CREARLO
                                var linkstitles =levels.item(indexlvl).querySelectorAll("input[name='title-enlace']");
                                var linksurls =levels.item(indexlvl).querySelectorAll("input[name='enlace']");

                                    linkstitles.forEach(function(currentValue, indexlink, listObj) {
                                    var linklevel = new FormData();
                                    linklevel.append("opc", "1");
                                    linklevel.append("sp_Nombre", linkstitles.item(indexlink).value);
                                    linklevel.append("sp_Video", linksurls.item(indexlink).value);

                                    $.ajax({
                                        url: "/7/Compass/course/insertlevellink",
                                        type: "POST",
                                        data: linklevel,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        dataType: "json",
                                        success: function(response){
                                        console.log(response);
                                        },
                                        error: function(response){
                                            console.log(response);
                                        }
                
                                    });
                                    console.log(linkstitles.item(indexlink).value);
                                    console.log(linksurls.item(indexlink).value);
                                });
                                

                            },
                            error: function(response){
                            
                                console.log(response);
                            }
    
                        });

                        
    
        
                        
        
                        
                    }
                    
        
        
                  );

            },
            error: function(response){
               
                console.log(response);
            }

        });

        
        
         /*console.log(levels);
       levels.forEach(
            function(currentValue, indexlvl, listObj) {
                console.log(levels.item(indexlvl).id);

                

                var linkstitles =levels.item(indexlvl).querySelectorAll("input[name='title-enlace']");
                var linksurls =levels.item(indexlvl).querySelectorAll("input[name='enlace']");
            
                linkstitles.forEach(
                    function(currentValue, indexlink, listObj) {
                        console.log(linkstitles.item(indexlink).value);
                        console.log(linksurls.item(indexlink).value);
                    });
            }
            


          );*/
        /*Array.prototype.forEach.call (x, function (node) {
           
            
           

            var y = node.querySelectorAll("input[name='title-enlace']");
            
            var y2 = node.querySelectorAll("input[name='enlace']");
            Array.prototype.forEach.call (y, function (node) {
                console.log(node);
                console.log(node.value);
                console.log(this);
                //console.log(y2.item(node.index).value);
            
            } );
        
        } );*/
        
    });



    $("#categories").select2({
        placeholder: "select category", //placeholder
        tags: true,
        tokenSeparators: ['/',',',';'," "]
    });
});