
$(document).ready(function(){
    
    $("#btn-Detalles").click(function(){
        $(".Comentarios").hide();
        $(".Detalles").show();
    });

    $("#btn-Comentarios").click(function(){
        $(".Detalles").hide();
        $(".Comentarios").show();
    });

    $(".contactoMsj").on("click", function(){
        $(".chat").hide("slow").show("slow");
    });

    $("#btn-like").click(function(){

        if($("#btn-dislike").hasClass("likedislike")){
            $("#btn-dislike").removeClass("likedislike");
        } 

        if($(this).hasClass("likedislike")){
            $(this).removeClass("likedislike");                              
        }
        else{
            $(this).addClass("likedislike");
        }
      
    });

    $("#btn-dislike").click(function(){

        if($("#btn-like").hasClass("likedislike")){
            $("#btn-like").removeClass("likedislike");                              
        }

        if($(this).hasClass("likedislike")){
            $(this).removeClass("likedislike");
        }
        else{
            $(this).addClass("likedislike");
        }
            
    });

    $("#EnviarCom").click(function(){
        if($(".tx-Comentario").val().length === 0)
        {
            $(".error").removeClass("d-none");
        }
        else
            $(".error").addClass("d-none");
    });

    $("#paypal").click(function(){
        $(".metPago").text("Paypal") ;
        $(".btn-visa").addClass("d-none");
        $(".btn-paypal").removeClass("d-none");
    });

    $("#visa").click(function(){
        $(".metPago").text("Visa") ;
        $(".btn-visa").removeClass("d-none");
        $(".btn-paypal").addClass("d-none");
    });
    
    $("#btn-CerrarSesion").click(function(){
        window.location.href = "../pages/login.html"
    });

    $(".btn-VerCurso").click(function(){
        $(".alert").remove("div");

        $(".verCursoAlert").append(' <div class="alert alert-warning alert-dismissible fade show" role="alert">'
        + '<strong>Inicie Sesión!</strong> Para ver los detalles del curso Inicie Sesión o en caso de '
        + 'no tener, Crea una Cuenta.'
        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
        + '<span aria-hidden="true">&times;</span></button></div>');
    });

    $(".a-ComprarNivel").click(function(){
        var nivel = $(this).text();
        $(".modal-body").text(nivel);
    });

    $("#btn-PagarNivel").click(function(){
        window.location.href = "../pages/pago.html"
    });

});

function validarImagen(obj){
    var uploadFile = obj.files[0];

    if (!(/\.(jpg|png|jpeg|jfif)$/i).test(uploadFile.name)) {
        alert('El archivo a adjuntar no es una imagen');
    }
    else
    {
        if (uploadFile.size > 160000000 )
        {
            alert('El peso de la imagen no puede exceder los 16mb');
        }
        else
        {
            const ObtImg = document.querySelector("#upload-photo"),
            InsertImg = document.querySelector(".profilepicture");

            const Img = ObtImg.files;
            InsertImg.src = URL.createObjectURL(Img[0]);
           
        }
    }               
}