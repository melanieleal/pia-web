$().ready(function() {
    $("#cardpaymentcontainer").card({
    // a selector or DOM element for the container
    // where you want the card to appear
    container: '.card-wrapper', // *required*
    formatting: true,
    
     // selectors
     formSelectors: {
        nameInput: 'input[name="firstname"], input[name="lastname"]'
    }
    // all of the other options from above
});
});