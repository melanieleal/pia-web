$().ready(function() {

    $.validator.setDefaults({
           highlight: function(element){
                   $(element).addClass('error');
                
           },
           unhighlight: function(element){
                   $(element).removeClass('error');
           },
      });     
   
   $("#userform").validate({
   
            rules: {
                username:{
                    required:true,
                    spcharvalidate: true,
                    minlength: 1,
                    maxlength: 30
                },
                Nombre:{
                    required:true,
                    spcharvalidate: true,
                    minlength: 1,
                    maxlength: 30
                },
                Apellido:{
                    required:true,
                    spcharvalidate: true,
                    minlength: 1,
                    maxlength: 30
                },
                password:{
                    required:true,
                    hasminus: {
                            param:true,
                            depends: function (element) {
                                return $('#password').val().length>0;}
                            },
                    hasmayus: {param:true,
                            depends: function (element) {
                                return $('#password').val().length>0;}},
                    hasnumber: {param:true,
                            depends: function (element) {
                                return $('#password').val().length>0;}},
                    hascharsp: {param:true,
                            depends: function (element) {
                                return $('#password').val().length>0;}},
                    minlength:{param:8,
                            depends: function (element) {
                                return $('#password').val().length>0;}},
                },
                passwordconfirm:{
                    equalTo: '#password',
                },
                email:{
                    required:true,
                    email:true
                },
                Nacimiento:{
                    required:true
                }

            },
            messages: {
                username:{
                    required:"! Ingrese un usuario",
                    spcharvalidate: "! Solo puede ingresar letras",
                    minlength: "! Usuario demasiado corto",
                },
                Nombre:{
                        required: "! Ingresar un nombre",
                        spcharvalidate: "! El nombre no puede contener caracteres especiales o números",
                        minlength: "! Nombre demasiado corto"
                },
                Apellido:{
                        required: "! Ingresar un apellido",
                        spcharvalidate: "! El apellido no puede contener caracteres especiales o números",
                        minlength: "! Apellido demasiado corto"
                },
                password:{
                        required:"! Ingrese la contraseña",
                        hasminus: "! La contraseña requiere una letra minúscula al menos.",
                        hasmayus: "! La contraseña requiere una letra mayúscula al menos.",
                        hasnumber: "! La contraseña requiere un número al menos.",
                        hascharsp: "! La contraseña requiere al menos un caracter especial (¡”#$%&/=’?¡¿:;,.-_+*{][}).",
                        minlength: "! La contraseña debe tener por lo menos 8 caracteres."
                },
                passwordconfirm:{
                        equalTo: "Las contraseñas no coinciden."
                },
                email:{
                    required:"! Ingrese un email",
                    email:"! Debe de tener el formato email ejemplo@ejemplo.com"
                },
                Nacimiento:{
                    required:"! Ingrese su fecha de nacimiento"
                  
                }
    
            }
    });


    
    $.validator.addMethod("spcharvalidate",
    function(value, element) {
            return /^[a-zA-Z]/.test(value);
    });
    
    
    $.validator.addMethod("hasmayus",
    function(value, element) {
            return /[A-Z]/.test(value);
    });
   
    $.validator.addMethod("hasminus",
    function(value, element) {
            return /[a-z]/.test(value);
    });
   
    $.validator.addMethod("hascharsp",
    function(value, element) {
            return /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(value);
    });
   
    $.validator.addMethod("hasnumber",
    function(value, element) {
            return /[0-9]/.test(value);
    });
});