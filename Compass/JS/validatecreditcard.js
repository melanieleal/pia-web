$().ready(function() {
    $("#cardpayment").validate({
        rules: {
            number:{
                     required:true,
                     minlength: 16
             },
             lastname:{
                    required:true,
                    lettersonly: true,
                    minlength: 1
            },
            firstname:{
                    required:true,
                    lettersonly: true,
                    minlength: 1
            },
            expiry:{
                    required:true,
                    minlength: 5
            },
            cvc:{
                    required:true,
                    minlength: 3
            }
        },
        messages: {
             
            number:{
                     required: "Ingresar información",
                     minlength: "Debe contener 16 caracteres"
             }, 
             firstname:{
                required: "Ingresar información",
                lettersonly: "Ingreser solo caracteres alfabéticos",
                minlength: "Denasiado corto"
            },
             lastname:{
                     required: "Ingresar información",
                     lettersonly: "Ingreser solo caracteres alfabéticos",
                     minlength: "Denasiado corto"
             },
             expiry:{
                required: "Ingresar información",
                minlength: "Denasiado corto"
            },
            cvc:{
               required: "Ingresar información",
               minlength: "Denasiado corto"
            }

        }
});


$.validator.addMethod("spcharvalidate",
function(value, element) {
        return /^[a-zA-Z0-9]/.test(value);
});

$.validator.addMethod("lettersonly", 
function(value, element) {
    return /^[a-zA-Z]/.test(value);
  }); 

$.validator.addMethod("hasmayus",
function(value, element) {
        return /^[A-Z]/.test(value);
});

$.validator.addMethod("hasminus",
function(value, element) {
        return /^[a-z]/.test(value);
});

$.validator.addMethod("hascharsp",
function(value, element) {
        return /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/.test(value);
});

$.validator.addMethod("hasnumber",
function(value, element) {
        return /^[a-zA-Z0-9]/.test(value);
});

jQuery.validator.addMethod("domain", function(value, element) {
 return this.optional(element) || /^http:\/\/mycorporatedomain.com/.test(value);
}, "Please specify the correct domain for your documents");


});


