$().ready(function() {
        $.validator.setDefaults({
        highlight: function(element){
                $(element).addClass('has-error');
        },
        unhighlight: function(element){
                $(element).removeClass('has-error');
        },
   });     

   $("#create_course_form").validate({
           rules: {
                course_name: {
                        required: true,
                        minlength: 4,
                        maxlength: 50
                },
                level_title:{
                        required: true,
                        minlength: 5,
                        maxlength: 50
                },
                lvlname:{
                        required: true,
                        spcharvalidate: true,
                        maxlength: 50
                },
                vidtitle:{
                        required: true,
                        spcharvalidate: true,
                        maxlength: 50
                },
                course_cat:{
                        required: true
                },
                costocourse:{
                        required:true,
                        numvalidate:true
                },
                costo_nivel:{
                        numvalidate:true
                }
        
           },
           messages: {
                course_name: {
                        required: "No dejar este campo vacío.",
                        minlength: "Este campo necesita mínimo 4 caracteres.",
                        maxlength: "El límite de este campo es 50 caracteres."
                },
                level_title: {
                        required: "El nivel debe tener un título",
                        minlength: "Este campo necesita mínimo 5 caracteres.",
                        maxlength: "El límite de este campo es 50 caracteres."
                },

                lvlname:{
                        required: "El nivel debe tener un título.",
                        spcharvalidate: "No puede contener caracteres especiales.",
                        maxlength: "El título tiene un máximo de 50 caracteres."
                },
                vidtitle:{
                        required: "El video debe tener un título.",
                        spcharvalidate: "No puede contener caracteres especiales.",
                        maxlength: "El título tiene un máximo de 50 caracteres."
                },
                course_cat:{
                        required: "La categoría es obligatoria."
                },
                costocourse:{
                        required:"Ingrese el costo del curso completo",
                        numvalidate:"Solo Ingrese números"
                },
                costo_nivel:{
                        numvalidate:"Solo Ingrese números"
                }

           }
   });



 $.validator.addMethod("spcharvalidate",
 function(value, element) {
         return /^[a-zA-Z0-9]/.test(value);
 });

 $.validator.addMethod("numvalidate",
 function(value, element) {
         return /^[0-9]/.test(value);
 });


});


