$().ready(function() {
     $("#loginform").validate({
         rules: {
            username:{
                      required:true,
                      spcharvalidate: true
              },
            password:{
                    required:true,
                    spcharvalidate: true,
                    minlength: 8
            }
         },
         messages: {
              
              username:{
                      required: "! Ingresar usuario",
                      spcharvalidate: "! El usuario no puede contener caracteres especiales"
              },
              password:{
                      required: "! Ingresar contraseña",
                      spcharvalidate: "! El usuario no puede contener caracteres especiales",
                      minlength: "! La contraseña debe contener mínimo 8 caracteres."
              }
 
         }
 });
 
 
 $.validator.addMethod("spcharvalidate",
 function(value, element) {
         return /^[a-zA-Z0-9]/.test(value);
 });


 $.validator.addMethod("hasmayus",
 function(value, element) {
         return /^[A-Z]/.test(value);
 });

 $.validator.addMethod("hasminus",
 function(value, element) {
         return /^[a-z]/.test(value);
 });

 $.validator.addMethod("hascharsp",
 function(value, element) {
         return /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/.test(value);
 });

 $.validator.addMethod("hasnumber",
 function(value, element) {
         return /^[a-zA-Z0-9]/.test(value);
 });
 
 jQuery.validator.addMethod("domain", function(value, element) {
  return this.optional(element) || /^http:\/\/mycorporatedomain.com/.test(value);
}, "Please specify the correct domain for your documents");

 
 });
 
 
 