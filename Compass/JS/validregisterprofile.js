
$().ready(function() {

 $.validator.setDefaults({
        highlight: function(element){
                $(element).addClass('has-error');
        },
        unhighlight: function(element){
                $(element).removeClass('has-error');
        },
   });     
function passwordisntblank() {
        return $('#passwordE').val().length > 0;
    }

$("#edit-user").validate({

         rules: {
                NombreE:{
                      required:true,
                      spcharvalidate: true,
                      minlength: 1,
                      maxlength: 30
                },
                ApellidoE:{
                        required:true,
                        spcharvalidate: true,
                        minlength: 1,
                        maxlength: 30
                },
                passwordE:{
                        hasminus: {
                                param:true,
                                depends: function (element) {
                                    return $('#passwordE').val().length>0;}
                                 },
                        hasmayus: {param:true,
                                depends: function (element) {
                                    return $('#passwordE').val().length>0;}},
                        hasnumber: {param:true,
                                depends: function (element) {
                                    return $('#passwordE').val().length>0;}},
                        hascharsp: {param:true,
                                depends: function (element) {
                                    return $('#passwordE').val().length>0;}},
                        minlength:{param:8,
                                depends: function (element) {
                                    return $('#passwordE').val().length>0;}},
                },
                passwordA:{
                        required:true,
                        hasminus: {
                                param:true,
                                depends: function (element) {
                                    return $('#passwordA').val().length>0;}
                                 },
                        hasmayus: {param:true,
                                depends: function (element) {
                                    return $('#passwordA').val().length>0;}},
                        hasnumber: {param:true,
                                depends: function (element) {
                                    return $('#passwordA').val().length>0;}},
                        hascharsp: {param:true,
                                depends: function (element) {
                                    return $('#passwordA').val().length>0;}},
                        minlength:{param:8,
                                depends: function (element) {
                                    return $('#passwordA').val().length>0;}},
                },
                generoE:{
                        required: true
                },
                passwordconfirmE:{
                        equalTo: '#passwordE',
                },
                NacimientoE:{
                        required:true
                }
         },
         messages: {
              
                NombreE:{
                        required: "Ingresar un nombre",
                        spcharvalidate: "El nombre no puede contener caracteres especiales o números",
                        minlength: "Nombre demasiado corto",
                        maxlength: "No ingrese más de 30 caracteres"
                },
                ApellidoE:{
                        required: "Ingresar un apellido",
                        spcharvalidate: "El apellido no puede contener caracteres especiales o números",
                        minlength: "Apellido demasiado corto",
                        maxlength: "No ingrese más de 30 caracteres"
                },
                passwordE:{
                       
                        hasminus: "La contraseña requiere una letra minúscula al menos.",
                        hasmayus: "La contraseña requiere una letra mayúscula al menos.",
                        hasnumber: "La contraseña requiere un número al menos.",
                        hascharsp: "La contraseña requiere un caracter especial al menos.",
                        minlength: "La contraseña debe tener por lo menos 8 caracteres."
                },
                generoE:{
                        required: "Debe seleccionar una opción."
                },
                passwordconfirmE:{
                        equalTo: "Las contraseñas no coinciden."
                },
                NacimientoE:{
                        required: "Ingrese su fecha de nacimiento"
                }
 
         }
 });
 
 
 $.validator.addMethod("spcharvalidate",
 function(value, element) {
         return /^[a-zA-Z]/.test(value);
 });
 
 
 $.validator.addMethod("hasmayus",
 function(value, element) {
         return /[A-Z]/.test(value);
 });

 $.validator.addMethod("hasminus",
 function(value, element) {
         return /[a-z]/.test(value);
 });

 $.validator.addMethod("hascharsp",
 function(value, element) {
         return /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(value);
 });

 $.validator.addMethod("hasnumber",
 function(value, element) {
         return /[0-9]/.test(value);
 });
 
 
 });
 
 