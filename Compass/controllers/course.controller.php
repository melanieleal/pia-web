<?php

require_once "controllers/template.controller.php";
require_once "models/UserModel.php";

require_once "models/CourseModel.php";
require_once "helpers/action.php";

class CourseController extends Controller {
    //CONTROLLER ROUTE
    public const ROUTE = "course";

    //ACTIONS ROUTING
    
    public const USER_Index = "index";
    public const USER_COURSE = "show";
    public const INSERT = "create";
    public const SUBMIT = "submit";
    public const INSERTCOURSE = "insertcourse";
    public const COURSECATEGORY = "insertcoursecat";
    public const INSERTCOUSERLEVEL = "insertcourselevel";
    public const INSERTLEVELLINK = "insertlevellink";
    public const INSERTPOST= "submitcourse";
    public const SUBMITREVIEW= "submitreview";
    public const BUYCOURSE= "buycourse";
    public const PAYCOURSE= "paycourse";
    public const DOWNLOAD= "download";
    public const DELETE = "suspendercurso";
    public const EDIT = "edit";
    public $actions;

    public function __construct(){
        //parent::__construct($pathName);
        //self::USER_COURSE => new Action("Index", "IndexPost"),
        $this->actions = array(
                                self::USER_Index => new Action("Index", "IndexPost"),
                                self::USER_COURSE => new Action("showCourse", "showCourse"),
                                self::INSERT => new Action("createCourse", "pageCreateCourse"),
                                self::SUBMIT => new Action("GetSubmitCourse", "PostSubmitCourse"),
                                self::INSERTCOURSE => new Action("InsertCourse", "InsertCourse"),
                                self::COURSECATEGORY => new Action("InsertCourseCat", "InsertCourseCat"),
                                self::INSERTCOUSERLEVEL => new Action("insertcourselevel", "insertcourselevel"),
                                self::INSERTLEVELLINK => new Action("insertcourselink", "insertcourselink"),
                                self::INSERTPOST => new Action("insertpost", "insertpost"),
                                self::SUBMITREVIEW => new Action("submitreview", "submitreview"),
                                self::BUYCOURSE => new Action("buycourse", "buycourse"),
                                self::PAYCOURSE => new Action("sp_buycourseM", "sp_buycourseM"),
                                self::DOWNLOAD => new Action("download", "download"),
                                self::DELETE => new Action("deleteCourse", "deleteCourse"),
                                self::EDIT => new Action("EditCourse", "EditCoursePost"),
                                "ajax" => new Action(null, "Ajax"));
    }

    public function ShowContent($paths) {
        //Determine wich method call TO A SPECIFIC ACTION SENDED IT IN THE URL
        Action::ValidateActionsPath($paths, $this);
    }

    public function Index($paths){  
        if (isset($_SESSION["User"])){  
            if(isset($paths[2])){
                $inscrito= CourseModel::getCourseInfo("1",$paths[2],$_SESSION["User"], 0);
                $cursoinfo= CourseModel::getCourseInfo("2",$paths[2],$_SESSION["User"], 0);
                $niveles= CourseModel::getCourseInfo("3",$paths[2],$_SESSION["User"],0);
                if($inscrito.["AlumnoInscrito"] == 1){
                    if(isset($paths[3])){
                    $selected= CourseModel::getCourseInfo("3IN",$paths[2],$_SESSION["User"],$paths[3]);}
                    else{ $selected= CourseModel::getCourseInfo("3IN",$paths[2],$_SESSION["User"],1);}
                    
                }
                $reviews= CourseModel::getCourseInfo("4",$paths[2],$_SESSION["User"],0);

                

                include "views/courses/courseview.php";

            } else{
                include "views/error.php";
            } 
        }
        else{
            header("location: /7/Compass/login");
        }
        
    }

    public function showCourse($paths){  
        if (isset($_SESSION["User"])){  
            if(isset($paths[2])){
                $inscrito= CourseModel::getCourseInfo("1",$paths[2],$_SESSION["User"], 0);
                $cursoinfo= CourseModel::getCourseInfo("2",$paths[2],$_SESSION["User"], 0);
                $categoriescourse= CourseModel::getCoursecat($paths[2]);
                $niveles= CourseModel::getCourseInfo("3",$paths[2],$_SESSION["User"],0);
                $hasreviewed= UserModel::hasReviewedM($_SESSION["User"], $paths[2]);
                $aux = $inscrito[0]["AlumnoInscrito"];
                if($aux == 1){

                    $update = new CourseModel();
                    $update->updateAlumInfo('UVisita', $paths[2],$_SESSION["User"]);          
                    
                    $progreso = new UserModel();
                    if(isset($paths[3]))
                    {
                        $selected= CourseModel::getCourseInfo("3IN",$paths[2],$_SESSION["User"],$paths[3]-1); 
                        $progreso->updateProgreso($paths[3], $paths[2],$_SESSION["User"]);
                    }

                    else{ 
                        $selected= CourseModel::getCourseInfo("3IN",$paths[2],$_SESSION["User"],0);
                        $progreso->updateProgreso(1, $paths[2],$_SESSION["User"]);
                    }
                    $lvlfiles= CourseModel::getLevelMultimedia("f", $selected[0]["ID_Nivel"]);
                    $lvlpdfs= CourseModel::getLevelMultimedia("pdf", $selected[0]["ID_Nivel"]);
                    $lvlurls= CourseModel::getLevelMultimedia("url", $selected[0]["ID_Nivel"]); 
                    $lvlimgs= CourseModel::getLevelMultimedia("img", $selected[0]["ID_Nivel"]);
                    $lvltxts= CourseModel::getLevelMultimedia("txt", $selected[0]["ID_Nivel"]);      
                    }

                $reviews= CourseModel::getCourseInfo("4",$paths[2],$_SESSION["User"],0);

                include "views/courses/courseview.php";

            } else{
                include "views/error.php";
            } 
        }
        else{
            header("location: /7/Compass/login");
        }
        
    }

    public function pageCreateCourse(){ 
        $categoriessel = CourseModel::getCategories();
        include "views/courses/createcourse.php";
    }

    public function createCourse(){

        $categoriessel = CourseModel::getCategories();
        include "views/courses/createcourse.php";
        $course = new CourseModel();
    }

    public function PostSubmitCourse(){
        $a=$_POST;
        include "views/courses/createcourse.php";
        $course = new CourseModel();
    }

    public function GetSubmitCourse(){

        include "views/courses/createcourse.php";
        $course = new CourseModel();
    }


    public function InsertCourse(){
        
        $sp_Titulo = $_POST["sp_Titulo"];
        $sp_Descripcion_C = $_POST["sp_Descripcion_C"];
        $sp_Descripcion_L = $_POST["sp_Descripcion_L"];
        $sp_Costo = $_POST["sp_Costo"];
        /*$sp_Portada = $_POST["sp_Portada"];
        $sp_Video_Presentacion = $_POST["sp_Video_Presentacion"];*/
        $sp_fk_Creador = $_SESSION['User'];
        
        //CourseModel::InsertCourseM(1, $sp_Titulo, $sp_Descripcion_C, $sp_Descripcion_L, $sp_Costo, $sp_Portada, $sp_Video_Presentacion, $sp_fk_Creador);
        CourseModel::InsertCourseM(1, $sp_Titulo, $sp_Descripcion_C, $sp_Descripcion_L, $sp_Costo, 0, 0, $sp_fk_Creador);

        ob_end_clean();
        $a=json_encode($sp_Titulo);
        echo $a;
        die();
        
    }

    public function InsertCourseCat($str){
      
       
        CourseModel::sp_cursocategoriaM("insert", $str);
        ob_end_clean();
        $a=json_encode($str);
        echo $a;
        die();
        
    }

    public function insertcourselevel(){
        $opc = $_POST["opc"];
        $sp_Nombre = $_POST["sp_Nombre"];
        $sp_Video = $_POST["sp_Video"];
       
       
        $aa = $_POST["level_title"];
        $bb = $_POST["level_linkurl"];

        $cc = $_POST["level_linktitle"];
        $dd = $_POST["level_linkurl"];
        



        foreach($aa as $clave=>$valor){

            //CourseModel::sp_createlevelM(1, $valor, $videos[$clave]);

            $cc = $_POST["level_linktitle$clave"];
            $dd = $_POST["level_linkurl$clave"];
            foreach($cc as $clavej=>$valorj){
                CourseModel::sp_createmediaM(1, $valorj, $dd[$clavej]);
            }

        }

        CourseModel::sp_createlevelM($opc, $sp_Nombre, $sp_Video);
        ob_end_clean();
        $a=json_encode($sp_Nombre);
        echo $a;
        die();
        
    }


    //CreateCourse
    public function insertpost(){
        $coursename= $_POST["course_name"];
        $coursedescrshort = $_POST["course_shortdesc"];
        $coursedescrlarge = $_POST["course_largedesc"];
        $coursecosto = $_POST["costocourse"];
        $courseimage = $_FILES["imagecourse"];
        $coursevideo = $_FILES["videoP"];
        $sp_fk_Creador = $_SESSION['User'];


        $leveltitles = $_POST["level_title"];
        $levelvideos = $_FILES["videolevel"];
        $categorias=$_POST["categorias"];

                                                                                                    //img y video
        CourseModel::InsertCourseM(1, $coursename, $coursedescrshort, $coursedescrlarge, $coursecosto, file_get_contents($_FILES["imagecourse"]["tmp_name"]), file_get_contents($_FILES["videoP"]["tmp_name"]), $sp_fk_Creador);
        //to-do: get categorías

        
        foreach($categorias as $clavecat=>$valorcat){
            CourseModel::sp_cursocategoriaM("insert", $valorcat);
            $valorcat;
        }
        foreach($leveltitles as $clave=>$valor){

            CourseModel::sp_createlevelM(1, $valor, file_get_contents($levelvideos["tmp_name"][$clave]));
            
            if(isset($_FILES["file$clave"])){
                $fileslvl = $_FILES["file$clave"]["tmp_name"];

               foreach($fileslvl as $clavei=>$valori){
                  

                    CourseModel::sp_createmedia_filesM("1", file_get_contents($valori),  $_FILES["file$clave"]["type"][$clavei], $_FILES["file$clave"]["name"][$clavei]);


                }
            }

            if(isset($_FILES["pdf$clave"])){
                
            
                $pdfslvl = $_FILES["pdf$clave"]["tmp_name"];
                
                foreach($pdfslvl as $clavej=>$valorj){
                    CourseModel::sp_createmedia_filesM("1", file_get_contents($valorj), $_FILES["pdf$clave"]["type"][$clavej], $_FILES["pdf$clave"]["name"][$clavej]);
                }
            }

            if(isset($_POST["title-enlace$clave"])){
                $linktitleslvl = $_POST["title-enlace$clave"];
                $linksurllvl = $_POST["enlace$clave"];
                // $fileslvl = $_POST["level_linkurl$clave"];
                foreach($linktitleslvl as $clavek=>$valork){
                    CourseModel:: sp_createmediaM("1", $valork, $linksurllvl[$clavek]) ;
                }
            }

            if(isset($_FILES["img$clave"])){
              $imagefilelvl = $_FILES["img$clave"]["tmp_name"];
              $imagedesclvl = $_POST["desc-img$clave"];
             
              // $fileslvl = $_POST["level_linkurl$clave"];
               foreach($imagefilelvl as $clavel=>$valorl){
                   CourseModel:: sp_media_imageM("1", file_get_contents($valorl), $imagedesclvl[$clavel]);
               }
            }
            if(isset($_POST["title-texto$clave"])){
                $textitlelvl = $_POST["title-texto$clave"];
                $textbodylvl = $_POST["textbody$clave"];
               
                // $fileslvl = $_POST["level_linkurl$clave"];
                 foreach($textitlelvl as $clavem=>$valorm){
                     CourseModel:: sp_media_textM("1", $valorm, $textbodylvl[$clavem]);
                 }
            }
        }
        
        include "views/courses/createcourse.php";
    }

    public function insertcourselink(){
        $opc = $_POST["opc"];
        $sp_Nombre = $_POST["sp_Nombre"];
        $sp_Video = $_POST["sp_Video"];
        
        CourseModel::sp_createmediaM($opc, $sp_Nombre, $sp_Video);
        ob_end_clean();
        $a=json_encode($sp_Nombre);
        echo $a;
        die();
        
    }


    public function submitreview(){
           
        CourseModel::InsertCourseRewviewM($_POST["reviewbody"], $_POST["score"], $_SESSION["User"], $_POST["course"]);
        header("location: /7/Compass/course/show/".$_POST['course']."/1");
        
    }

    public function buycourse($paths){
        if(isset($_POST["cursoid"])&&isset($_SESSION["User"])){
            $cursoinfo= CourseModel::getCourseInfo("2", $_POST["cursoid"],$_SESSION["User"], 0);
        
            include "views/courses/paycourse.php";
        }
       else
       include "views/error.php";
    }

    public function sp_buycourseM($paths){
        $formap=0;
        if(isset($_POST["formapago"])){
            $formap = $_POST["formapago"];
        }
        $cursoinfo= CourseModel::sp_buycourseM($_SESSION["User"], $_POST["curso"], (int)$formap);
       
    }
    
    public function download($paths){
       
        if(isset($_POST["file"])){
            $result= CourseModel::download($_POST["file"]);
            /*$aux=$_POST["file"];
            header('Content-Type: application/octet-stream');
            header("Content-length: $result");
            header('Content-Disposition: attachment; filename='.$aux.'');
            ob_clean();
            flush();
            echo $result;*/


            header('Content-Description: File Transfer');
            header('Content-Type:'.$result[0]["tipo"]);
            header('Content-Disposition: attachment; filename='.$result[0]["filename"]);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            //header('Content-Length: ' .filesize($fichero));
            ob_clean();
            flush();
            echo $result[0]["file_"];
            exit;



            
        }
    }

    public function deleteCourse($paths){
        if(isset($paths[2])){
            CourseModel::suspenderCourse($paths[2]);
            $coursesby= CourseModel::GetCourseUser("E",$_SESSION["User"]);
            $ventas = UserModel::getHistorialVentas($_SESSION["User"]);
            header("location: /7/Compass/usuario/profile/".$_SESSION["User"]);
        }
        else
            include "views/error.php";
    }



    public function EditCourse($paths){

        if(isset($paths[2])){
            
            $cursoinfo= CourseModel::getCourseInfo("2",$paths[2],$_SESSION["User"], 0);
            //$categoriescourse= CourseModel::getCoursecat($paths[2]);
            $niveles= CourseModel::getCourseInfo("3ed",$paths[2],$_SESSION["User"],0);


            include "views/users/EditCourse.php";

        } else{
            include "views/error.php";
        }

    }

    public function EditCoursePost($paths){
        
        $idToEdit = $_POST["id_course"];
        $usuario=$_SESSION["User"];
        
        $leveltitles = $_POST["level_title"];
        $levelids = $_POST["level_id"];
        
        $photoaux=base64_decode($_POST["photoori"]);
        if($_FILES["photo"]["tmp_name"]!=""){
            $photoaux=file_get_contents($_FILES["photo"]["tmp_name"]);
        }


        $videoPaux=base64_decode($_POST["videoPori"]);
        if($_FILES["videoP"]["tmp_name"]!=""){
            $videoPaux=file_get_contents($_FILES["videoP"]["tmp_name"]);
        }

        CourseModel::UpdateCourse($usuario, $_POST["id_course"], $_POST["course_name"],$_POST["course_shortdesc"],$_POST["course_largedesc"],$_POST["costocourse"],$photoaux,$videoPaux);
            
        foreach($leveltitles as $clavelvl=>$valor){
            $videoaux=base64_decode($_POST["videolevelOr"][$clavelvl]);
            if($_FILES["videolevel"]["tmp_name"][$clavelvl]!=""){
                $videoaux=file_get_contents($_FILES["videolevel"]["tmp_name"][$clavelvl]);
            }
            
            CourseModel::UpdateCourseLevel($usuario, $_POST["level_id"][$clavelvl], $_POST["level_title"][$clavelvl],$videoaux);
            
        }

        if(true){
            ob_end_clean();
            header("Location: /7/Compass/".self::ROUTE."/".self::USER_COURSE."/".$idToEdit);
            die();
        } else {
            include "views/error.php";
        }
        



    }



}
