<?php

require_once "controllers/template.controller.php";
require_once "models/UserModel.php";
require_once "helpers/action.php";

class LoginController extends Controller {
    //CONTROLLER ROUTE
    public const ROUTE = "login";

    //ACTIONS ROUTING
    
    public const LOGIN = "index";
    public const LOGOUT = "logout";


    public $actions;

    public function __construct(){
        //parent::__construct($pathName);

        $this->actions = array(self::LOGIN => new Action("Index", "IndexPost"),
                                self::LOGOUT => new Action("LogOut", null),
                                "ajax" => new Action(null, "Ajax"));
    }

    public function ShowContent($paths) {
        //Determine wich method call TO A SPECIFIC ACTION SENDED IT IN THE URL
        Action::ValidateActionsPath($paths, $this);
    }


    public function Index($paths){       
       include "views/login/index.php";
    }

    
    public function IndexPost($paths) {
        
        $user = new UserModel();

        $user->setId($_POST["username"]);
        $user->setPass($_POST["password"]);
        $a = $user->getId();
        $result = $user->UserLogin();

        $args = array( "result" => $result);
        if($result == 1){

            session_start();
            $_SESSION['User']= $a;
            ob_end_clean();

            
            header("location: /7/Compass/");
        }
        else{
            include "views/login/index.php";
        }         
       
    }

    public function LogOut() {
        session_unset();
        session_destroy();
        ob_end_clean();

        header("location: /7/Compass/");   
    }

    public function Ajax($paths){
        ob_end_clean();

        
        $name = $_POST["name"];
        $age = $_POST["age"];

        $respuesta = array("prueba" => "hola");
        echo json_encode($_POST);
        die();
    }

}
