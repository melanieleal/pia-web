<?php

require_once "controllers/template.controller.php";
require_once "models/MessageModel.php";
require_once "models/UserModel.php";


class MessageController extends Controller {
    // CONTROLLER ROUTE
    public const ROUTE = "message";

    //ACTIONS ROUTE
    public const INDEX = "index";
    public const CHAT = "msj";
    public const MSJ = "sent";

    public $actions;
    
    public function __construct(){
        //parent::__construct($pathName);
        $this->actions = array(self::INDEX => new Action("Index", null),
                                self::CHAT => new Action( "MsgUser", "MsgUser"),
                                self::MSJ => new Action(null, "sentMsj"));

    }

    public function ShowContent($paths) {
        //Determine wich method call
       Action::ValidateActionsPath($paths, $this);

    }


    public function Index($paths) {

        include "views/message/message.php";
    }

    public function MsgUser($paths) {
        if (isset($_SESSION["User"])){  
            if(isset($paths[2])){
                
                $user = UserModel::GetUser($paths[2]);

                $msj = MessageModel::getMsg($paths[2], $_SESSION['User']);

                $preview = MessageModel::getPreviewAllMsg($_SESSION['User']);

                include "views/message/message.php";

            } else{
                //include "views/error.php";
                $ultimoMsj = MessageModel::getUltimoChat($_SESSION['User']);
                if($ultimoMsj == true){
                    $user = UserModel::GetUser($ultimoMsj->getDestiny());
                    $msj = MessageModel::getMsg($ultimoMsj->getDestiny(), $_SESSION['User']);
                    $preview = MessageModel::getPreviewAllMsg($_SESSION['User']);
                }
                else{
                    $user = null;
                    $msj = null;
                    $preview = null;
                }
                include "views/message/message.php";

            }
        }
        else{
            header("location: /7/Compass/login");
        }   
    }

    public function sentMsj($paths){
        $message = new MessageModel;
        $message->setMessage($_POST['msjtext']);
        $message->setDestiny($_POST['id']);
        $message->setRemitente($_SESSION['User']);

        $result = $message->insertMessage();

        if($result == true){
          /*  $user = UserModel::GetUser($_POST['id']);

            $Users = new MessageModel;
            $msj = MessageModel::getMsg($_POST['id'], $_SESSION['User']);

            $preview = MessageModel::getPreviewAllMsg($_SESSION['User']);

            include "views/message/message.php";*/

            
        }
        else
            include "views/error.php";  
   
    }

    
}