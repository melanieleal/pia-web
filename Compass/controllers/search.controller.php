<?php

require_once "controllers/template.controller.php";
require_once "models/UserModel.php";
require_once "helpers/action.php";
require_once "models/CourseModel.php";


class SearchController extends Controller {
    //CONTROLLER ROUTE
    public const ROUTE = "search";

    //ACTIONS ROUTING
    
    public const SEARCH = "index";
    public const SHOW_COURSE_CATEGORY = "course_category";
    public const AJAX = "results";


    public $actions;

    public function __construct(){
        //parent::__construct($pathName);

        $this->actions = array(self::SEARCH => new Action("Index", "IndexPost"),  
                                self::SHOW_COURSE_CATEGORY => new Action("showCourseC", null),
                                self::AJAX => new Action(null, "Ajax"));
    }

    public function ShowContent($paths) {
        //Determine wich method call TO A SPECIFIC ACTION SENDED IT IN THE URL
        Action::ValidateActionsPath($paths, $this);
    }


    public function Index($paths){ 

        $results = false;
        $category = CourseModel::getCategories();
        
        include "views/search/index.php";
    }

    
    public function IndexPost($paths) {
        
       $str = $_POST["search"];
       $user = $_POST["user"];
       $categoria = $_POST["Categoria"];
       $FechaI = $_POST["FechaInicio"];
       $FechaF = $_POST["FechaFin"];

        if(strcmp($str, "") == 0)
            $str = null;
        if(strcmp($user, "") == 0)
            $user = null;
        if(strcmp($categoria, "") == 0)
            $categoria = null;
        if(strcmp($FechaI, "") == 0)
            $FechaI = null;
        if(strcmp($FechaF, "") == 0)
            $FechaF = null;

    
        $category = CourseModel::getCategories();
        $results = CourseModel::SearchACourse($str, $user, $categoria, $FechaI, $FechaF); 

       include "views/search/index.php";
       
    }

    public function Ajax($paths){
        
        $str = $_POST["string"];
        
        $results = CourseModel::SearchCourses($str);
        ob_end_clean();
        $a = json_encode($results);
        echo $a;
        die();
    }

    public function showCourseC($paths){
        $category = str_replace('_', ' ',$paths[2]);
        $course = CourseModel::SearchCourseCategory(str_replace('_', ' ',$paths[2]));
        include "views/courses/coursecategory.php";
    }
}
