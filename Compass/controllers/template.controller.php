<?php
require_once "controllers/controller.php";
require_once "controllers/home.controller.php";
require_once "controllers/users.controller.php";
require_once "controllers/login.controller.php";
require_once "controllers/search.controller.php";
require_once "controllers/course.controller.php";
require_once "controllers/message.controller.php";

class Template {
    private $controllers;
    //It must come from the DB
    public const ROOT_PATH = "/7/Compass/";
    public function __construct(){
        $this->InitializeControllers();
    }
    
    public function ShowTemplate(){


        $categories = CourseModel::getCategories();
        if(isset($_SESSION["User"])){

            $usernav = UserModel::GetUser($_SESSION["User"]);

        }
        include "views/header_footer.php";
    }

    public function InitializeControllers(){
        $this->controllers = array(HomeController::ROUTE => new HomeController(),
                                    UsersController::ROUTE=> new UsersController(),
                                    LoginController::ROUTE=> new LoginController(),
                                    SearchController::ROUTE=> new SearchController(),
                                    CourseController::ROUTE=> new CourseController(),
                                    MessageController::ROUTE=> new MessageController());

    }

    
    public function DeterminePage(){
        Controller::ValidateControllersPath($this->controllers);
    }

    public static function Route($controller, $action){
        $route = self::ROOT_PATH;
        if($action != null)
            $route = self::ROOT_PATH.$controller."/".$action;
        else
            $route = self::ROOT_PATH.$controller;
        return $route;
    }

    
   
}


