<?php

require_once "controllers/template.controller.php";
require_once "models/UserModel.php";
require_once "models/CourseModel.php";
require_once "helpers/action.php";

class UsersController extends Controller {
    //CONTROLLER ROUTE
    public const ROUTE = "usuario";

    //ACTIONS ROUTING
    public const SHOW_USER_PROFILE = "profile";
    public const EDIT_USER = "editarusuario";
    public const REGISTER = "index";
    public const DELETE = "deleteuser";
    public const DETALLE_VENTA = "detalleventa";
    public const DIPLOMA = "diploma";

    public $actions;

    public function __construct(){
        //parent::__construct($pathName);

        $this->actions = array(self::SHOW_USER_PROFILE=> new Action("ShowUserProfile", "ShowUserProfile"),
                                self::EDIT_USER => new Action("EditUser", "EditUserPost"),
                                self::REGISTER => new Action("Index", "IndexPost"),
                                self::DELETE => new Action(null, "DeleteUser"),
                                self::DETALLE_VENTA => new Action("DetalleVenta", "DetalleVenta"),
                                self::DIPLOMA => new Action(null, "diploma"),
                                "ajax" => new Action(null, "Ajax"));
    }

    public function ShowContent($paths) {
        //Determine wich method call TO A SPECIFIC ACTION SENDED IT IN THE URL
        Action::ValidateActionsPath($paths, $this);
    }


    public function Index($paths){       
       include "views/users/index.php";
    }

    public function IndexPost($paths) {
        
        $user = new UserModel();

        $genero = $_POST["genero"];
        $rol = $_POST["rol"];

        if (strcmp($rol, "Escuela") == 0)
            $rol = 2;
        else
            $rol = 1;

        if (strcmp($genero, "Femenino") == 0)
            $genero = 1;
        else
            if (strcmp($genero, "Masculino") == 0)
                $genero = 2;
            else
                $genero = 3;


        $user->setRol($rol);
        $user->setId($_POST["username"]);
        $user->setPass($_POST["password"]);
        $user->setName($_POST["Nombre"]);
        $user->setLastName($_POST["Apellido"]);
        $user->setGenero($genero);
        $user->setDateNac($_POST["Nacimiento"]);
        $user->setEmail($_POST["email"]);

        $result = $user->InsertUser($user);
       
        $args = array("result" => $result);

        if($result == true){
            session_start();
            $_SESSION['User']= $user->getId();
            ob_end_clean();

            
            $categories = CourseModel::getCategories();
            header("location: /7/Compass/");
        }
        else
            include "views/users/index.php";            
       
    }

    public function DetalleVenta($paths){
        if(isset($paths[2])){
            $detalles = UserModel::getDetallesVenta($paths[2]);
            $curso = str_replace('_', ' ', $paths[3]);
            include "views/users/detallesVenta.php";
        }
        else
            include "view/error.php";
    }

    public function ShowUserProfile($paths){
        //Paths[] es un arreglo que extrae todos los parámetros de la url separados por "/", por lo que se puede extraer el elemnto N para ejecutar una funcion con un ode estos parámetros
        if (isset($_SESSION["User"])){  
            if(isset($paths[2])){
                
                $user = UserModel::GetUser($paths[2]);

                if($user->getRol()=="Escuela"){
                    $coursesby= CourseModel::GetCourseUser("E",$paths[2]);
                    $ventas = UserModel::getHistorialVentas($paths[2]);
                }

                if($user->getRol()=="Estudiante"){
                    $coursesof= CourseModel::GetCourseUser("A",$paths[2]);
                }

                include "views/users/showprofile.php";

            } else{
                include "views/error.php";
            }
        }
        else{
            header("location: /7/Compass/login");
        }
    }

    public function EditUser($paths){

        if(isset($paths[2])){
            
            $user = UserModel::GetUser($paths[2]);
            $genero = [
                ['value'=>'Femenino'],
                ['value'=>'Masculino'],
                ['value'=>'Otro']
            ];

           // $allCourse = CourseModel::GetCourseUser($paths[2]);
            include "views/users/edituser.php";

        } else{
            include "views/error.php";
        }

    }

    public function EditUserPost($paths){
        
        $idToEdit = $_POST["id"];

        $user = UserModel::GetUser($idToEdit);
        
        $user->setName($_POST["NombreE"]);
        $user->setLastName($_POST["ApellidoE"]);

        if(strcmp($_POST["passwordE"], "") == 0){
            $user->setPass($_POST["passwordA"]);
        }
        else
        $user->setPass($_POST["passwordE"]);

        $genero = $_POST["generoE"];
        if (strcmp($genero, "Femenino") == 0)
            $genero = 1;
        else
            if (strcmp($genero, "Masculino") == 0)
                $genero = 2;
            else
                $genero = 3;
        $user->setGenero($genero);

        $user->setDateNac($_POST["NacimientoE"]);

        //imagen
        
        if(count($_FILES) > 0){
            /*$nameImg = $_FILES["photo"]["name"];
            $nameFile = uniqid("perfil_").$nameImg;

            $carpeta = $_SERVER["DOCUMENT_ROOT"]."/7/Compass/img/";
            $pathImg = $carpeta.$nameFile;

            move_uploaded_file($_FILES["photo"]["tmp_name"], $pathImg);*/
            $user->setFoto(file_get_contents($_FILES["photo"]["tmp_name"]));
        }

        if($user->UpdateUser()){
            ob_end_clean();
            header("Location: /7/Compass/".self::ROUTE."/".self::SHOW_USER_PROFILE."/". $user->getId());
            die();
        } else {
            include "views/error.php";
        }
        
    }

    public function Ajax($paths){
        ob_end_clean();

        echo json_encode($_POST);
        die();
    }

    public function diploma($paths){
        
        if(isset($paths[2]) && isset($paths[3])){
            $Info = UserModel::getDiplomaInfo($paths[3], $paths[2]);
            include "views/users/diploma.php";
        }
       else
       include "views/error.php";
    }


    public static function getProfileImg($id){
        $user = UserModel::GetUser($id);
        return $user->getFoto();
    }

}
