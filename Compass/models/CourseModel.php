<?php

require_once "models/DbConnection.php";

class CourseModel {
    private $titulo;
    private $descC;
    private $descL;
    private $costo;
    private $portada;

    public function getTitle(){
        return $this->titulo;
    }

    public function setTitle($title){
        $this->titulo = $title;
    }

    public function getDescC(){
        return $this->descC;
    }

    public function setDescC($descC){
        $this->descC = $descC;
    }

    public function getDescL(){
        return $this->descL;
    }

    public function setDescL($descL){
        $this->descL = $descL;
    }
   
    public function getCosto(){
        return $this->costo;
    }

    public function setCosto($costo){
        $this->costo = $costo;
    }

    public function getPortada(){
        return $this->portada;
    }

    public function setPortada($portada){
        $this->portada = $portada;
    }

    public static function GetCourseUser($opc, $username) {
        
        $statement = DbConnection::Connect()->prepare("CALL getCoursesFrom(:opc, :user)");
        
        $idUser = strval($username);

        $statement->bindParam(":opc", $opc, PDO::PARAM_STR);
        $statement->bindParam(":user", $idUser, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);

            foreach($result as &$row){
                $row["Portada"] = base64_encode($row["Portada"]);
            }
            
        }catch(Exception $e){
            $result = false;
        }


        return $result;
    }

    public static function getCategories(){
        $statement = DbConnection::Connect()->prepare("CALL sp_cursocategoria ('show', null)");

        $result = true;
        try {
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public static function SearchCourseCategory($category){
        $procedure = "SP_Busqueda";

        $statement = DbConnection::Connect()->prepare("CALL ".$procedure."('category',null, :category, null, null, null)");
        
        $result = true;
        try {
            $statement->execute(array(  
                ":category" => $category
            ));

            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            foreach($result as &$row){
                $row["Portada"] = base64_encode($row["Portada"]);
            }
        } catch(Exception $e){
            $result = false;
        }

        return $result;
    }

    public static function SearchACourse($string, $user, $category, $dateI, $dateF){
        $procedure = "SP_Busqueda";

        $statement = DbConnection::Connect()->prepare("CALL ".$procedure."('avanzada', :string, :category, :user, :dateI, :dateF)");
        
        $result = true;
        try {
            $statement->execute(array(  
                ":string" => $string,
                ":category" => $category,
                ":user" => $user,
                ":dateI" => $dateI,
                ":dateF" => $dateF
            ));

            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
           
            foreach($result as &$row){
                $row["Portada"] = base64_encode($row["Portada"]);
            }

        } catch(Exception $e){
            $result = false;
        }

        return $result;
    }

    public static function SearchCourses($string) {
        $procedure = "SP_Busqueda";

        $statement = DbConnection::Connect()->prepare("CALL ".$procedure."('search', :string, null, null, null, null)");
        
        
        $statement->bindParam(":string", $string, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            foreach($result as &$row){
                $row["Portada"] = base64_encode($row["Portada"]);
            }

        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public static function getCourseInfo($opc, $id_curso, $username, $selectedlvl) {
        
        $statement = DbConnection::Connect()->prepare("CALL getCourseInfo(:opc, :idcurso, :user, :selected)");
        
        $idUser = strval($username);
        
        $statement->bindParam(":opc", $opc, PDO::PARAM_STR);
        $statement->bindParam(":idcurso", $id_curso, PDO::PARAM_INT);
        $statement->bindParam(":user", $idUser, PDO::PARAM_STR);
        $statement->bindParam(":selected", $selectedlvl, PDO::PARAM_INT);

        $result = true;
        try {
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            if($opc=="2")
            {
                foreach($result as &$row){
                    $row["Video_Presentacion"] = base64_encode($row["Video_Presentacion"]);
                }
            }
            if($opc=="3IN")
            {
                foreach($result as &$row){
                    $row["Video"] = base64_encode($row["Video"]);
                }
            }
            if($opc=="4")
            {
                foreach($result as &$row){
                    $row["Foto"] = base64_encode($row["Foto"]);
                }
            }
            


        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }


    public static function InsertCourseM($opc, $sp_Titulo, $sp_Descripcion_C, $sp_Descripcion_L, $sp_Costo, $sp_Portada, $sp_Video_Presentacion, $sp_fk_Creador) {
        $procedure = "sp_createcourse";

        $statement = DbConnection::Connect()->prepare("CALL ".$procedure."(:opc, :sp_Titulo, :sp_Descripcion_C, :sp_Descripcion_L, :sp_Costo,:sp_Portada, :sp_Video_Presentacion, :sp_fk_Creador)");
        
        
        $result = true;
        try {
            $statement->execute(array(  
                ":opc" => $opc,
                ":sp_Titulo" => $sp_Titulo,
                ":sp_Descripcion_C" => $sp_Descripcion_C,
                ":sp_Descripcion_L" => $sp_Descripcion_L,
                ":sp_Costo" => $sp_Costo,
                ":sp_Portada" => $sp_Portada,
                ":sp_Video_Presentacion" => $sp_Video_Presentacion,
                ":sp_fk_Creador" => $sp_fk_Creador
            ));

        } catch(Exception $e){
            $result = false;
        }

        return $result;
    }


    public static function InsertCourseRewviewM($sp_review, $sp_score, $sp_fk_Usuario, $sp_fk_course) {
        $procedure = "sp_createreview";

        $statement = DbConnection::Connect()->prepare("CALL ".$procedure."(:sp_review, :sp_score, :sp_fk_Usuario, :sp_fk_course)");
        
        
        $result = true;
        try {
            $statement->execute(array(  
                ":sp_review" => $sp_review,
                ":sp_score" => $sp_score,
                ":sp_fk_Usuario" => $sp_fk_Usuario,
                ":sp_fk_course" => $sp_fk_course
            ));

        } catch(Exception $e){
            $result = false;
        }

        return $result;
    }

    public static function sp_cursocategoriaM($opc, $cat) {
        $procedure = "sp_cursocategoria";

        $statement = DbConnection::Connect()->prepare("CALL ".$procedure."(:opc, :sp_categoria)");
        
        
        
        $statement->bindParam(":opc", $opc, PDO::PARAM_STR);
        $statement->bindParam(":sp_categoria", $cat, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
           
        }catch(Exception $e){
            $result = false;
        }


        return $result;
    }


    public static function sp_createlevelM($opc, $sp_Nombre, $sp_Video) {
        $procedure = "sp_createlevel";

        $statement = DbConnection::Connect()->prepare("CALL ".$procedure."(:opc, :sp_Nombre, :sp_Video)");
        
                
        $result = true;
        try {
            $statement->execute(array(  
                ":opc" => $opc,
                ":sp_Video" => $sp_Video,
                ":sp_Nombre" => $sp_Nombre
            ));
           
        }catch(Exception $e){
            $result = false;
        }


        return $result;
    }
    public static function sp_createmediaM($opc, $sp_Titulo, $sp_Link) {
        $procedure = "sp_createmedia";

        $statement = DbConnection::Connect()->prepare("CALL ".$procedure."(:opc,  :sp_Titulo, :sp_Link)");
        
        
        
        $statement->bindParam(":opc", $opc, PDO::PARAM_STR);
        $statement->bindParam(":sp_Titulo", $sp_Titulo, PDO::PARAM_STR);
        $statement->bindParam(":sp_Link", $sp_Link, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
           
        }catch(Exception $e){
            $result = false;
        }


        return $result;
    }

    public static function sp_createmedia_filesM($opc, $sp_file_, $sp_tipo, $sp_filename) {
        $procedure = "sp_createmedia_files";
         $statement = DbConnection::Connect()->prepare("CALL ".$procedure."(:opc, :sp_file_, :sp_tipo, :sp_filename)");
        
        
        $result = true;
        try {
            $statement->execute(array(  
                ":opc" => $opc,
                ":sp_file_" => $sp_file_,
                ":sp_tipo" => $sp_tipo,
                ":sp_filename" => $sp_filename
            ));

        } catch(Exception $e){
            $result = false;
        }

        return $result;
    }  

    public static function sp_media_imageM($opc, $sp_image, $descrip) {
        $procedure = "sp_media_image";

        $statement = DbConnection::Connect()->prepare("CALL ".$procedure."(:opc,  :sp_image, :descrip)");
        
        

        $result = true;
        try {
            $statement->execute(array(  
                ":opc" => $opc,
                ":sp_image" => $sp_image,
                ":descrip" => $descrip
            ));

           
        }catch(Exception $e){
            $result = false;
        }


        return $result;
    }
    public static function sp_media_textM($opc, $sp_Titulo, $sp_Descripcion) {
        $procedure = "sp_media_text";
        $statement = DbConnection::Connect()->prepare("CALL ".$procedure."(:opc, :sp_Titulo, :sp_Descripcion)");
        
        
        $statement->bindParam(":opc", $opc, PDO::PARAM_STR);
        $statement->bindParam(":sp_Titulo", $sp_Titulo, PDO::PARAM_STR);
        $statement->bindParam(":sp_Descripcion", $sp_Descripcion, PDO::PARAM_STR);

        $result = true;
        try {
            $statement->execute();
           
        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public static function getMasRecientes(){
        
        $statement = DbConnection::Connect()->prepare("CALL SP_Inicio('recientes')");

        $result = true;
        try{
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            foreach($result as &$row){
                $row["Portada"] = base64_encode($row["Portada"]);
            }
        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public static function getMasVendidos(){
        
        $statement = DbConnection::Connect()->prepare("CALL SP_Inicio('vendidos')");

        $result = true;
        try{
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            foreach($result as &$row){
                $row["Portada"] = base64_encode($row["Portada"]);
            }
        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public static function getMjrCalificados(){
        
        $statement = DbConnection::Connect()->prepare("CALL SP_Inicio('calificados')");

        $result = true;
        try{
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            foreach($result as &$row){
                $row["Portada"] = base64_encode($row["Portada"]);
            }
        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }
    
    public static function sp_buycourseM($sp_ID_Usuario,$sp_ID_Curso,$sp_Forma_pago){
        
        $statement = DbConnection::Connect()->prepare("CALL sp_buycourse(:sp_ID_Usuario,:sp_ID_Curso,:sp_Forma_pago)");

       
        $statement->bindParam(":sp_ID_Usuario", $sp_ID_Usuario, PDO::PARAM_STR);
        $statement->bindParam(":sp_ID_Curso", $sp_ID_Curso, PDO::PARAM_INT);
        $statement->bindParam(":sp_Forma_pago", $sp_Forma_pago, PDO::PARAM_INT);

        $result = true;
        try {
            $statement->execute();
           
        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public static function getLevelMultimedia($opc, $selectedlvl) {
        
        $statement = DbConnection::Connect()->prepare("CALL getlevelmultimedia(:opc, :selectedlvl)");
        
        
        $statement->bindParam(":opc", $opc, PDO::PARAM_STR);
        $statement->bindParam(":selectedlvl", $selectedlvl, PDO::PARAM_INT);

        $result = true;
        try {
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            if($opc=="f")
            {
                foreach($result as &$row){
                    $row["file_"] = base64_encode($row["file_"]);
                }
            }
            
            if($opc=="pdf")
            {
                foreach($result as &$row){
                    $row["file_"] = base64_encode($row["file_"]);
                }
            } 
            
            if($opc=="img")
            {
                foreach($result as &$row){
                    $row["image"] = base64_encode($row["image"]);
                }
            } 

        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public static function download($id) {
        
        $statement = DbConnection::Connect()->prepare("CALL downloadfile(:id)");
        
        
        $statement->bindParam(":id", $id, PDO::PARAM_INT);

        $result = true;
        try {
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
           
                foreach($result as &$row){
                    //$row["file_"] = base64_encode($row["file_"]);
                }

        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public  function updateAlumInfo($opc, $course, $usuario){
        $statement = DbConnection::Connect()->prepare("CALL sp_UpdateAlumnCourseInfo(:opc, :course, :usuario)");
        $result = true;

        try{
            $statement->execute(array(
                ":opc" => $opc,
                ":course" => $course,
                ":usuario" => $usuario
            ));
        }catch(Exception $e){
            $result = false;
        }
        
        return $result;
    }

    public  static function suspenderCourse($course){
        $statement = DbConnection::Connect()->prepare("CALL sp_suspenderCurso(:course)");
        $result = true;

        try{
            $statement->execute(array(
                ":course" => $course
            ));
        }catch(Exception $e){
            $result = false;
        }
        
        return $result;
    }
        
    public static function getCoursecat($id_curso) {
            
        $statement = DbConnection::Connect()->prepare("CALL getcategoriescourse(:idcurso)");
        
        
        $statement->bindParam(":idcurso", $id_curso, PDO::PARAM_INT);

        $result = true;
        try {
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }



    public static function UpdateCourse($uservalidate, $sp_ID_Curso,$sp_Titulo,$sp_Descripcion_C,$sp_Descripcion_L,$sp_Costo,$sp_Portada,$sp_Video_Presentacion){
        $proc = "sp_editcurso";
        $statement = DbConnection::Connect()->prepare("CALL ".$proc."(:uservalidate,:sp_ID_Curso,:sp_Titulo,:sp_Descripcion_C,:sp_Descripcion_L,:sp_Costo,:sp_Portada,:sp_Video_Presentacion)");

        $result = true;
        try {
            $statement->execute(array(  
                ":uservalidate" => $uservalidate,
                ":sp_ID_Curso" => $sp_ID_Curso,
                ":sp_Titulo" => $sp_Titulo,
                ":sp_Descripcion_C" => $sp_Descripcion_C,
                ":sp_Descripcion_L" => $sp_Descripcion_L,
                ":sp_Costo" => $sp_Costo,
                ":sp_Portada" => $sp_Portada,
                ":sp_Video_Presentacion"=> $sp_Video_Presentacion
            ));

        } catch(Exception $e){
            $result = false;
        }

        return $result;
    }

    public static function UpdateCourseLevel($uservalidate, $sp_ID_Nivel,$sp_Nombre,$sp_Video){
        $proc = "sp_editlvl";
        $statement = DbConnection::Connect()->prepare("CALL ".$proc."(:uservalidate,:sp_ID_Nivel,:sp_Nombre,:sp_Video)");

        $result = true;
        try {
            $statement->execute(array(  
                ":uservalidate" => $uservalidate,
                ":sp_ID_Nivel" => $sp_ID_Nivel,
                ":sp_Nombre" => $sp_Nombre,
                ":sp_Video" => $sp_Video
            ));

        } catch(Exception $e){
            $result = false;
        }

        return $result;
    }
}
