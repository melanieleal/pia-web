<?php

require_once "models/DbConnection.php";

class MessageModel {
    private $message;
    private $date;
    private $remitente;
    private $destiny;

    public function getMessage(){
        return $this->message;
    }

    public function setMessage($message){
        $this->message = $message;
    }

    public function getDate(){
        return $this->date;
    }

    public function setDate($date){
        $this->date = $date;
    }

    public function getRemitente(){
        return $this->remitente;
    }

    public function setRemitente($remitente){
        $this->remitente = $remitente;
    }

    public function getDestiny(){
        return $this->destiny;
    }

    public function setDestiny($destiny){
        $this->destiny = $destiny;
    }
   


    public function insertMessage() {
       
        $statement = DbConnection::Connect()->prepare("CALL SP_Mensajes('insert', :message, :remitente, :destiny)");
        $result = true;
        try {
            $statement->execute(array(  
                ":message" => $this->message,
                ":remitente" => $this->remitente,
                ":destiny" => $this->destiny
            ));

        } catch(Exception $e){
            $result =  false;
        } 

        return $result;
    }

    public static function getCountMsj($remitente){
        $statement = DbConnection::Connect()->prepare("CALL SP_Mensajes('seen', null, :remitente, null)");
        $result = true;
        try{
            $statement->execute(array(
                ":remitente" => $remitente
            ));
            $count = $statement->fetch();
            $result = $count['msj'];
           
        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public static function getMsg($remitente, $destiny){

        $statement = DbConnection::Connect()->prepare("CALL SP_Mensajes('msj', null, :remitente, :destiny)");
        $result = true;
        try{
            $statement->execute(array(
                ":remitente" => $remitente,
                ":destiny" => $destiny
            ));
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            
        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public static function getPreviewAllMsg($remitente){
        $statement = DbConnection::Connect()->prepare("CALL SP_Mensajes('preview', null, :remitente, null)");
        $result = true;
        try{
            $statement->execute(array(
                ":remitente" => $remitente
            ));
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
           
        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public static function getUltimoChat($remitente){
        $statement = DbConnection::Connect()->prepare("CALL SP_Mensajes('ultimo', null, :remitente, null)");
        
        try{
            $statement->execute(array(
                ":remitente" => $remitente
            ));
            $result = $statement->fetch();
            $user = new MessageModel();

            if($result == true){
                $destiny = null;
                if($result["destiny"] == $_SESSION['User']):
                    $destiny = $result["remitente"];
                else:
                    $destiny = $result["destiny"];
                endif;

                $user->setDestiny($destiny);
            }
            else
                $user = false;

        }catch(Exception $e){
            
        }
        return $user;
    }
}
