<?php

require_once "models/DbConnection.php";

class UserModel {
    private static $procedureName = "SP_Usuario";
    private $id_user;
    private $name;
    private $lastname;
    private $email;
    private $genero;
    private $pass;
    private $foto;
    private $rol;
    private $date_nac;
    private $date_mod;
    private $activo;
    private $total;


    public function getId(){
        return $this->id_user;
    }

    public function setId($id_user){
        $this->id_user = $id_user;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getLastName(){
        return $this->lastname;
    }

    public function setLastName($lastname){
        $this->lastname = $lastname;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function getGenero(){
        return $this->genero;
    }

    public function setGenero($genero){
        $this->genero = $genero;
    }
  
    public function getPass(){
        return $this->pass;
    }

    public function setPass($pass){
        $this->pass = $pass;
    }

    public function getFoto(){
        return $this->foto;
    }

    public function setFoto($foto){
        $this->foto = $foto;
    }
    
    public function getRol(){
        return $this->rol;
    }

    public function setRol($rol){
        $this->rol = $rol;
    }
    
    public function getDateNac(){
        return $this->date_nac;
    }

    public function setDateNac($date_nac){
        $this->date_nac = $date_nac;
    }

    public function getDateMod(){
        return $this->date_mod;
    }

    public function setDateMod($date_mod){
        $this->date_mod = $date_mod;
    }

    public function getActivo(){
        return $this->activo;
    }

    public function setActivo($activo){
        $this->activo = $activo;
    }

    public function getTotal(){
        return $this->total;
    }

    public function setTotal($total){
        $this->total = $total;
    }

    public function InsertUser() {
        $option = "Register";
       
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:opcion, :id, :name, :lastname, :dateNac, :email, :pass, null, :rol, :genero)");
        $result = true;
        try {
            $statement->execute(array(  
                ":opcion" => $option,
                ":id" => $this->id_user,
                ":name" => $this->name,
                ":lastname" => $this->lastname,
                ":genero" => $this->genero,
                ":dateNac" => $this->date_nac,
                ":email"=> $this->email,
                ":pass" => $this->pass,
                ":rol" => $this->rol
            ));

        } catch(Exception $e){
            $result =  false;
        } 


        return $result;
    }

    public function UpdateUser(){
        $option = "EditUser";
        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:opcion, :id, :name, :lastname, :dateNac, null, :pass, :photo, null,  :genero)");

        $result = true;
        try {
            $statement->execute(array(  
                ":opcion" => $option,
                ":id" => $this->id_user,
                ":name" => $this->name,
                ":lastname" => $this->lastname,
                ":genero" => $this->genero,
                ":dateNac" => $this->date_nac,
                ":photo"=> $this->foto,
                ":pass" => $this->pass
            ));

        } catch(Exception $e){
            $result = false;
        }

        return $result;
    }

    public static function GetUser($id) {
        $option = "GetUser";

        $statement = DbConnection::Connect()->prepare("CALL ".self::$procedureName."(:opcion, :id, null, null, null, null, null, null, null , null)");
        
        $idUser = strval($id);

        $statement->bindParam(":opcion", $option, PDO::PARAM_STR);
        $statement->bindParam(":id", $idUser, PDO::PARAM_STR);

        $user = null;
        try {
            $statement->execute();
            $result = $statement->fetch();

            $user = new UserModel();
            $genero = $result["fk_genero"];
            $rol = $result["fk_rol"];

            if ($rol == 1)
                $rol = "Estudiante";
            else
                $rol = "Escuela";

            if ($genero == 1)
                $genero = "Femenino";
            else
                if ($genero == 2)
                    $genero = "Masculino";
                else
                    $genero = "Otro";

            $user->setRol($rol);
            $user->setId($result["ID_Usuario"]);
            $user->setPass($result["Pass"]);
            $user->setName($result["Nombre"]);
            $user->setLastName($result["Apellido"]);
            $user->setGenero($genero);
            $user->setDateNac($result["Fecha_Nacimiento"]);
            $user->setEmail($result["Email"]);
            $user->setActivo($result["Activo"]);

            $user->setFoto($result["Foto"]);

        }catch(Exception $e){
           
        }
        return $user;
    }

    public function UserLogin(){
        $option = "user_login";
       
        $statement = DbConnection::Connect()->prepare("CALL ".$option."(:id, :pass)");
       
        $a=$this->id_user;
        $statement->bindParam(":id", $a, PDO::PARAM_STR);
        $statement->bindParam(":pass", $this->pass, PDO::PARAM_STR);
        $result = true;
        try {
            $statement->execute();
            $result = $statement->fetch();
            

            $aux = $result["validate"];

        }catch(Exception $e){
            $aux = false;
        }
        
        return $aux;
    }

    public static function hasReviewedM($user, $curso){

        $statement = DbConnection::Connect()->prepare("CALL sp_HasReviewed(:usuario, :curso)");

        $result = true;
        try {
            $statement->execute(array(  
                ":usuario" => $user,
                ":curso" => $curso
            ));
            $result=$statement->fetchAll(PDO::FETCH_ASSOC);

        } catch(Exception $e){
            $result = false;
        }

        return $result;
    }

    public static function getHistorialVentas($user){
        $statement = DbConnection::Connect()->prepare("CALL SP_Historial(:usuario)");

        $result = true;
        try{
            $statement->execute(array(
                ":usuario" => $user
            ));
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        } catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public static function getDetallesVenta($course){
        $statement = DbConnection::Connect()->prepare("CALL SP_DetallesC(:course)");

        $result = true;
        try{
            $statement->execute(array(
                ":course" => $course
            ));
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            
        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }

    public static function getDiplomaInfo($course, $usuario){
        $statement = DbConnection::Connect()->prepare("CALL sp_diploma(:usuario, :course)");

        $result = true;
        try{
            $statement->execute(array(
                ":usuario" => $usuario,
                ":course" => $course
            ));
            $result = $statement->fetch();
            
        }catch(Exception $e){
            $result =  false;
        }
        return $result;
    }

    public static function updateProgreso($nivel, $course, $usuario){
        $statement = DbConnection::Connect()->prepare("CALL sp_Progreso(:nivel, :curso, :usuario)");
        
        $result = true;
        try{
            $statement->execute(array(
                ":nivel" => $nivel,
                ":curso" => $course,
                ":usuario" => $usuario
            ));

        }catch(Exception $e){
            $result = false;
        }
        return $result;
    }
}

