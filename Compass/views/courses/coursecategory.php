
    <section class = "main">
        <div class="container text-white">
            <div class="row">
              
                <div class="col-md-12 mb-3">
                    <h2 class="text-capitalize font-weight-bold m-2"><?php echo $category?></h2>
                    <?php if( count($course) > 0):?>
                      <?php foreach($course as $key => $value):?>
                        
                        <a href="" class="text-white linkCard" style="text-decoration: none;">
                          <div class="card col-mb-4 bg-dark mt-2" style="max-height: 200px;">
                              <div class="row no-gutters" style="max-height: 200px;">
                                <div class="col-md-4">
                                  <img src="data:image/jpg;base64,<?php echo $value["Portada"]?>" class="card-img" style="border-bottom-right-radius: 0; border-top-right-radius: 0;" alt="..." height="200px">
                                </div>
                                <div class="col-md-8 ">
                                  <div class="card-body">
                                    <h3 class="card-title mb-0"><?php echo $value["Titulo"]?></h3>
                                    <p class="card-text m-0"><small class="text-muted"><?php echo $value["Usuario"] ?></small></p>
                                    <p class="card-text" style="min-height: 70px;"><?php echo $value["Descripcion_C"]?></p>
                                    <div class="text-right">
                                        <p class="card-text text-info">$MX <?php echo $value["Costo"]?></p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </a>

                      <?php endforeach?>
                    <?php else:?>
                      <h5>No hay cursos</h5>  
                    <?php endif?>
                </div>
            </div>

            <!--
              <ul class="pagination pagination-sm justify-content-center mt-2" >
                <li class="page-item"><a href=""><i class="fas fa-angle-left"></i></a></li>
                <li class="page-item ml-2 mr-2">2</li>
                <li class="page-item"><a href=""><i class="fas fa-angle-right"></i></a></li>
              </ul>
            -->
        </div>
    </section>