<head>
    <link rel="stylesheet" href="/7/Compass/style/curso.css">
    
    <link rel="stylesheet" href="/7/Compass/style/curso-comprado.css">
</head>
    <?php if ($inscrito[0]["AlumnoInscrito"] == 0):?>
    <section class = "main">
        <div class="container-fluid text-white">
            <div class="row ">
                <ol class="breadcrumb bg-transparent m-0">
                    <li class="breadcrumb-item active text-white" style="margin-left: 1.5rem;">Categoria</li>
                        <?php  for($h=0; $h<count($categoriescourse); $h++){
                            echo '<li class="breadcrumb-item"><a href="/7/Compass/search/course_category/'.$categoriescourse[$h]["categoria_name"].'">'.$categoriescourse[$h]["categoria_name"].'</a></li>';
                        }?>
                    
                </ol>
            </div>
            <div class="row">
                <h1><?php echo $cursoinfo[0]["Titulo"]?></h1>  
            </div>
            <div class="row">
                <div class="col-md-8"> 
                    <div class="row embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive embed-responsive-16by9 embed-responsive-item" <?php echo 'src="data:video/jpg;base64,'.$cursoinfo[0]["Video_Presentacion"].'"'?> allowfullscreen muted></iframe> 
                    </div>                
                   
                    <div class="row justify-content-end m-3">                   
                        <i class="fas fa-thumbs-up"></i> 
                        <h3 class=""><?php echo $cursoinfo[0]["likes"];?></h3>                                                          
                    </div>                
                </div>

                <div class="col-md-4" style="margin-top: 5rem;">                                                      
                    <p class="text-justify"><?php echo $cursoinfo[0]["Descripcion_L"]?></p>                 
                    <div class="text-right">
                        <h2 class = "d-inline">Creado por </h2>
                        <a href="/7/compass/usuario/profile/<?php echo $cursoinfo[0]["fk_Creador"]?>"><?php echo $cursoinfo[0]["fk_Creador"]?></a>
                    </div>
                    
                    <div class="contenido-curso">
                         <h2>Este curso incluye:</h2>
                         <ul>
                             <li><i class="fas fa-book"></i><?php echo count($niveles)?> Niveles</li>
                             <li><i class="fas fa-infinity"></i>Acceso de por vida</li>
                             <li><i class="fas fa-book-open"></i>Recursos extras</li>
                             <li><i class="fas fa-graduation-cap"></i>Certificado al completar el curso</li>
                             <li><i class="fas fa-language"></i>Español</li>
                         </ul>
                     </div>
                                        
                     <div class="row text-center" style="margin-top: 5rem;">
                         <div class="col-md-6 " style="margin-top: 1rem;">                           
                            <h3 class="d-inline">MX $<?php echo $cursoinfo[0]["Costo"]?></h3>
                         </div>
                         <div class="col-md-6">
                             <form action="/7/Compass/course/buycourse" method="post">
                                 <input name="cursoid" type="hidden" value="<?php echo $paths[2]?>">
                                <button class=" text-white border-0 btn-info" id="btn-ComprarCurso">Comprar ahora</button>
                            </form>
                         </div>  
                     </div>  
                </div>
            </div>
        </div>

        <div class="container text-white">
            <div class="row btn-group btn-group-sm">
                <button id="btn-Detalles" type = "button" class="btn btn-info">Detalles</button>
                <button id="btn-Comentarios" type = "button" class="btn btn-info">Comentarios</button>
            </div>
            <div class="row">
                <div class="Detalles" style="width:100%;">
                    <p class="text-justify"><?php echo $cursoinfo[0]["Descripcion_L"]?>
                    </p>                
                    <h2>Contenido del Curso</h2>
                    <ul class="list-group " style="width:100%;">
                    <?php  for($i=0; $i<count($niveles); $i++){
                        $auxi=$i+1;
                        echo '
                        <li class="list-group-item d-flex justify-content-between align-items-center">Nivel '.$auxi.': '.$niveles[$i]["Nombre"].'</li>';}?>
                    </ul>
                </div>   
                <div class="Comentarios" style="width: 100%;">
                <?php if($reviews == true){
                     for($i=0; $i<count($reviews); $i++){
                            echo '                   
                        <div class="Commentcontainer  row m-0" >
                            <div class="ico">';
                            if($reviews[$i]["Foto"] == null)
                                echo'<img class="rounded-circle m-3" src="/7/Compass/img/usuario.png" alt="">';
                            else
                                echo'<img class="rounded-circle mt-3" src="data:image/jpg;base64,'.$reviews[$i]["Foto"].'" alt="">';                       
                            echo
                            '</div>
                            <div class="col-md-10 m-3 comment">        
                                <h1>'.$reviews[$i]["Nombre"].'</h1>
                                <h2>'.$reviews[$i]["date_r"].'</h2>
                                <p>'.$reviews[$i]["review"].'</p>        
                                <div class="calif-comentario text-right">';
                                if ($reviews[$i]["score"] == 1){
                                    echo '<i class="fas fa-thumbs-up" aria-hidden="true"></i>';
                                }else{   
                                    echo '<i class="fas fa-thumbs-down" aria-hidden="true"></i>';}
                                echo '</div>  
                            </div>
                        </div>';}
                        }else
                        echo'<p>No hay comentarios del curso</p>';
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?php endif ?>
    
    <?php if ($inscrito[0]["AlumnoInscrito"] == 1):?>
    <section class = "main">
        <div class="container-fluid text-white">
            <div class="row">
                <h1><?php echo $cursoinfo[0]["Titulo"]?></h1>  
            </div>
            <div class="row">
                <h1>Nivel 
                    <?php if(isset($paths[3])):
                                {echo $paths[3];}
                            else:{echo '1';}
                    endif ?> 
                    <?php echo ': '.$selected[0]["Nombre"];  ?>
                </h1>
            </div> 
            <div class="row ml-1">
              <h6 class="ml-5">Creado por:<a href="/7/Compass/usuario/profile/<?php echo $cursoinfo[0]["fk_Creador"]?>" class="ml-1"><?php echo $cursoinfo[0]["fk_Creador"]?></a><a href="/7/Compass/message/msj/<?php echo $cursoinfo[0]["fk_Creador"]?>"><i class="far fa-comments ml-1 text-primary"  style="color: #91d5b3 !important;"></i></a></h6> 
            </div>

            <div class="row m-0">
                <div class="col-md-9" > 
                    <nav> 
                        <div class="nav nav-tabs bg-dark"  role="tablist">
                            <a class="nav-item nav-link active" id="opc-video" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="true">Video</a>
                            <a class="nav-item nav-link" id="opc-file" data-toggle="tab" href="#file" role="tab" aria-controls="info" aria-selected="false">Archivos</a>  
                            <a class="nav-item nav-link" id="opc-pdf" data-toggle="tab" href="#pdf" aria-controls="arch" aria-selected="false">PDFs</a>
                            <a class="nav-item nav-link" id="opc-link" data-toggle="tab" href="#links" role="tab" aria-controls="info" aria-selected="false">Enlaces</a>  
                            <a class="nav-item nav-link" id="opc-img" data-toggle="tab" href="#images" aria-controls="arch" aria-selected="false">Imágenes</a>
                            <a class="nav-item nav-link" id="opc-text" data-toggle="tab" href="#textos" aria-controls="arch" aria-selected="false">Texto</a>
                        </div>
                    </nav>
                    <div class="tab-content" >
                        <div class="tab-pane fade show active" id="video" role="tabpanel" aria-labelledby="opc-video">
                             <iframe class="embed-responsive embed-responsive-16by9 embed-responsive-item" <?php echo 'src="data:video/jpg;base64,'.$selected[0]["Video"].'"'?> allowfullscreen muted></iframe> 
                        </div> 


                       <div class="tab-pane fade" id="file" role="tabpanel" aria-labelledby="opc-info">
                            <p class="text-justify">
                            Descargue los archivos adjuntos al nivel.
                                <ul> 

                                <?php  if($lvlfiles == true){ 
                                    for($i=0; $i<count($lvlfiles); $i++){
                                        $auxi=$i+1;
                                        echo '
                                    
                                        <p>
                                        <form action="/7/Compass/course/download/" method="post">
                                        
                                        <button type="submit"><i class="fas fa-file-download"></i>'.$lvlfiles[$i]["filename"].'</button>
                                        <input name="file" type="hidden" value="'.$lvlfiles[$i]["ID_media_files"].'"/>
                                        
                                        </form></p>
                                ';}
                                }else
                                    echo '<p class="text-muted"> No hay archivos</p>';?> 

                                
                                </ul>
                            </p>           
                        </div> 
                        
                        <div class="tab-pane fade" id="pdf" role="tabpanel" aria-labelledby="opc-info">
                            <p class="text-justify">
                            Descargue los PDFs referentes a este nivel.
                                <ul> 
                                <?php if($lvlpdfs == true){ 
                                     for($i=0; $i<count($lvlpdfs); $i++){
                                        $auxi=$i+1;
                                        echo '
                                    
                                        <p>
                                        <form action="/7/Compass/course/download/" method="post">
                                        
                                        <button type="submit"><i class="far fa-file-pdf"></i>'.$lvlpdfs[$i]["filename"].' </button>
                                        <input name="file" type="hidden" value="'.$lvlpdfs[$i]["ID_media_files"].'"/>
                                    
                                        </form></p>
                                ';}}else
                                    echo '<p class="text-muted"> No hay Pdf´s</p>';?> 

                                
                                </ul>
                            </p>


                            
                        </div> 
                        
                        <div class="tab-pane fade" id="links" role="tabpanel" aria-labelledby="opc-info">
                            <p class="text-justify"> 
                            Acceda a enlaces referenciados por el autor para este nivel.
                                <ul>  
                                <?php  if($lvlurls == true){ 
                                    for($i=0; $i<count($lvlurls); $i++){
                                        $auxi=$i+1;
                                        echo '
                                    
                                        <li><p>'.$lvlurls[$i]["Titulo"].': <a href="'.$lvlurls[$i]["Link"].'" style="color: #91d5b3 !important;">'.$lvlurls[$i]["Link"].'</a>
                                        </p></li>
                                ';}}else
                                    echo '<p class="text-muted"> No hay enlaces</p>';?> 

                                
                                </ul>
                            </p>


                            
                        </div>   

                        <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="opc-info">
                            <p class="text-justify"> 
                            Imágenes de referencia para este nivel.
                                <ul style="text-align: center;">  
                                <?php  if($lvlimgs == true){ 
                                for($i=0; $i<count($lvlimgs); $i++){
                                    $auxi=$i+1;
                                    echo '
                                
                                    <li><img src="data:image/jpg;base64,'.$lvlimgs[$i]["image"].'">
                                    <p> '.$lvlimgs[$i]["descrip"].'</p></li>
                                ';}}else
                                    echo '<p class="text-muted"> No hay Imagenes</p>';?> 

                                
                                </ul>
                            </p>


                            
                        </div>               
                        <div class="tab-pane fade" id="textos" role="tabpanel" aria-labelledby="opc-info">
                            <p class="text-justify"> 
                            Consulta las notas de texto para este nivel.
                                <ul>  
                                <?php  if($lvltxts == true){ 
                                    for($i=0; $i<count($lvltxts); $i++){
                                        $auxi=$i+1;
                                        echo '
                                    
                                        <li><h3>'.$lvltxts[$i]["Titulo"].'</h3>
                                        <p> '.$lvltxts[$i]["Descripcion"].'</p></li>
                                ';}}else
                                    echo '<p class="text-muted"> No hay notas de texto</p>';?> 

                                
                                </ul>
                            </p>


                            
                        </div>  
                    </div>                                              
                </div>

                <div class="col-md-3">                                                      
                    
                        <ul class="list-group">
                            
                            <?php  for($i=0; $i<count($niveles); $i++){
                            $auxi=$i+1;
                            echo '
                            <li class="list-group-item">
                                <a href="/7/Compass/course/show/'.$cursoinfo[0]["ID_Curso"].'/'.$auxi.'" class="nivel" id="nivel'.$auxi.'">Nivel '.$auxi.': '.$niveles[$i]["Nombre"].'</a>
                             </li>';}?> 
                        </ul>
                    <div class="row comentario mt-2">
                        <h5 style="width: 100%;">Comentarios</h5> 
                        <?php if($hasreviewed[0]["result"] == 0 && $hasreviewed[0]["Progreso"]>99):?>
                            <form action="/7/Compass/course/submitreview" id="reviewform"style="width: 100%;" method="post">
                                <div class=" crear-comentario" >
                                    <div class="puntuacion  text-right">
                                        <p class="d-inline">¿Te gustó el curso?</p>
                                        <input type="radio" id="like" name="score" value="1" required/> <label for="like"> <i class="fas fa-thumbs-up" id="btn-like"> </i> </label>
                                        <input type="radio" id="dislike" name="score" value="0"/> <label for="dislike"><i class="fas fa-thumbs-down" id="btn-dislike"> </i></label>
                                    </div>
                                    <textarea class="d-block tx-Comentario" placeholder="Escribe tu comentario" id="reviewbody" cols="30" rows="10" name="reviewbody"></textarea>                          
                                    <h2 class="text-danger d-none error"><i class="fas fa-exclamation"></i> Ingrese su comentario</h2>
                                    <input type="hidden" name="course" value="<?php echo $cursoinfo[0]["ID_Curso"]?>"> 
                                    <input type="hidden" name="lvl" value="<?php if(isset($paths[3])){echo $paths[3];}else{echo 0;}?>"> 
                                    <div class="enviar d-flex justify-content-center">
                                        <button class="d-block border-0 text-white" type="submit"  id = "EnviarCom">Enviar</button>
                                    </div>      
                                </div>
                            </form>    
                        <?php endif ?>

                        <?php  if($reviews == true){
                            for($i=0; $i<count($reviews); $i++){
                                echo ' 
                                <div class="row m-0" style="width: 100%;">
                                    <div class="col-md-1 text-center">';
                                        if($reviews[$i]["Foto"] == null)
                                        echo ' <img src="/7/Compass/img/usuario.png" class="rounded-circle mt-3" alt="">';
                                        else
                                        echo'<img class="rounded-circle mt-3" src="data:image/jpg;base64,'.$reviews[$i]["Foto"].'" alt="">';                       
                                echo'</div>
                                    <div class="col-md-10 m-3">        
                                        <h1 class="m-0">'.$reviews[$i]["Nombre"].'</h1>
                                        <h2>'.$reviews[$i]["date_r"].'</h2>
                                        <p>'.$reviews[$i]["review"].'</p>        
                                        <div class="calif-comentario text-right">';
                                        if($reviews[$i]["score"]=="1"){echo '<i class="fas fa-thumbs-up"></i>';}else{echo '<i class="fas fa-thumbs-down"></i>';}
                                        echo '</div>  
                                    </div>
                                </div>
                         ';}} else
                                echo 'No hay comentarios del curso';
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif ?>
</html>