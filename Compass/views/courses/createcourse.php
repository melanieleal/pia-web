<head>
    <script src = "/7/Compass/JS/append.js"></script>
    
    <script src = "/7/Compass/JS/deletedivform.js"></script>
    <script src = "/7/Compass/JS/validcreatecourse.js"></script>
    <link rel="stylesheet" href="/7/Compass/style/formsformat.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    
</head>

<section class = "main">
    <form id="create_course_form" name="create_course" action="/7/Compass/course/submitcourse" method="post" enctype="multipart/form-data" class="row col-md-12">    
        <div class="container py-4"> 
            <div class="row text-white" id="createcourseform" >
                <div class="col-md-12 px-0">
                    <div class="row">
                        <div class="col-md-12 p-2" id="formtitle">   
                            <h2 class="frm_section_n text-white fw-bold">Crear curso</h2>                            
                        </div>  
                    </div>
                    <div class="row cardbody">
                        <h4 class="col-md-12 frm_section_n fw-bold p-3" style="color: #91d5b3;">Información Básica</h4>
                        <div class="col-md-12 form_field_outer p-0 pb-3">
                            <div class="row form_field_outer_row">
                                <div class="form-group col-md-2" style="display: flex; padding-left: 10px;">
                                    <h6>Imagen de Presentación: </h6>
                                </div>
                                <div class="form-group col-md-7">
                                <input type="file" name="imagecourse"  accept="image/*" id="" required >  
                                </div>
                                
                            </div>
                            <div class="row form_field_outer_row">
                                    <div class="form-group col-md-2" style="display: flex; padding-left: 10px;">
                                        <h6>Video de Presentación: </h6>
                                    </div>
                                    <div class="form-group col-md-7">
                                    <input type="file" name="videoP"  accept="video/*" id="" required>  
                                    </div>
                                    
                            </div>
                            <div class="row form_field_outer_row">
                                <div class="form-group col-md-2" style="display: flex; padding-left: 10px;">
                                    <h6>Nombre del curso: </h6>
                                </div>
                                <div class="form-group col-md-7">
                                    <input required name="course_name" type="text" class="form-control w_90"  id="course_name" placeholder="Curso" />
                                </div>
                                
                            </div>
                            <div class="row form_field_outer_row mt-2">
                                <div class="form-group col-md-2" style="display: flex; padding-left: 10px;">
                                    <h6>Categoría: </h6>
                                </div>
                                <div class="form-group col-md-5">
                                    <select id="categories"  multiple="multiple" name="categorias[]" required>
                                    <?php foreach($categoriessel as $key => $value):?>
                                        <option value="<?php echo $value["categoria_name"]?>"> <?php echo $value["categoria_name"]?></a>
                                    <?php endforeach?> 
                                    
                                    </select>
                                </div>
                            </div>
                            <div class="row col-md-12 p-0 form-group mt-2">
                                <div class="form-group col-md-4" style="display: flex; padding-left:10px;">
                                    <h6>Descripción corta: </h6>
                                </div>
                                <div class="form-group col-md-12">
                                    <input required name="course_shortdesc" type="text" class="form-control w_90"  id="course_shortdesc" placeholder="Descripcion" />
                                </div>  
                                <div class="form-group col-md-4 mt-2" style="display: flex; padding-left:10px;">
                                    <h6>Descripción detallada:  </h6>
                                </div>
                                <div class="col-md-12 pb-2">
                                    <textarea required name="course_largedesc" class="form-control" id="course_largedesc" rows="4"></textarea>
                                </div>

                            </div>
                            <div class="row col-md-12 p-0 form-group mt-2">
                                <div class="form-group col-md-2" style="display: flex; padding-left:10px;">
                                    <h6>Costo del Curso </h6>
                                </div>
                                <div class="form-group col-md-10">
                                    <input required name="costocourse" type="text" class="form-control col-2"  id="course_costo" placeholder="MX$" />
                                </div>  

                            </div>
                        </div>
                    </div>

                    <div class="row cardbody" id="levels">          
                        <h4 class="col-md-12 frm_section_n fw-bold p-3" style="color: #91d5b3;">Niveles</h4>
                        <p class="col-md-12"> Todo curso debe tener, como mínimo, un nivel; cada nivel debe contar con un video obligatoriamente.</p>
                        <div id="levelssection" class="col-md-12">
                            <div class="accordionlevels accordion" id="accordionlevel0"  name="niveles">      
                                <!--Card de nivel-->
                                <div class="col-md-12 p-0 courselevel card">
                                    <div class="col-md-12 form_field_outer p-0">
                                        <div class="row form_field_outer_row">
                                        
                                            <div class="form-group col-md-12 pb-2 card-header" id="headinglvl0">
                                                <div class="form-group col-md-12 ">
                                                        <h4 class="text-white" style="padding-right: 25px;">Nivel 1 </h4>
                                                        
                                                </div>
                                            </div>

                                            <div id="lvl0" class="collapse show col-md-12 p-0" aria-labelledby="headinglvl0" data-parent="#accordionlevels">
                                                <div class="card-body">
                                                    <div class="form-group col-md-12 pt-3">
                                                        <div class="col-md-12 form_field_outer p-0">
                                                            <div class="row form_field_outer_row pb-3">    
                                                                <div class="form-group" style="display: flex; padding-left: 10px;">
                                                                    <h6 class="text-white m-0 p-0">Nombre del nivel:  </h6>                                 
                                                                </div>    
                                                                <div class="form-group col-md-5 p-0">
                                                                    <input name="level_title[]" id="lvl0level_title" type="text" class="form-control w_90" placeholder="nivel"  style="margin:0 10px;"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <section class="formsection pb-3 col-md-12 p-0">
                                                        <div class="col-md-12 form_field_outer">
                                                            <div class="row form_field_outer_row pb-2">    
                                                                <div class="form-group" style="display: flex; padding-left: 10px;">
                                                                    <h6 class="text-white m-0 p-0">Multimedia del nivel:  </h6>                                 
                                                                </div>    
                                                            </div>
                                                        </div>

                                                        <div class="col-md-11 form_field_outer m-auto p-0">    
                                                            <div class="noth">
                                                                <div class="accordion" id="accordionlvl0">
                                                                    <div class="card">
                                                                        <div class="card-header p-0 p-0" id="headingOne0">
                                                                            <h5 class="mb-0">
                                                                               <h3>Video</h3>
                                                                            </h5>
                                                                        </div>
                                                                
                                                                        <div id="collapseOne0" class="collapse show " aria-labelledby="headingOne0" data-parent="#accordionlvl0">
                                                                            <div class="card-body">
                                                                                <div class="levelcontent form-group col-md-12">  
                                                                                    <div class="levelmedia" id="lvl0vids">
                                                                                        <div class="form-group col-md-12">
                                                                                                <input type="file" name="videolevel[]" id="lvl0videolevel"  accept="video/*" required>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>  
                                                                    <div class="card">
                                                                        <div class="card-header p-0" id="headingTwo0">
                                                                            <h5 class="mb-0">
                                                                                    <h3>Archivos</h3>
                                                                            </h5>
                                                                        </div>
                                                                        <div class="collapse show" aria-labelledby="headingTwo0" data-parent="#accordionlvl0">
                                                                            <div class="card-body">
                                                                                <div class="levelcontent form-group col-md-12">
                                                                                    <div class="levelmedia" id="lvl0files" data-lvl="0">
                                                                                        
                                                                                    </div>
                                                                                    <div class="form-group col-md-12"> 
                                                                                        <div class="flexdiv" style="display: flex;">
                                                                                            <button  type="button" class="btn btn-outline-lite py-0 add_new_frm_field_btn ml-auto addfilebtn" data-parent="#lvl0files"><i class="fas fa-plus add_icon ml-auto" ></i> Añadir Archivo</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> 
                                                        
                                                                    <div class="card">
                                                                        <div class="card-header p-0" id="headingThree">
                                                                            <h5 class="mb-0">
                                                                                  <h3>PDFs</h3>
                                                                            </h5>
                                                                        </div>
                                                                        <div class="collapse show" aria-labelledby="headingThree" data-parent="#accordionlvl0">
                                                                            <div class="card-body">
                                                                                <div class="levelcontent form-group col-md-12">
                                                                                    <div class="levelmedia" id="lvl0pdfs" data-lvl="0">  
                                                                                        
                                                                                           
                                                                                        
                                                                                    </div>
                                                                                    <div class="form-group col-md-12"> 
                                                                                        <div class="flexdiv" style="display: flex;">
                                                                                                <button  type="button" class="btn btn-outline-lite py-0 add_new_frm_field_btn ml-auto addpdfbtn" data-parent="#lvl0pdfs"><i class="fas fa-plus add_icon ml-auto" ></i> Añadir PDf</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> 
                                                        
                                                                    <div class="card">
                                                                        <div class="card-header p-0" id="headingFour">
                                                                            <h5 class="mb-0">
                                                                               <h3>Enlaces</h3>
                                                                            </h5>
                                                                        </div>
                                                                        <div class="collapse show" aria-labelledby="headingFour" data-parent="#accordionlvl0">
                                                                            <div class="card-body">
                                                                                <div class="levelcontent form-group col-md-12">
                                                                                    <div class="levelmedia" id="lvl0links" data-lvl="0">
                                                                                        
                                                                                    </div>
                                                                                    <div class="form-group col-md-12"> 
                                                                                        <div class="flexdiv" style="display: flex;">
                                                                                                <button type="button"  class="btn btn-outline-lite py-0 add_new_frm_field_btn ml-auto addlinkbtn"  data-parent="#lvl0links"><i class="fas fa-plus add_icon ml-auto" ></i> Añadir Enlace</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> 
                                                        
                                                        
                                                                    <div class="card">
                                                                        <div class="card-header p-0" id="headingFive">
                                                                            <h5 class="mb-0">
                                                                               <h3>Imágenes</h3>
                                                                            </h5>
                                                                        </div>
                                                                        <div  class="collapse show card-body" aria-labelledby="headingFive" data-parent="#accordionlvl0">
                                                                            <div id="lvl0imgs" data-lvl="0">
                                                                               
                                                                            </div>
                                                                             <div class="form-group col-md-12"> 
                                                                                <div class="flexdiv" style="display: flex;">
                                                                                    <button  type="button" class="btn btn-outline-lite py-0 add_new_frm_field_btn ml-auto addimgbtn"  data-parent="#lvl0imgs"><i class="fas fa-plus add_icon ml-auto" ></i> Añadir Imagen</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                       
                                                                    </div> 
                                                        
                                                                    <div class="card">
                                                                        <div class="card-header p-0" id="headingSix">
                                                                            <h5 class="mb-0">
                                                                                  <h3>Texto</h3>
                                                                            </h5>
                                                                        </div>
                                                                
                                                                        <div class="collapse show" aria-labelledby="headingSix" data-parent="#accordionlvl0">
                                                                            <div class="card-body">
                                                                                
                                                                                        <div id="lvl0texts" data-lvl="0">
                                                                                           
                                                                                        </div>
                                                                                        <div class="form-group col-md-12"> 
                                                                                            <div class="flexdiv" style="display: flex;">
                                                                                                    <button  type="button" class="btn btn-outline-lite py-0 add_new_frm_field_btn ml-auto addtextbtn"  data-parent="#lvl0texts"><i class="fas fa-plus add_icon ml-auto" ></i> Añadir Texto</button>
                                                                                            </div>
                                                                                        </div>
                                                                                    
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div> 
                                                                </div>
                                                            </div> 
                                                        </div>  
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                     
                            </div>
                            
                        </div>


                        <div class="row ml-auto  mt-3 mr-3" >
                            <div class="col-md-12" style="display: flex;">
                                <div class="flexdiv mr-auto" style="display: flex;">
                                    <button  type="button" class="btn btn-outline-lite py-0 add_new_frm_field_btn ml-auto addbtn mr-auto" id="addlevelbtn"><i class="fas fa-plus add_icon ml-auto" ></i> Agregar Nivel</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row col-md-12 cardbody m-auto" style="border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;"> 
                        <button class="roundbutton border-0 text-white mt-3 mb-3 ml-auto mr-auto"> Publicar </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>


