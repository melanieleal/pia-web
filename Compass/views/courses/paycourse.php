<head>

    <link rel="stylesheet" href="/7/Compass/style/pago.css">
    <script src="/7/Compass/JS/jquery.card.js"></script>

    <script src="/7/Compass/JS/payment.js"></script>
    
    <script src = "/7/Compass/JS/validatecreditcard.js"></script>

</head>

<section class="main text-white">
    <div class="container">
        <div class="row ">
            <div class="col-md-8 ">
                <h1>Pago</h1>
                <div class="pago">
                    <div class="detalle ">
                        <h2>Detalles de pedido</h2>
                        <p class="d-inline"><?php echo $cursoinfo[0]["Titulo"]?></p>
                        <h3 class="d-inline">MX$<?php echo $cursoinfo[0]["Costo"]?></h3>
                    </div>

                </div>

            </div>
            <div class="col-md-4 pb-3">
                <h2>Resumen</h2>
                <hr>
                <div class="resumen text-center">
                    <p class="m-3"><?php echo $cursoinfo[0]["Titulo"]?></p>
                </div>

                <h6 class="text-justify text-muted">Compass está obligado por ley a cobrar los impuestos de transacción aplicables para las compras realizadas en ciertas jurisdicciones fiscales.
                    Al completar su compra, usted acepta estos Términos de servicio.</h6>
                <div class="total text-right">
                    <p>Total:</p>
                    <h4 class="d-inline">MX$<?php echo $cursoinfo[0]["Costo"]?></h4>
                </div>



                    <p class="d-inline">Método a pagar: </p>
                <?php if($cursoinfo[0]["Costo"] > 0):?>
                <!-- Set up a container element for the button -->
                <div id="paypal-button-container"></div>
                <?php endif;?>
                <!-- Include the PayPal JavaScript SDK -->
                <script src="https://www.paypal.com/sdk/js?client-id=Afj1q6CQgkbfs0kqb2RmzDmWpkp6aG--ACghPyvsC2EA_dXxJgME7BS1RNbfDOZZZHYtoJGHwBzYDKWZ&currency=MXN&disable-funding=card" data-sdk-integration-source="button-factory" data-namespace="paypal_sdk"></script>
                <script>
                    paypal_sdk.Buttons({

                        // Set up the transaction
                        createOrder: function(data, actions) {
                            return actions.order.create({
                                purchase_units: [{
                                    amount: {
                                        value: <?php echo $cursoinfo[0]["Costo"]?>
                                    }
                                }]
                            });
                        },

                        // Finalize the transaction
                        onApprove: function(data, actions) {
                            return actions.order.capture().then(function(orderData) {
                                // Successful capture! For demo purposes:
                                console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));
                                var transaction = orderData.purchase_units[0].payments.captures[0];
                              
                                var formData = new FormData();
                               
                                formData.append("curso", "<?php echo $cursoinfo[0]["ID_Curso"]?>");
                                formData.append("formapago", 0); // number 123456 is immediately converted to string "123456"

                                var request = new XMLHttpRequest();
                                request.open("POST", "/7/Compass/course/paycourse");
                                request.send(formData);
                                alert("Transacción exitosa");
                               
                            });
                        }


                    }).render('#paypal-button-container');
                </script>

                <a class="btn btn-primary" data-toggle="collapse" href="#collapseCardForm" role="button" aria-expanded="false" aria-controls="collapseCardForm" style="box-shadow:none; border-radius:4px; height:45px;font-size: 1.2rem;">
                <i class="far fa-credit-card"></i>  Pago con tarjeta
                </a>
                <div class="collapse" id="collapseCardForm"style="box-shadow:none;">
                    <div id="cardpaymentcontainer" class="card card-body p-0" style="box-shadow:none; background:none;">
                        <div class="card-wrapper pt-1"></div>

                        <div class="form-container active">
                            <form id="cardpayment" action="/7/Compass/course/paycourse" method="post">
                                <label class="mb-0 mt-2" for="text"> Número de tarjeta</label>
                                <input type="text" placeholder="Número de tarjeta" name="number">
                                <label class="mb-0 mt-2" for="cardname"> Nombre(s)</label>
                                <input type="text" placeholder="Nombre" name="firstname" />
                                <label class="mb-0 mt-2" for="cardname"> Apellido(s)</label>
                                <input type="text" placeholder="Apellido" name="lastname" />
                                <div style="width: 100%; display: flex;">
                                    <div style="width: 50%;">
                                        <label class="mb-0 mt-2" for="cardmonth"> Vencimiento</label>
                                        <input type="text" placeholder="Vencimiento" name="expiry" style="margin-right: 5px;" />
                                    </div>
                                    <div style="width: 50%;">
                                        <label class="mb-0 mt-2" for="cardyear"> CVC</label>
                                        <input type="text" placeholder="CVC" name="cvc" style="margin-left: 5px;" />
                                    </div>
                                </div>
                                
                                <input name="curso" type="hidden" value="<?php echo $cursoinfo[0]["ID_Curso"]?>">
                                <input name="formapago" type="hidden" value= "1">


                                <button type="submit" placeholder="Pagar" class="m-2" id="paybutton" style="width: 100px;">Pagar</button>
                            </form>
                        </div>
                    </div>
                </div>






            </div>
        </div>


    </div>




</section>