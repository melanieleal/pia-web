
<?php 
  /*ob_start();*/
  session_start();
  /*cambo*/
  if(isset($_SESSION['User']))
  {
    $aux = $_SESSION['User'];
    $countMsj = MessageModel::getCountMsj($_SESSION['User']);
  } 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--bootstrap CSS 4.0.0-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> <!--responsive-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>   

    <link rel="stylesheet" href="/7/Compass/style/main.css">
    <script src = "/7/Compass/JS/jquery-3.6.0.min.js"></script>

    <script src = "/7/Compass/JS/main.js"></script>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Mukta:wght@300;400&display=swap" rel="stylesheet">

    <link  rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    
    <title>COMPASS</title>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://kit.fontawesome.com/9a45069470.js" crossorigin="anonymous"></script>
</head>
<body>
    <section class="header ">
      <nav class="navbar navbar-expand-lg navbar-light container-fluid">
      
            <div style="width: 20%; min-width: 240px;">
              <a class="navbar-brand" href= "<?php echo "/7/Compass/".HomeController::ROUTE."/".HomeController::INDEX?>"> <h1 class ="d-inline fw-bolder">C </h1>
              <img src="/7/Compass/img/compass-h.png" alt="" style="height: 30px; width: 30px;">
              <h1 class ="d-inline fw-bolder">M P A S S</h1></a>
            </div>
            <button class="navbar-toggler mb-3" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
      
       
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav m-auto" style="width: 60%; min-width: 400px;">
            <li class="searchbar" style="width: 100%;">
          
                <form class="box " action="/7/compass/search" method="post" charset="utf-8" id="searchform">  
                    <input name = "search" type="search"  class = "border-0 searchboxnav" id="searchstring" style="padding-left: 15px; width: 90%;" placeholder="Buscar en Compass">  
                    <button class = "border-0 bg-transparent"><i class="fab fa-sistrix"></i></button>           
                </form>
            
            </li>
            <li class="nav-item dropdown "style="min-width: 115px;">
              <a class="nav-link dropdown-toggle header-categorias mt-2" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Categorías
              </a>
              <div class="dropdown-menu submenu-categorias" aria-labelledby="navbarDropdown">
                <?php foreach($categories as $key => $value):?>
                  <a class="dropdown-item" href="<?php echo "/7/Compass/".SearchController::ROUTE."/".SearchController::SHOW_COURSE_CATEGORY."/".str_replace(' ', '_', $value["categoria_name"])?>"> <?php echo $value["categoria_name"]?></a>
                <?php endforeach?>     
              </div>
            </li>
          </ul> 

          <ul class="navbar-nav" id="leftsect">
            <li class="nav-item mt-2">
              <div style=" min-width: 135px;">
                <?php $aux = isset($_SESSION['User']); if(isset($_SESSION['User'])): ?>
                  <a class="nav-link" href="<?php echo "/7/Compass/".MessageController::ROUTE."/".MessageController::CHAT?>"><span class="badge badge-info m-1"><?php echo $countMsj?></span>Mensajes<i class="far fa-comments"></i></a>
                <?php else: ?> 
                  <a class ="d-inline" href="/7/Compass/login">Iniciar sesión</a>
                <?php endif?>
              </div>
             
            </li>
            <li class="nav-item text-center">
              <?php if (isset($_SESSION['User'])):?>
                <a class="nav-link dropdown-toggle" href="#" id="navbarPerfil" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?PHP echo'<img class="rounded-circle" src="data:image/jpg;base64,'.base64_encode(UsersController::getProfileImg($_SESSION["User"])).'" alt="perfil" id = "img-perfil">'?>
              
                  <?php echo $_SESSION["User"] ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarPerfil">
                  <a class="dropdown-item" href="<?php echo "/7/Compass/".UsersController::ROUTE."/".UsersController::SHOW_USER_PROFILE."/".$_SESSION["User"]?>">Mi Perfil</a>
                  <a class="dropdown-item" data-toggle="modal" data-target="#confirmSalida">Cerrar Sesión</a>
                </div>
              <?php else:?>
                <a class="nav-link mt-2 border-0 roundbutton" href="<?php echo Template::Route(UsersController::ROUTE, null);?>" style="min-width: 135px;">Crear Cuenta</a>
              <?php endif ?>
            </li>

          </ul> 
        </div>  
      </nav>
    </section>

    <div class="modal fade" id="confirmSalida" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" >Cerrar Sesión</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            ¿Seguro que desea Salir?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <a href="<?php echo Template::Route(LoginController::ROUTE, LoginController::LOGOUT);?>" class="btn btn-primary" id = "btn-CerrarSesion">Aceptar</a>
          </div>
        </div>
      </div>
    </div>

<?php $this->DeterminePage();?>

    <section class = "footer bg-dark text-white">
        <div class="container-fluid">
           <div class="row mb-4 justify-content-center">        
                <i class="fab fa-facebook-f m-3"></i>
                    
                <i class="fab fa-twitter m-3"></i>

                <i class="fab fa-google m-3"></i>

                <i class="fab fa-instagram m-3"></i>                  
           </div>

           <div class="row justify-content-end">
               <p>© 2021 Copyright: Compass</p>
           </div>

        </div>
    </section>


<script>
$("#searchform").submit( function( e ) {
    e.preventDefault();

    // I suggest making an Ajax request here to a processing script to check 
    // the validity of the data that was entered to the search. Presuming all 
    // went well and the data is available in your database, redirect the user:
    document.location = "/7/compass/search/index/" + $("#searchstring").val();
} );

</script>   

  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.js"  type="text/javascript"></script>
</body>
</html>