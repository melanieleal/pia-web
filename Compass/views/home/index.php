<head>
<link rel="stylesheet" href="/7/Compass/style/sectioncarouselcss.css">
<script src = "/7/Compass/JS/coursecarousel.js"></script>
</head>

<section class = "main">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="position: relative;width: 100%;overflow: hidden;min-height: 80.6vh;margin-top: 75px;">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" style="position: relative;width: 100%;overflow: hidden;min-height: 80.6vh;">
            <div class="carousel-item carousel-item2 active">
                <img class="d-block" src="../img/img2.jpg" alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                    <h1 class="bg-primary">BIENVENIDO A COMPASSS</h1>
                    <p>Lugar donde encontraras los mejores cursos online</p>
                </div>
            </div>
            <div class="carousel-item carousel-item2">
                <img class="d-block" src="../img/img3.png" alt="Second slide">
            </div>
            <div class="carousel-item carousel-item2">
                <img class="d-block" src="../img/img.jpg" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    </section>

   
    <div class="recentcourses">

      <div class="coursesectiontitle"><h1>Cursos recientes</h1></div>
      
      <div class="row justify-content-center">
        
      <?php foreach($recientes as $key => $value):?>
        <div class="col-md-2">
          <div class="card coursecard" style="width: 16rem; ">
              <div class="imgholder">
                <img class="card-img-top" src="data:image/jpg;base64,<?php echo $value["Portada"]?>" alt="Card image cap" width="200px" height="200px";>
              </div>
              <div class="card-body">
                <a href="/7/Compass/course/show/<?php echo $value["ID_Curso"]?>"> <h5 class="card-title mb-0"><?php echo $value["Titulo"]?></h5></a>
                <p class="card-text m-0"><small class="text-muted"><?php echo $value["Usuario"]?></small></p>             
                <p class="card-text text-justify" style="min-height: 10vh;"><?php echo $value["Descripcion_C"]?></p>
                <div class="text-center d-flex" style="justify-content: space-between;">
                  <p class="card-text text-info m-2">MX$ <?php echo $value["Costo"]?></p>
                  <a href="<?php echo "/7/Compass/course/show/".$value["ID_Curso"]?>" class="btn btn-primary">Ver</a>
                </div>
              </div>
          </div>
        </div>
      <?php endforeach?>
      </div>
        

    </div>

    <div class="bestsellingcourses">

      <div class="coursesectiontitle"><h1>Cursos más Vendidos</h1></div>

      <div class="row justify-content-center">
        
      <?php foreach($vendidos as $key => $value):?>
        <div class="col-md-2">
          <div class="card coursecard" style="width: 16rem; ">
              <div class="imgholder">
                <img class="card-img-top" src="data:image/jpg;base64,<?php echo $value["Portada"]?>" alt="Card image cap" width="200px" height="200px";>
              </div>
              <div class="card-body">
              <a href="/7/Compass/course/show/<?php echo $value["ID_Curso"]?>"> <h5 class="card-title mb-0"><?php echo $value["Titulo"]?></h5></a>
                <p class="card-text m-0"><small class="text-muted"><?php echo $value["Usuario"]?></small></p>             
                <p class="card-text text-justify" style="min-height: 10vh;"><?php echo $value["Descripcion_C"]?></p>
                <div class="text-center d-flex" style="justify-content: space-between;">
                  <p class="card-text text-info m-2">MX$ <?php echo $value["Costo"]?></p>
                  <a href="<?php echo "/7/Compass/course/show/".$value["ID_Curso"]?>" class="btn btn-primary">Ver</a>
                </div>
              </div>
          </div>
        </div>
      <?php endforeach?>
       
      </div>

    </div>

    <div class="bestvaluedcourses mb-5">

      <div class="coursesectiontitle"><h1>Cursos Mejor Calificados</h1></div>
    
      <div class="row justify-content-center">
        
      <?php foreach($calificados as $key => $value):?>
        <div class="col-md-2">
          <div class="card coursecard" style="width: 16rem; ">
              <div class="imgholder">
                <img class="card-img-top" src="data:image/jpg;base64,<?php echo $value["Portada"]?>" alt="Card image cap" width="200px" height="200px";>
              </div>
              <div class="card-body">
              <a href="/7/Compass/course/show/<?php echo $value["ID_Curso"]?>"> <h5 class="card-title mb-0"><?php echo $value["Titulo"]?></h5></a>
                <p class="card-text m-0"><small class="text-muted"><?php echo $value["Usuario"]?></small></p>             
                <p class="card-text text-justify" style="min-height: 10vh;"><?php echo $value["Descripcion_C"]?></p>
                <div class="text-center d-flex" style="justify-content: space-between;">
                  <p class="card-text text-info m-2">MX$ <?php echo $value["Costo"]?></p>
                  <a href="<?php echo "/7/Compass/course/show/".$value["ID_Curso"]?>" class="btn btn-primary">Ver</a>
                </div>
              </div>
          </div>
        </div>
      <?php endforeach?>
       
      </div>
    </div>  