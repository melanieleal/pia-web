
<head>
    <link rel="stylesheet" href="/7/Compass/style/login.css">
    <script src = "/7/Compass/JS/validlogin.js"></script>
</head>
<body>
   <section class = "main">

        <div class = "container" > 
            <form id="loginform"  method="post">         
                <div class = "form-group">

                    <h2 class="pl- pt-3" style="margin-top: 6rem;">Inicia Sesión en tu cuenta de Compass.</h2>
                    <hr class="bg-secondary">
                    <?php if(isset($args["result"]) && $args["result"] == false):?>
                        <div class="alert alert-danger" role="alert">
                            <strong>Usuario y/o contraseña incorrectos.</strong>
                        </div>  
                    <?php endif?>

                    <h2>Usuario</h2>
                    <input  name="username" type="text" class="form-control" placeholder="Enter usuario" id="tx-User">
                    <div class="ErrorUser">

                    </div>
                    <h2>Contraseña</h2>
                    <input type="password" name="password" class="form-control" placeholder="Enter contraseña" id="tx-PassW">   
                    <div class="ErrorPassW">
                        
                    </div>
                    <div class="p-3 m-auto text-center">
                        <button type="submit" class="btn btn-login m-auto"  id = "Entrar" >Entrar</button>  
                    </div>
                    
                </div>
                   
            </form>          
        </div>

        <div class="container">
           
            <div class="row text-center">
                <div class="col-12 " >
                    <h4 class = "d-inline">No tienes Cuenta?</h4>  
                    <a href="<?php echo Template::Route(UsersController::ROUTE, null);?>" class="btn btn-link" id = "registro">Registrate</a> 
                </div>
            </div>

        </div>
    </section>

</body>