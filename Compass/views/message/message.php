<head>
    <link rel="stylesheet" href="/7/Compass/style/chat.css">
    <script src = "/7/Compass/JS/moment.js"></script>
</head>

<section class = "main text-white">
    <div class="container p-5">
        <div class="row contentMsj">
            <div class="col-md-4 border-right border-light p-0">
                <div class="chatAll rounded-top p-2 d-flex justify-content-between">                                         
                    <h1 class="d-inline">Chat</h1>
                    <i class="far fa-edit m-1"></i>                      
                </div>
                <div class="contactos">
                    <?php if (isset($preview)):
                        foreach($preview as $key => $value):
                            if($value['remitente'] == $_SESSION['User'] || ($value['remitente'] == $_SESSION['User'] && $value['countMsj'] == 1)): ?>
                                <a href="<?php echo "/7/Compass/".MessageController::ROUTE."/".MessageController::CHAT."/".$value['destiny']?>" class=" contactoMsj text-white pl-3 pt-3 pb-3 d-flex border-info border-bottom">
                                    
                                    <?php if($value['foto'] == null):?>
                                        <img src="/7/Compass/img/usuario.png" alt="" class="rounded-circle mr-2" width="50px" height="50px">
                                    <?php else:?>
                                        <img src="data:image/jpg;base64, <?php echo base64_encode($value['foto'])?>" alt="" class="rounded-circle mr-2" width="50px" height="50px">
                                    <?php endif?>
                                    <div class="msj" >
                                        <h6 class = "text-capitalize"><?php echo $value['Nombre']?></h6>
                                        <p class="m-0" ><?php echo $value['message']?></p>
                                    </div>
                                    <div class="">
                                        <span class="small"><?php echo $value['fecha']?></span> 
                                        <?php if($value['remitente'] ==  $_SESSION['User']):?>
                                                <span class="badge badge-info m-1"><?php echo $value['countMsjSeen']?></span>
                                            <?php else:?>
                                                <span class="badge badge-info m-1">0</span>
                                        <?php endif;?>
                                    </div>  
                                </a>
                            <?php endif;?>
                    <?php endforeach;
                        endif;?>
                </div>
            </div>
            <div class="col-md-8 p-0">
                    <div class="ChatActivo d-flex p-3">
                        <?php if(isset($user)): 
                            if($user->getFoto() == null):?>
                                <img src="/7/Compass/img/usuario.png" class="rounded-circle mr-2" width="50px" height="50px" alt="">
                            <?php else:?>
                                <img src="data:image/jpg;base64, <?php echo base64_encode($user->getFoto())?>" class="rounded-circle mr-2" width="50px" height="50px" alt="">
                            <?php endif;?>
                            <h5 class="m-3 text-capitalize"><?php echo $user->getName()?> <?php echo $user->getLastName()?></h5>
                        <?php else: ?>
                            <h5 class="m-3 text-capitalize">Inicie una Nueva Conversación</h5>
                        <?php endif;?>
                        
                        
                    </div>
                    <div class="chat pb-3">
                        <?php if(isset($msj)):
                        foreach($msj as $key => $value):?>
                            <?php if ( $value["destiny"] != $_SESSION['User']):?>
                                <div class="row">
                                    <div class="col-md-3 offset-md-9">
                                        <div class="chatContentright mr-3 mt-3">
                                            <p class="p-1 pl-2 m-0"><?php echo $value["message"]?></p>
                                        </div>
                                        <div class="text-right mr-3">
                                            <p class="text-muted" style="font-size: 15px;"><?php echo $value["fecha"]?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php else:?>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="chatContentleft  ml-3 mt-3">
                                            <p class="p-1 pl-2 m-0"><?php echo $value["message"]?></p>
                                        </div>
                                        <div class="text-left ml-3">
                                            <p class="text-muted" style="font-size: 15px;"><?php echo $value["fecha"]?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endif?>
                        <?php endforeach;
                        endif;?>  
                    
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <form id="formMsj"  charset="utf-8">
                                <div class="responder p-2 d-flex justify-content-between rounded-bottom ">
                                    <input type="text" id="mensaje" name = "msjtext" placeholder="Aa" class="rounded bg-dark  border-0 ">
                                    <?php if(isset($user)):?>
                                        <input type="hidden" id="destiny" name="id" value="<?php echo $user->getid()?>" >
                                        <button type="button" class="border-0 bg-transparent text-white" id="btn_msj"><i class="fab fa-telegram-plane mr-3 p-2"></i></button>               
                                    <?php endif;?>
                                </div>
                            </form> 
                        </div>
                    </div>
                               
            </div>
        </div>
    </div>
</section>

<script>
    $("document").ready(function(){
        $("#btn_msj").click(function(){ 
            var day = new Date()
            var dayWrapper = moment(day); 
            var dayString = dayWrapper.format("YYYY-MM-D H:mm:ss"); 
            var txt = $("#mensaje").val();

            if($("#mensaje").val() == ""){
                $("#mensaje").removeClass("bg-dark");
                $("#mensaje").addClass("bg-danger");
            }
            else {
                $.ajax({
                    url: "/7/Compass/message/sent",
                    type:"POST",
                    data: $("#formMsj").serialize()
                })
                .done(function(response){
                    $(".chat").append(`
                    <div class="row">
                        <div class="col-md-3 offset-md-9">
                            <div class="chatContentright mr-3 mt-3">
                                <p class="p-1 pl-2 m-0">`+txt+`</p>
                            </div>
                            <div class="text-right mr-3">
                                <p class="text-muted" style="font-size: 15px;">`+dayString+`</p>
                            </div>
                        </div>
                    </div>
                    `)

                })
            }
        });
    });

 
</script>
