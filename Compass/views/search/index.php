
<section class = "main">
        <div class="container text-white">
            
            <div class="row">
                <div class="col-md-2">
                    <form action="<?php echo "/7/Compass/".SearchController::ROUTE."/".SearchController::SEARCH?>" method="post" class="mt-3">
                        <label class="d-block"><b>Busqueda Avanzada</b></label>
                        <input type="text" name="search">
                        <label class="d-block"><b>Usuario:</b></label>
                        <input type="text" name="user">
                        <label class="d-block"> <b>Categorias: </b></label>
                        
                        <select name="Categoria" id="Categoria" class="bg-dark text-white  mb-2 rounded" style="width: 100%;">
                            <option></option>
                            <?php foreach($category as $key => $value):?>
                                <option><?php echo $value["categoria_name"]?></option> 
                            <?php endforeach?>                               
                        </select>

                        <label class="mt-3 d-block"><b>Fecha de publicación</b></label>
                        <label>De</label>
                        <input type="date" name="FechaInicio" id="FechaInicio" class="bg-dark text-white  mb-2 ml-4 rounded d-block" style="width: 95%;">
                        <label>A</label>
                        <input type="date" name="FechaFin" id="FechaFin" class="bg-dark text-white  mb-2 ml-4 rounded d-block" style="width: 95%;"> 
                        
                        <div class="text-center">
                          <input type="submit" value="Buscar" class="btn btn-primary mb-3 mt-3">
                        </div>
                          
                    </form> 
                </div>
                <div class="col-md-10">
                    <div class="row mb-3 ml-3"  id="results">
                        <?php if($results):?>
                            <?php foreach($results as $key => $value):?>
                                <div class="col-md-12">
                                <a href="<?php echo "/7/Compass/course/show/".$value["ID_Curso"] ?>" class="text-white linkCard" style="text-decoration: none;">
                                    <div class="card col-mb-4 bg-dark mt-2" style="max-height: 200px;">
                                        <div class="row no-gutters" style="max-height: 200px;">
                                            <div class="col-md-4">
                                            <img src="data:image/jpg;base64,<?php echo $value["Portada"]?>" class="card-img" style="border-bottom-right-radius: 0; border-top-right-radius: 0;" alt="..." height="200px">
                                            </div>
                                            <div class="col-md-8 ">
                                            <div class="card-body">
                                                <h3 class="card-title mb-0"><?php echo $value["Titulo"]?></h3>
                                                <p class="card-text m-0"><small class="text-muted"><?php echo $value["Usuario"] ?></small></p>
                                                <p class="card-text" style="min-height: 70px;"><?php echo $value["Descripcion_C"]?></p>
                                                <div class="text-right">
                                                    <p class="card-text text-info">MX$ <?php echo $value["Costo"]?></p>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                </div>
                            <?php endforeach?>
                        <?php endif?>
                    </div>
                </div>
               <!--
                    <ul class="pagination pagination-sm justify-content-center">
                        <li class="page-item"><a href=""><i class="fas fa-angle-left"></i></a></li>
                        <li class="page-item ml-2 mr-2">2</li>
                        <li class="page-item"><a href=""><i class="fas fa-angle-right"></i></a></li>
                    </ul>-->
                </div>
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function(){  
            console.log( "ready!" );
            let searching="<?php echo $paths[2]?>";
            let formData = new FormData();
            formData.append("string", searching);
            
            $.ajax({
                url: "/7/Compass/search/results",
                type: "POST",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(response){
                  for ( var i = 0; i < response.length; i++ ) {
                     
                       $("#results").append( `
                    <div class="col-md-12">
                    <a href="/7/Compass/course/show/`+response[i].ID_Curso+`" class="text-white linkCard" style="text-decoration: none;">
                        <div class="card col-mb-4 bg-dark mt-2" style="max-height: 200px;">
                            <div class="row no-gutters" style="max-height: 200px;">
                                <div class="col-md-4">
                                <img src="data:image/jpg;base64,`+response[i].Portada+`" class="card-img" style="border-bottom-right-radius: 0; border-top-right-radius: 0;" alt="..." height="200px">
                                </div>
                                <div class="col-md-8 ">
                                <div class="card-body">
                                    <h3 class="card-title mb-0">`+response[i].Titulo+`</h3>
                                    <p class="card-text m-0"><small class="text-muted">`+response[i].Usuario+`</small></p>
                                    <p class="card-text" style="min-height: 70px;">`+response[i].Descripcion_C+`</p>
                                    <div class="text-right">
                                        <p class="card-text text-info">MX$ `+response[i].Costo+`</p>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    </div>
                       `);
                    }
                },
                error: function(response){
                    console.log("error");
                }

            });


        });

    </script>
    