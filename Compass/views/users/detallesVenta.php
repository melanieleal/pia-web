<head></head>

<section class = "main text-white">
        <div class="jumbotron jumbotron-fluid bg-dark">
            <div class="container">
                <h1 class="display-4">Ventas</h1>
            </div>
        </div>
        
        
        <div class="container">
           <div class="curso text-center">
              <h2 class="nameCurso text-capitalize"><?php echo $curso ?></h2>
           </div>

            <table class="table table-striped table-dark">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Alumno</th>
                        <th scope="col">Fechas de Inscripción</th>
                        <th scope="col">Nivel de Avance</th>
                        <th scope="col">Pago</th>
                        <th scope="col">Forma de Pago</th>
                    </tr>
                </thead>
                <?php if($detalles == true):
                  for($i = 0; $i<count($detalles); $i++):?>
                      <tbody>
                          <tr>
                              <th scope="row"><?php echo ($i+1)?></th>
                              <td class="text-capitalize"><?php echo $detalles[$i]["Nombre"] ?></td>
                              <td><?php echo $detalles[$i]["dia"],' ', substr($detalles[$i]["mes"],0,3), ' ', $detalles[$i]["year"]?></td>
                              <td>Nivel <?php echo $detalles[$i]["Nivel"] ?></td>
                              <td>MX$ <?php echo $detalles[$i]["Costo"] ?></td>
                              <?php if($detalles[$i]["Forma_pago"] == 0): ?>
                                <td>PayPal</td>
                              <?php else:?>
                                <td>Tarjeta de Débito</td>
                              <?php endif;?>
                          </tr>
                      </tbody>
                  <?php endfor;?>
                <?php else:?>
                  <tbody>
                    <tr>
                        <th scope="row">0</th>
                        <th>--</th>
                        <th>No</th>
                        <th>Hay</th>
                        <th>Alumnos</th>
                        <th>Inscritos</th>
                    </tr>
                  </tbody>
                <?php endif;?>
            </table>
           

            <div class="Total text-center mt-5 mb-5">
              <div class="row">
                <div class="col-md-4 pt-3">
                  <h5>Ingresos por Tarjeta</h5>
                  <?php $TotalT = 0;
                  foreach($detalles as $key => $value){
                    if($value["Forma_pago"] == 1)
                      $TotalT = $TotalT + $value["Costo"];
                  }?>
                  <h1>MX$ <?php echo $TotalT?></h1>
                </div>
                <div class="col-md-4 pt-3">
                  <h5>Ingresos por PayPal</h5>
                  <?php $TotalP = 0;
                  foreach($detalles as $key => $value){
                    if($value["Forma_pago"] == 0)
                      $TotalP = $TotalP + $value["Costo"];
                  }?>
                  <h1>MX$ <?php echo $TotalP?></h1>
                </div>
                <div class="col-md-4 border-left pt-3">
                  <h5>Total de Ingresos</h5>
                  <?php $Total = 0;
                  foreach($detalles as $key => $value){
                      $Total = $Total + $value["Costo"];
                  }?>
                  <h1>MX$ <?php echo $Total?></h1>
                </div>
              </div> 
            </div>
              
        </div>
        
</section>