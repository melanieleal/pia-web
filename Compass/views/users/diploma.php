<head>
<script src = "/7/Compass/JS/printThis.js"></script>    
</head>
<style type="text/css" media="print">
  @page { size: landscape;}
</style>

<div class="text-right mb-0" style="margin-top: 100px;">
    <button class="btn btn-info imprimir">Imprimir</button>
</div>  

<section class="main  mt-0">
    <div class="container-fluid">
        <div class="row bg-white">
            <div class="col-md-12" >
                <div class="bg-info" style="max-height: 5px; min-height: 5px;">
                </div>
                <div class="logo d-flex justify-content-center mt-5 ">
                    <h3 class = "display-1 d-inline">C </h3>
                    <img src="/7/Compass/img/compass.png" alt="logo" width="90px" height="90px">
                    <h3 class = "display-1 d-inline ">M P A S S</h3>
                </div>

                <?php if($Info == true):?>
                    <div class="info ml-5 mr-5 mt-1">
                        <h3>Otorga el presente</h3>
                        <div class="text-center" >
                            <h3 class="d-inline" style="font-size: 80px;">DIPLOMA</h3>
                            <img src="/7/Compass/img/diploma.jpg" alt=""  height="90px" width="90px" class="mb-5">     
                        </div>
                        
                        <div class="nombreUser d-flex mt-3">
                            <h3 class="d-inline">a:</h3>
                            <h2 class="text-info"><?php echo $Info["Nombre"] ?></h2>
                            <span></span>
                        </div> 
                        <hr>
                        <h4>por haber concluido satisfactoriamente el curso
                                <b><?php echo $Info["Titulo"] ?></b></h4> 
                            
                        <div class="firma text-right pt-5">
                            <h1><?php echo $Info["Creador"] ?></h1>
                            <p>Creador del Curso</p>
                        </div>  
                        <div class="fecha text-center pt-5">
                            <p style="font-size: 20px;">Monterrey, NL, a <?php echo $Info["Fecha_graduacion"] ?></p>
                        </div> 
                    </div>
                <?php endif;?>
                <div class="bg-info" style="max-height: 5px; min-height: 5px;">
                </div>
            </div> 
        </div>
    </div>
</section>

<script>
    $("document").ready(function(){
        $(".imprimir").click(function(){
            $(".main").printThis({
                importStyle: true,
            });
            return false;
        });
    })
</script>
