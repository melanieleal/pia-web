<head>
    <script src = "/7/Compass/JS/validcreatecourse.js"></script>
    <link rel="stylesheet" href="/7/Compass/style/editcourse.css">
    <link rel="stylesheet" href="/7/Compass/style/formsformat.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

</head>

<section class = "main">
    <form id="create_course_form" name="create_course" action="" method="post" enctype="multipart/form-data" class="row col-md-12">    
        <div class="container py-4"> 
            <div class="row text-white" id="createcourseform" >
                <div class="col-md-12 px-0">
                    <div class="row">
                        <div class="col-md-12 p-2" id="formtitle">   
                            <h2 class="frm_section_n text-white fw-bold">Editar curso:</h2>   
                            <h4 class="text-capitalize pl-1"><?php echo $cursoinfo[0]["Titulo"]?></h4>                         
                        </div>  
                    </div>
                    <div class="row cardbody">
                        <h4 class="col-md-12 frm_section_n fw-bold p-3" style="color: #91d5b3;">Información Básica</h4>
                        <div class="col-md-12 form_field_outer p-0 pb-3">
                            <div class="portada">
                                <img src="data:image/jpg;base64, <?php echo base64_encode($cursoinfo[0]["Portada"])?>" class="profilepicture" alt="">
                               
                            </div>
                            <div class="changeportad mb-4">
                                <input type="hidden" name="id_course" value="<?php echo $cursoinfo[0]["ID_Curso"]?>"/>
                                <label for="upload-photo" style="margin-left: auto; margin-right:auto; display:flex"><p><i class="fas fa-camera"></i>Cambiar portada</p></label>
                                <input type="file" name="photo" id="upload-photo" accept="image/*" onchange="validarImagen(this);"/>
                                <input type="hidden" name="photoori" value="<?php echo base64_encode($cursoinfo[0]["Portada"])?>"/>
                            </div>

                            

                            <div class="row form_field_outer_row ">
                                <p class="form-group col-md-12">Si desea cambiar el video de presentación, adjunte un nuevo video:</p>  
                                <div class="form-group col-md-2" style="display: flex; padding-left: 10px;">
                                    <h6>Video de Presentación: </h6>
                                   
                                </div>
                                
                                <div class="form-group col-md-7">
                                    <input type="file" name="videoP"  accept="video/*" id="">
                                    <input type="hidden" name="videoPori" value="<?php echo $cursoinfo[0]["Video_Presentacion"]?>"/>

                                </div>
                                    
                            </div>
                            <div class="row form_field_outer_row">
                                <div class="form-group col-md-2" style="display: flex; padding-left: 10px;">
                                    <h6>Nombre del curso: </h6>
                                </div>
                                <div class="form-group col-md-7">
                                    <input required name="course_name" type="text" class="form-control w_90"  id="course_name" placeholder="Curso" value="<?php echo $cursoinfo[0]["Titulo"]?>"/>
                                </div>
                                
                            </div>
                            <!--<div class="row form_field_outer_row mt-2">
                                <div class="form-group col-md-2" style="display: flex; padding-left: 10px;">
                                    <h6>Categoría: </h6>
                                </div>
                                <div class="form-group col-md-5">
                                    <select id="categories"  multiple="multiple" name="categorias[]">
                                    <?php //foreach($categoriessel as $key => $value):?>
                                        <option value="<?php //echo $value["categoria_name"]?>"> <?php //echo $value["categoria_name"]?></a>
                                    <?php //endforeach?> 
                                    
                                    </select>
                                </div>
                            </div>-->
                            <div class="row col-md-12 p-0 form-group mt-2">
                                <div class="form-group col-md-4" style="display: flex; padding-left:10px;">
                                    <h6>Descripción corta: </h6>
                                </div>
                                <div class="form-group col-md-12">
                                    <input required name="course_shortdesc" type="text" class="form-control w_90"  id="course_shortdesc" placeholder="Descripcion" value="<?php echo $cursoinfo[0]["Descripcion_C"]?>"/>
                                </div>  
                                <div class="form-group col-md-4 mt-2" style="display: flex; padding-left:10px;">
                                    <h6>Descripción detallada:  </h6>
                                </div>
                                <div class="col-md-12 pb-2">
                                    <textarea required name="course_largedesc" class="form-control" id="course_largedesc" rows="4"><?php echo $cursoinfo[0]["Descripcion_L"]?></textarea>
                                </div>

                            </div>
                            <div class="row col-md-12 p-0 form-group mt-2">
                                <div class="form-group col-md-2" style="display: flex; padding-left:10px;">
                                    <h6>Costo del Curso </h6>
                                </div>
                                <div class="form-group col-md-10">
                                    <input required name="costocourse" type="text" class="form-control col-2"  id="course_costo" placeholder="MX$" value="<?php echo $cursoinfo[0]["Costo"]?>" />
                                </div>  

                            </div>
                        </div>
                    </div>

                    <div class="row cardbody" id="levels">          
                        <h4 class="col-md-12 frm_section_n fw-bold p-3" style="color: #91d5b3;">Niveles</h4>
                        <?php  for($i=0; $i<count($niveles); $i++){
                        $auxi=$i+1;
                        echo '
                        <div id="levelssection" class="col-md-12">
                            <div class="accordionlevels accordion" id="accordionlevel0"  name="niveles">      
                                <!--Card de nivel-->
                                <div class="col-md-12 p-0 courselevel card">
                                    <div class="col-md-12 form_field_outer p-0">
                                        <div class="row form_field_outer_row">
                                        
                                            <div class="form-group col-md-12 pb-2 card-header" id="headinglvl0">
                                                <div class="form-group col-md-12 ">
                                                        <h4 class="text-white" style="padding-right: 25px;">Nivel '.$auxi.' </h4>
                                                        
                                                </div>
                                            </div>

                                            <div id="lvl0" class="collapse show col-md-12 p-0" aria-labelledby="headinglvl0" data-parent="#accordionlevels">
                                                <div class="card-body">
                                                    <div class="form-group col-md-12 pt-3">
                                                        <div class="col-md-12 form_field_outer p-0">
                                                            <div class="row form_field_outer_row pb-3">    
                                                                <div class="form-group" style="display: flex; padding-left: 10px;">
                                                                    <h6 class="text-white m-0 p-0">Nombre del nivel:  </h6>                                 
                                                                </div>    
                                                                <div class="form-group col-md-5 p-0">
                                                                    <input name="level_title[]" id="lvl0level_title" type="text" class="form-control w_90" placeholder="nivel" value="'.$niveles[$i]["Nombre"].'" style="margin:0 10px;"/>
                                                                    <input name="level_id[]" type="hidden" value="'.$niveles[$i]["ID_Nivel"].'"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <section class="formsection pb-3 col-md-12 p-0">
                                                        <div class="col-md-12 form_field_outer">
                                                            <div class="row form_field_outer_row pb-2">    
                                                                <div class="form-group" style="display: flex; padding-left: 10px;">
                                                                    <h6 class="text-white m-0 p-0">Multimedia del nivel:  </h6>                                 
                                                                </div>    
                                                            </div>
                                                        </div>

                                                        <div class="col-md-11 form_field_outer m-auto p-0">    
                                                            <div class="noth">
                                                                <div class="accordion" id="accordionlvl0">
                                                                    <div class="card">
                                                                        <div class="card-header p-0 p-0" id="headingOne0">
                                                                            <h5 class="mb-0">
                                                                               <h3>Video</h3>
                                                                            </h5>
                                                                        </div>
                                                                
                                                                        <div id="collapseOne0" class="collapse show " aria-labelledby="headingOne0" data-parent="#accordionlvl0">
                                                                            <div class="card-body">
                                                                                <div class="levelcontent form-group col-md-12">  
                                                                                    <div class="levelmedia" id="lvl0vids">
                                                                                        <div class="form-group col-md-12">
                                                                                            <p>Si desea cambiar el video del nivel, suba un nuevo video.</p>
                                                                                            <input type="file" name="videolevel[]" id="lvl0videolevel"  accept="video/*">
                                                                                            <input type="hidden" name="videolevelOr[]" value="'.base64_encode($niveles[$i]["Video"]).'"/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>  
                                                                </div>
                                                            </div> 
                                                        </div>  
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                     
                            </div>
                            
                        </div>
                        
                        ';}?>
                        


                    </div>

                   <!--<div class="row col-md-12 cardbody m-auto" style="border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;"> 
                        <input type="submit" class="roundbutton border-0 text-white mt-3 mb-3 ml-auto mr-auto"> Editar </input>
                    </div>-->
                    <div class="row col-md-12 cardbody m-auto" style="border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;"> 
                        <button class="roundbutton border-0 text-white mt-3 mb-3 ml-auto mr-auto"> Editar </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
