<head>
    <script src = "/7/Compass/JS/validregisterprofile.js" type="text/javascript"> </script>
    <link rel="stylesheet" href="/7/Compass/style/profile.css">
</head>

<section class = "main">
        <div class = "container text-center text-white" >    
            <div class="valInfo">

            </div>
            <div class="row" id="logo">
                <div class="col-12  p-3" >
                    <h1>Editar perfil</h1>
                </div>          
            </div>   
        </div>

        <div class = "container  text-white " > 
            <form method="post" id="edit-user" style="margin: 0 20rem;" enctype="multipart/form-data">         
                <div class = "form-group">

                    <div class="avatar">
                        <?php if($user->getFoto() == null):?>
                            <img src="/7/Compass/img/usuario.png" class="profilepicture" alt="">
                        <?php else:?>
                            <img src="data:image/jpg;base64, <?php echo base64_encode($user->getFoto())?>" class="profilepicture" alt="">
                        <?php endif?>
                        <label for="upload-photo"><p><i class="fas fa-camera"></i></p></label>
                        <input type="file" name="photo" id="upload-photo" accept="image/*" onchange="validarImagen(this);"/>
                    </div>

                    <h5>Nombre</h5>
                    <input  name="NombreE" type="text" class="form-control m-2" placeholder="Nombre" value="<?php echo $user->getName()?>">

                    <h5>Apellido</h5>
                    <input  name="ApellidoE" type="text" class="form-control m-2" placeholder="Apellido" value="<?php echo $user->getLastName()?>">
                    
                    <h5>Contraseña</h5>
                    <input   id="passwordA"  name="passwordA" type="password" class="form-control m-2" placeholder="Enter contraseña" value="<?php echo $user->getPass()?>">

                    <h5>Nueva contraseña</h5>
                    <h6 class="pl-2">No rellenar si no desea actualizar su contraseña</h6>
                    <input   id="passwordE"  name="passwordE" type="password" class="form-control m-2" placeholder="Enter contraseña">

                     <h5>Confirmar nueva contraseña</h5>
                    <input  id="passwordconfirmE" name="passwordconfirmE" type="password" class="form-control m-2" placeholder="Confirmar contraseña">
                    
                    <h5>Género</h5>
                    <select name="generoE" id="Genre" class="rounded m-2 form-control" style="width: 100%;"> 
                        <?php foreach($genero as $key => $genero): ?>
                            <?php if($genero['value'] == $user->getGenero()): ?>
                            <option selected ><?= $genero['value'] ?></option>
                        <?php else: ?>
                            <option><?= $genero['value'] ?></option>
                        <?php endif; ?>
                        <?php endforeach; ?>                             
                    </select>

                    <h5>Fecha de nacimiento</h5>
                    <input name="NacimientoE" type="date" class="form-control m-2" placeholder="Fecha" value="<?php echo $user->getDateNac()?>">                   
                      
                </div>    

                <div class="btnEdit text-center">
                    <input type="hidden" name="id" value="<?php echo $user->getId()?>">
                    <button type="submit" class="btn roundbutton"  id = "EditarPerfil">Editar</button> 
                </div>
                
            </form>          
        </div>
    </section>

  