<head>    
    <link rel="stylesheet" href="/7/Compass/style/register.css">
    <script src = "/7/Compass/JS/validarUsuario.js"></script>
</head>

   <section class = "main">
        <div class = "container" > 
            
            

            <form  id="userform" method="post">         
                <div class = "form-group text-white">
                    <h2 class="pl- pt-3 text-left" style="margin-top: 6rem;">Regístrate y comienza a aprender.</h2>
                    <hr class="bg-secondary">
                    
                    <?php if(isset($args["result"]) && $args["result"] == false):?>
                        <div class="alert alert-danger" role="alert">
                            <strong>Ingrese un usuario que no exista.</strong>
                        </div>  
                    <?php endif?>

                    <h2>Rol de Usuario</h2>
                    <select name="rol" id="Genre" class="rounded" style="width: 20%;">
                        <option>Estudiante</option> 
                        <option>Escuela</option>                                
                    </select>

                    <h2>Usuario</h2>
                    <input name="username" id="User" type="text" class="form-control col-4" placeholder="Enter usuario">

                    <!--Contraseña-->
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-1"></div>
                        <div class="col-4"><h2>Contraseña</h2>  </div>
                        <div class="col-2"></div>
                        <div class="col-4"><h2>Confirmar contraseña</h2> </div>
                        <div class="col-1"></div>
                    </div> 
                    <div class="row">
                        <div class="col-1"></div>
                        <div class="col-4 p-0">
                            <input  type="password" name="password" id="password" class="form-control" placeholder="Enter contraseña">
                        </div>
                        <div class="col-2"></div>
                        <div class="col-4 p-0">
                            <input type="password" name="passwordconfirm" class="form-control" placeholder="Confirmar contraseña">
                        </div>                                                            
                        <div class="col-1"></div>
                    </div>
                    
                    <!--Nombre y Apellido-->
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-1"></div>
                        <div class="col-4"><h2>Nombre</h2>  </div>
                        <div class="col-2"></div>
                        <div class="col-4"><h2>Apellido</h2> </div>
                        <div class="col-1"></div>
                    </div> 
                    <div class="row">
                        <div class="col-1"></div>
                        <div class="col-4 p-0">
                            <input name="Nombre" type="text" class="form-control" placeholder="Nombre">
                        </div>
                        <div class="col-2"></div>
                        <div class="col-4 p-0">
                            <input name="Apellido" type="text" class="form-control" placeholder="Apellido">
                        </div>
                        <div class="col-1"></div>
                    </div>


                    <!--género y Nacimiento-->
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-1"></div>
                        <div class="col-4"><h2>Género</h2>  </div>
                        <div class="col-2"></div>
                        <div class="col-4"><h2>Fecha de nacimiento</h2> </div>
                        <div class="col-1"></div>
                    </div> 
                    <div class="row" >
                        <div class="col-1"></div>
                        <div class="col-4 p-0">
                            <select name="genero" id="Genre" class="rounded" style="width: 100%;">
                                <option>Femenino</option> 
                                <option>Masculino</option> 
                                <option>Otro</option>                                
                            </select>
                        </div>
                        <div class="col-2"></div>
                        <div class="col-4 p-0">
                            <input name="Nacimiento" type="date" class="form-control" placeholder="Fecha">
                        </div>                      
                        <div class="col-1"></div>
                    </div>
                    <!--E-mail-->

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-4"></div>
                        <div class="col-4"><h2>Correo electrónico</h2></div>
                        <div class="col-4"></div>
                    </div> 
                    
                    <input name="email" type="email" class="form-control col-6" placeholder="Ingresar correo electrónico">
                    <h3 class="errorCorreo"><i class="fas fa-exclamation"></i> Ingrese un correo válido</h3>
                    <h3 class="errorCorreo"><i class="fas fa-exclamation"></i> Solo puede contar con letras, '.' y  '_'</h3>
                      
                </div>    

                <div class="text-center">
                    <button type="submit" class="btn"  id = "Enter">Registrar</button>
                </div>

            </form>          
        </div>

        <div class="container">

            <div class ="row text-center">
                <div class="col-12">
                    <h4 class = "d-inline">Ya tienes Cuenta?</h4>  
                    <a href= "<?php echo Template::Route(LoginController::ROUTE, null);?>" class="btn btn-link" id = "registro">Inicia Sesión</a> 
                </div>
            </div>
        </div>

    </section>

    