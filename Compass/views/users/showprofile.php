<head>
   <link rel="stylesheet" href="/7/Compass/style/profile.css">
</head>


<?php if ($user->getRol()=="Escuela"):?>
<section class = "main">
    <div class="profcontenthd">
     <div class="profileheader"> 
        <div class="avatar">
            <?php if($user->getFoto() == null):?>
                <img src="/7/Compass/img/usuario.png" class="profilepicture" alt="">
            <?php else:?>
                <img src="data:image/jpg;base64, <?php echo base64_encode($user->getFoto())?>" class="profilepicture" alt="">
            <?php endif?>
        </div>

        <div class="userinfo">  
            <p class="name d-inline" id="p-username"><?php echo $user->getName()?> <?php echo $user->getLastName()?></p>
            
            <a href="<?php echo "/7/Compass/".MessageController::ROUTE."/".MessageController::CHAT."/".$user->getId()?>" class="d-inline pl-1 text-white"><i class="far fa-comment-dots"></i></a>

            <p id="profilename"><?php echo $user->getId()?></p>
            <p id="userdescription"><?php echo $user->getRol()?></p>
            
            <?php if (isset($_SESSION["User"])):?>
                <?php if ($user->getId()==$_SESSION["User"]):?>
                    <form action="<?php echo "/7/Compass/".UsersController::ROUTE."/".UsersController::EDIT_USER."/".$user->getId()?>" method="get">
                        <button class = "sqrrndbtn btn btn-primary">Editar perfil</button>  
                    </form>
                    <form action="<?php echo "/7/Compass/".CourseController::ROUTE."/".CourseController::INSERT?>" method="post" >
                        <button class = "sqrrndbtn btn btn-primary  mt-1">Crear Curso</button>  
                    </form>
                <?php endif ?>     
            <?php endif ?> 
          
          
               
        </div>
     </div>
    </div>
    
    <div class="profcontent" id="subnav">
      <div class="subnavprof">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Cursos</a>
              </li>
             <?PHP if($_SESSION["User"]==$user->getId()){
                  echo '
              <li class="nav-item">
                <a class="nav-link" id="sells-tab" data-toggle="tab" href="#sells" role="tab" aria-controls="sells" aria-selected="false">Ventas</a>
              </li>';}?>
            </ul>
            
      </div>
     
  </div>
    <div class="profcontent">  
    <section class = "text-white">
      <div class="tab-content subnavprof" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
           
            <div class="container mb-3">
                <div class="row mb-3"> 
                    <?php if ($coursesby == true):?>
                        <?php  for($i = 0; $i<count($coursesby); $i++){
                            echo '
                            <div class="col-md-4 cursoDiv" >
                               
                                <div class="card m-3" style="background-color: rgba(17, 17, 17, 0.452);">
                                <a href="/7/Compass/course/show/'.$coursesby[$i]["ID_Curso"].'">
                                <img class="card-img-top" src="data:image/jpg;base64,'.$coursesby[$i]["Portada"].' " alt="'.$coursesby[$i]["Titulo"].'" width="351px" height="180px"> </a>
                                    
                                    <div class="card-body">
                                        <a href="/7/Compass/course/show/'.$coursesby[$i]["ID_Curso"].'">
                                        <h5 class="card-title">'.$coursesby[$i]["Titulo"].'</h5>
                                        </a>
                                        ';
                                        if ($user->getId()==$_SESSION["User"]){
                                        echo'<div class="text-right">
                                        <a href="/7/Compass/course/suspendercurso/'.$coursesby[$i]["ID_Curso"].'" class="bg-danger border-0 p-1"><i class="far fa-trash-alt text-white p-1" aria-hidden="true"></i></a>
                                        <a href="/7/Compass/course/edit/'.$coursesby[$i]["ID_Curso"].'" class="bg-info border-0 p-1"><i class="far fa-edit text-white p-1" aria-hidden="true"></i></a>
                                        </div>';};
                                        echo '</div>
                                    <div class="card-footer">
                                        <small class="text-muted">Creacion '.$coursesby[$i]["fechaCreate"].'</small>
                                    </div>
                                </div>
                            
                            </div>
                        ';
                        }?>
                    <?php else:?>
                        <h4 class="text-muted">No tiene Cursos en existencia</h4>
                    <?php endif;?>
                </div>

            </div>
        </div>

        <?PHP if($_SESSION["User"]==$user->getId()):?>
               
            <div class="tab-pane fade show" id="sells" role="tabpanel" aria-labelledby="sells-tab">
            
            <table class="table table-striped table-dark">
                <thead>
                    <tr>
                        <th scope="col">Curso</th>
                        <th scope="col">Cant. Alumnos Inscritos</th>
                        <th scope="col">Nivel Promedio</th>
                        <th scope="col">Ingresos por Curso</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($ventas as $key => $value):?>
                    <tr>
                        <td><a href="<?php echo "/7/Compass/".UsersController::ROUTE."/".UsersController::DETALLE_VENTA."/".$value['ID_Curso']."/".str_replace(' ', '_', $value['Titulo'])?>" class="a-DetallesCurso"><?php echo $value['Titulo']?></a></td>
                        <td><?php echo $value['CantUsuarios']?></td>
                        <?php if($value['NivelProm'] == null):?>
                            <td>- -</td>
                        <?php else:?>
                            <td>Nivel <?php echo $value['NivelProm']?></td>
                        <?php endif;?>
                        <td>MX$ <?php echo $value['Ganancias']?></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
                </table>
                <div class="Total text-right mt-5 mb-5">
                    <h5>Total de Ingresos:</h5>
                    <?php $total = null;
                    foreach( $ventas as $key => $value): 
                        $total = $total + $value['Ganancias']?>
                    <?php endforeach;?>
                    <h1>MX$ <?php echo $total?></h1>
                </div>

        
            </div>
        <?php endif ?>

     </div>   
    </section>
</section>
<?php endif ?>


<?php if ($user->getRol()=="Estudiante"):?>
<section class = "main">
    <div class="profcontenthd">
     <div class="profileheader"> 
        <div class="avatar">
            <?php if($user->getFoto() == null):?>
                <img src="/7/Compass/img/usuario.png" class="profilepicture" alt="">
            <?php else:?>
                <img src="data:image/jpg;base64, <?php echo base64_encode($user->getFoto())?>" class="profilepicture" alt="">
            <?php endif?>
        </div>

        <div class="userinfo">  
            <p class="name d-inline" id="p-username"><?php echo $user->getName()?> <?php echo $user->getLastName()?></p>
            <a href="<?php echo "/7/Compass/".MessageController::ROUTE."/".MessageController::CHAT."/".$user->getId()?>" class="d-inline pl-1 text-white"><i class="far fa-comment-dots"></i></a>

            <p id="profilename" class=""><?php echo $user->getId()?></p>
            <p id="userdescription"><?php echo $user->getRol()?></p>
            <?php if (isset($_SESSION["User"])):?>
            <?php if ($user->getId()==$_SESSION["User"]):?>
                <form action="<?php echo "/7/Compass/".UsersController::ROUTE."/".UsersController::EDIT_USER."/".$user->getId()?>" method="get">
                    <button class = "sqrrndbtn btn btn-primary">Editar perfil</button>  
                </form>
            <?php endif ?>     
            <?php endif ?> 
        </div>
     </div>
    </div>
    
    <div class="profcontent" id="subnav">
        <div class="subnavprof">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Historial</a>
                </li>
              </ul>
              
        </div>
       
    </div> 
    <div class="profcontent">  
    <section class = "text-white">
      <div class="tab-content subnavprof" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            
                    <div class="container mb-3">
                        <div class="row mb-3"> 
                        <?php if($coursesof == true):?>
                            <?php for($i=0; $i<count($coursesof); $i++){ echo 
                            ' <div class="col-md-4">
                                <div class="card m-3" style="background-color: rgba(17, 17, 17, 0.452);">
                                    <img class="card-img-top" src="data:image/jpg;base64,'.$coursesof[$i]["Portada"].'" alt="'.$coursesof[$i]["Titulo"].'" width="351px" height="180px">
                                    <div class="card-body">';
                                    if($coursesof[$i]["Activo"] == 0){
                                        echo '<h5 class="card-title text-danger">'.$coursesof[$i]["Titulo"].'</h5>
                                              <p class="text-muted text-center">Curso suspendido</p>';
                                    }
                                    else{
                                      echo  '<a href="/7/Compass/course/show/'.$coursesof[$i]["ID_Curso"].'"> 
                                            <h5 class="card-title">'.$coursesof[$i]["Titulo"].'</h5>
                                        </a>';
                                    }
                                    echo
                                    '   <div class="progress">
                                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width:'.$coursesof[$i]["Progreso"].'%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">'.$coursesof[$i]["Progreso"].'%</div>
                                        </div>
                                        <p class="mt-3 mb-0">Fecha de Inicio del Curso:</p>
                                        <h6 class="text-right">'.$coursesof[$i]["Fecha_Inscripcion"].'</h6>
                                        <p class="mt-3 mb-0">Fecha en que Término el Curso:</p>';
                                        if($coursesof[$i]["Fecha_graduacion"] == null){
                                            echo '  <h6 class="text-right">-- -- ----</h6>';
                                        }else{ if($_SESSION["User"]==$user->getId()){
                                            echo '  <form action="/7/Compass/usuario/diploma/'.$_SESSION["User"].'/'.$coursesof[$i]["ID_Curso"].'" method= "post" class = "text-right">
                                                        <h6 class="d-inline">'.$coursesof[$i]["Fecha_graduacion"].'</h6>
                                                        <button class="btn btn-info"><i class="fas fa-graduation-cap"></i></button>
                                                    </form>';}
                                        }
                                        echo'
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted">Última vez '.$coursesof[$i]["Ultima_visita"].'</small>
                                    </div>
                                </div>   
                            </div>';
                        }?>
                        <?php else:?>
                            <h4 class="text-muted">No ha comprado Cursos</h4>
                        <?php endif;?>
                        <!--<div class="row">
                          <ul class="pagination pagination-sm  justify-content-center">
                            <li class="page-item"><a href=""><i class="fas fa-angle-left"></i></a></li>
                            <li class="page-item ml-2 mr-2">2</li>
                            <li class="page-item"><a href=""><i class="fas fa-angle-right"></i></a></li>
                          </ul>
                        </div>-->
                    </div>
                </div>
 
     </div>   
    </section>
</section>
<?php endif ?>