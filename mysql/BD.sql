drop Database Compass;
Create Database Compass;

use Compass;

create table usuario(
	ID_Usuario varchar(30) not null primary key COMMENT 'Primary key del Usuario',
    Nombre varchar(30) not null COMMENT 'Nombre del Usuario',
    Apellido varchar(30) not null COMMENT 'Apellido del Usuario',
    Fecha_Nacimiento date not null COMMENT 'Fecha en la que nació el usuario',
    Email varchar(50) not null COMMENT 'Correo electronico del Usuario',
    Pass varchar(8) not null COMMENT 'Contraseña con la que el usuario podrá ingresar',
    Foto mediumblob COMMENT 'Foto de perfil del usuario',
    Fecha_Alta datetime default NOW() COMMENT 'Fecha en la que el usuario se registro',
    Fecha_Mod datetime COMMENT 'Fecha en la que el usuario modifico sus datos',
    Activo bit default 1 COMMENT 'Bit para saber si el usuario existe o elimino su cuenta',
    fk_rol int not null COMMENT 'Foreign key del rol que el usuario tiene en el sitio web',
    fk_genero int not null COMMENT 'Foreign key del género del usuario'
);

create table genero(
	ID_genero int auto_increment primary key COMMENT 'Id del género, generado automáticamente',
    Nombre varchar(30) not null COMMENT 'Nombre del género con el cual se identifica una persona'
);

create table rol(
	ID_rol int auto_increment primary key COMMENT 'Id del rol, generado automáticamente',
    Nombre varchar(30) not null COMMENT 'nombre del rol que tendrá el usuario'
);

create table curso(
	ID_Curso int auto_increment primary key COMMENT 'id del curso, generado automáticamente',
    Titulo varchar(50) not null COMMENT 'título con el cual se va a mostrar el curso',
    Descripcion_C varchar(100) not null COMMENT 'descripción corta de que va a tratar el curso',
    Descripcion_L varchar(300) COMMENT 'descripción detallada del curso',
    Costo decimal(6,2) not null COMMENT 'costo del curso completo',
    Portada mediumblob COMMENT 'foto de portada con la que se mostrará el curso',
    Video_Presentacion mediumblob,
    Activo bit default 1 COMMENT 'bit para saber si el curso esta disponible o lo eliminaron',
    fk_Creador varchar(30) not null COMMENT 'foreign key del usuario que creo el curso',
    fechaCreate datetime not null default now()
);

drop table if exists categoria_curso;
create table categoria_curso(
	categoria_name varchar(30) not null COMMENT 'categoria que tendrá el curso',
    fk_Curso int not null COMMENT 'foreign key del curso'
);

drop table if exists nivel;
create table nivel(
	ID_Nivel int auto_increment primary key COMMENT 'id del nivel, generado automáticamente',
    Nombre varchar(50) not null COMMENT 'nombre que tendrá el nivel',
    Video mediumblob COMMENT 'video con el que contará cada nivel',
    fk_Curso int not null COMMENT 'foreign key del curso',
    Activo bit default 1
);

/*LINKS */
drop table if exists media;
create table media(
	ID_Medialinks int auto_increment primary key COMMENT 'id generado automáticamente',
    Titulo varchar(50) COMMENT 'título que tendrá el archivo',
    Link varchar(200)  COMMENT 'link del archivo',
    fk_id_level int  COMMENT 'foreign key del nivel'
);

/*ARCHIVOS y PDF*/
drop table if exists media_files;
create table media_files(
	ID_media_files		int primary key auto_increment not null COMMENT 'id del archivo, generado automáticamente',
    file_				mediumblob not null COMMENT 'archivo de video',
    fk_id_level			int not null COMMENT 'foreign key del nivel',
	tipo 				varchar(30) COMMENT 'indica el tipo de archivo que es el registro',
    filename 			varchar(150)
);

/*IMAGES*/
create table media_image(
	ID_media_image		int primary key auto_increment not null COMMENT 'id del archivo de imagen, generado automáticamente',
    image				blob not null COMMENT 'archivo de imagen',
    descrip 			varchar (150) COMMENT 'descripcion con la que contará la imagen',
    fk_id_level			int not null COMMENT 'foreign key del nivel'
);

/*---------------------------------------------------------------- 08/10 ----------------------------------------------------------------*/
/*TEXTOS*/
create table media_text(
	ID_media_text int primary key auto_increment not null COMMENT 'id generado automáticamente',
	Titulo		 		varchar(50) COMMENT 'titulo que tendrá',
    Descripcion 		varchar(600) COMMENT 'información',
    fk_id_level			int not null COMMENT 'foreign key del nivel'
);


create table alumn_course_info(
	ID_Informacion			int not null auto_increment primary key COMMENT 'id generado automáticamente',
	Fecha_Inscripcion		datetime default now() COMMENT 'fecha en la que un usuario se inscribió al curso',
	Progreso				float default 0 COMMENT 'proceso que lleva el usuario del curso',
	Ultima_visita			datetime default now() COMMENT 'fecha en la que accedió por ultima vez al curso',
	Forma_pago				int not null COMMENT 'forma en la que pago el curso el usuario',
	Fecha_graduacion		datetime null COMMENT 'fecha en la que el usuario termino el curso'
);


create table alumn_course(
	Id_alumncourse int auto_increment primary key COMMENT 'id generado automáticamente',
	diploma			blob COMMENT 'diploma que se crea al terminar el curso', 
	fk_Usuario		varchar(30) not null COMMENT 'foreign key del usuario',
	fk_curso		int not null COMMENT 'foreign key del curso',
	fk_Informacion 	int not null COMMENT 'foreign key de la informacion del alumno sobre el curso',
    activo 			bit default 1 COMMENT 'bit para saber si esta activo'
);
delimiter $$
drop table if exists message;$$
create table message(
	ID_message			int primary key auto_increment COMMENT 'id del mensaje, generado automáticamente',
    message				varchar (660) COMMENT 'texto del mensaje',
    fecha				datetime default now() COMMENT 'fecha en la que se envió el mensaje',
    remitente 			varchar(30) not null COMMENT 'id del usuario quien envió el mensaje',
    destiny				varchar(30) not null COMMENT 'id del usuario a quien va dirigido el mensaje',
    seen 				bit default 0
);

create table review(
	ID_review			int not null auto_increment primary key COMMENT 'id de la review que da el usuario, generado automáticamente ',
    review				varchar(200) COMMENT 'comentario del usuario hacía el curso',
    score				bool not null COMMENT 'calificación que le da el usuario al curso',
    date_r				datetime not null default now() COMMENT 'fecha en la que se hizo el comentario',
    fk_Usuario			varchar(30) not null COMMENT 'foreign key del usuario',
    fk_course			int not null COMMENT 'foreign key del curso'
);

/*alter table FK*/
Alter table usuario add constraint FK_Usuario_id_rol foreign key (fk_rol) references rol(ID_rol);
Alter table usuario add constraint FK_Usuario_id_genero foreign key (fk_genero) references genero(ID_genero); 

Alter table curso add constraint FK_usuario_creador foreign key (fk_Creador) references usuario(ID_Usuario); 

Alter table categoria_curso add constraint FK_Categoria_Curso_ID_Curso foreign key (fk_Curso) references curso(ID_Curso);

Alter table nivel add constraint FK_Nivel_ID_Curso foreign key (fk_Curso) references curso(ID_Curso);

Alter table media add constraint fk_media foreign key (fk_id_level) references nivel(ID_Nivel); 

Alter table media_files add constraint fk_media_files_level foreign key (fk_id_level) references nivel(ID_Nivel); 

Alter table media_image add constraint fk_media_image_level foreign key (fk_id_level) references nivel(ID_Nivel); 

Alter table media_text add constraint fk_media_text_level foreign key (fk_id_level) references nivel(ID_Nivel); 

Alter table alumn_course add constraint fk_alumn_course_user foreign key (fk_Usuario) references usuario(ID_Usuario); 
Alter table alumn_course add constraint fk_alumn_course_course foreign key (fk_curso) references curso(ID_Curso); 
Alter table alumn_course add constraint fk_alumn_course_info foreign key (fk_Informacion) references alumn_course_info(ID_Informacion); 

Alter table message add constraint fk_message_remit foreign key (remitente) references usuario(ID_Usuario); 
Alter table message add constraint fk_message_dest foreign key (destiny) references usuario(ID_Usuario); 

Alter table review add constraint fk_review_user foreign key (fk_Usuario) references usuario(ID_Usuario); 
Alter table review add constraint fk_review_course foreign key (fk_course) references curso(ID_Curso); 

/*alter table FK*/

insert into rol(nombre) values("Estudiante"), ("Escuela");
insert into genero(nombre) values("Femenino"), ("Masculino"), ("Otro");



