Delimiter $$
drop procedure if exists SP_Busqueda;$$
create procedure SP_Busqueda(
	op 			varchar(10),
	busqueda  	varchar(100),
    categoria 	varchar(50),
    usuario 	varchar(50),
    fechaI 		date,
    fechaF 		date
)
begin
	
	if op = "search" then
		select
			C.ID_Curso,
			C.Titulo,
			C.Descripcion_C,
			C.Portada,
            C.Costo,
            concat(U.Nombre, " ", U.Apellido) as "Usuario"
		from curso C
        inner join usuario U
        on U.ID_Usuario = C.fk_Creador
		where if(busqueda Is null, 1, Titulo like concat('%',busqueda,'%')) and C.Activo = 1;
	end if;
    
    if op = "avanzada" then
		select 
			C.ID_Curso,
			C.Titulo,
			C.Descripcion_C,
			C.Portada,
            C.Costo,
            concat(U.Nombre, " ", U.Apellido) as "Usuario"
		from curso C
        inner join categoria_curso CC
        on CC.fk_Curso = C.ID_Curso
        inner join usuario U
        on U.ID_Usuario = C.fk_Creador
        
		where if(busqueda Is null, 1, C.Titulo like concat('%',busqueda,'%')) 
        and if(IFNULL(categoria, -1) = -1, 1, CC.categoria_name = categoria)
        and if(IFNULL(usuario, -1) = -1, 1, U.ID_Usuario = usuario)
        and if(fechaI is null, 1, date(C.FechaCreate) >= fechaI)
        and if(fechaF is null, 1, date(C.FechaCreate) <= fechaF)
        and C.Activo = 1
        group by C.ID_Curso;
    end if;
    
    if op = 'category' then         
        select ID_Curso, Titulo, Descripcion_C, Portada, Costo, Usuario, categoria_name from V_CourseCategory where categoria_name = categoria;
    end if;
end$$

drop view if exists V_CourseCategory;
create view V_CourseCategory
as
select 
C.ID_Curso,
C.Titulo,
C.Descripcion_C,
C.Portada,
C.Costo,
concat(U.Nombre, " ", U.Apellido) as "Usuario",
CC.categoria_name
from curso C
inner join categoria_curso CC
on CC.fk_Curso = C.ID_Curso 
inner join usuario U
on U.ID_Usuario = C.fk_Creador
where C.Activo = 1;
        
