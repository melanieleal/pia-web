delimiter $$
drop procedure if exists getCoursesFrom;$$
create procedure getCoursesFrom(
	opc  varchar(1),
    SP_ID_Usuario varchar(30)
)
begin 
	if opc="A" then 
		select C.ID_Curso,
        C.Titulo,
        C.Descripcion_C,
        C.Costo,
        C.Portada,
        C.fechaCreate,
        C.Activo, 
        A_C.fk_Informacion, 
        AC_I.Fecha_Inscripcion, 
        AC_I.Progreso, 
        AC_I.Ultima_visita, 
        AC_I.Fecha_graduacion,
        C.Activo
        from curso C
        join alumn_course A_C on A_C.fk_Usuario=SP_ID_Usuario AND C.ID_Curso = A_C.fk_curso
        join alumn_course_info AC_I on AC_I.ID_Informacion = A_C.fk_Informacion
        where ID_Curso in (select fk_curso from alumn_course where fk_Usuario = SP_ID_Usuario);
	end if;
    
    if opc="E" then 
		select ID_Curso,
        Titulo,
        Descripcion_C,
        Costo,
        Portada, 
        fechaCreate, 
        Activo 
        from curso where fk_Creador = SP_ID_Usuario and Activo = 1;
	end if;
end;$$

delimiter $$
drop procedure if exists getCourseInfo;$$
create procedure getCourseInfo(
	opc				varchar(3),
    sp_id_curso		int,
    sp_usuario		varchar (30),
    sp_selected_lvl	int
)
begin 
	/*inscrito*/
	if opc="1" then
		select count(fk_Usuario)as AlumnoInscrito from alumn_course where fk_Usuario=sp_usuario and fk_curso=sp_id_curso;
    end if;
   
   /*info curso*/
    if opc="2" then
		select ID_Curso,Titulo,Descripcion_C,Descripcion_L,Costo,Portada,Video_Presentacion,Activo,fk_Creador, (select count(score) from review where fk_course=sp_id_curso and score=1) as likes from curso where ID_Curso=sp_id_curso;
	end if;
	/*niveles de curso*/
     if opc="3" then
		select ID_Nivel,Nombre,fk_Curso from nivel 
        where ID_Nivel in (select ID_Nivel from nivel where fk_Curso=sp_id_curso and Activo=1);
	end if;
    
    /*niveles de curso editar*/
     if opc="3ed" then
		select ID_Nivel,Nombre,Video, fk_Curso from nivel 
        where ID_Nivel in (select ID_Nivel from nivel where fk_Curso=sp_id_curso and Activo=1);
	end if;
    
    
    if opc="3IN" then
		select ID_Nivel,Nombre,Video,fk_Curso from nivel 
        where ID_Nivel=(select id_nivel from nivel where fk_curso=sp_id_curso and Activo=1 limit 1 offset sp_selected_lvl) and fk_Curso=sp_id_curso;
	end if;
    
    /*reseñas de curso*/
    if opc="4" then
		select ID_review, review, score, date_r, fk_Usuario, fk_course, Nombre, Foto from reviewsview
        where ID_review in (select ID_review from review where fk_course = sp_id_curso);
    end if;
end;$$

/*VIEW REVIEWS*/
drop view if exists reviewsview;
create view reviewsview as
select r.ID_review,r.review,r.score,r.date_r,r.fk_Usuario,r.fk_course, concat(u.Nombre, ' ', u.Apellido) as Nombre, u.Foto as Foto from review r
join usuario u 
on r.fk_Usuario = u.ID_Usuario;
        
delimiter $$
drop procedure if exists getlevelmultimedia;$$
create procedure getlevelmultimedia(
	opc				varchar(3),
	sp_ID_Nivel int(11)
)
begin 
	/*files*/
	if opc="f" then
		select ID_media_files,file_,fk_id_level,tipo,filename from media_files where  fk_id_level=sp_ID_Nivel and tipo!="application/pdf";
    end if;
    
	/*pdfs*/
    if opc="pdf" then
		select ID_media_files,file_,fk_id_level,tipo,filename from media_files where fk_id_level=sp_ID_Nivel and tipo="application/pdf";
    end if;
    
	/*links*/
	if opc="url" then
		select ID_Medialinks,Titulo,Link from media  where fk_id_level=sp_ID_Nivel;
    end if;
    
        
    /*image*/
	if opc="img" then
		select ID_media_image,image,descrip from media_image where fk_id_level=sp_ID_Nivel;
    end if;
    
    /*text*/
	if opc="txt" then
		select ID_media_text,Titulo,Descripcion from media_text where fk_id_level=sp_ID_Nivel;
    end if;
    
   end;$$

DELIMITER $$
drop procedure if exists downloadfile;$$
create procedure downloadfile(
id INT	
)
begin
select file_,tipo,filename from media_files where ID_media_files=id;
 end;$$


DELIMITER $$
drop procedure if exists sp_UpdateAlumnCourseInfo;$$
create procedure sp_UpdateAlumnCourseInfo(
	op varchar(10),
	sp_curso int,
    sp_usuario varchar(30)
)
begin
    if op = "UVisita" then
		set @fk_info = (select fk_Informacion from alumn_course where fk_curso = sp_curso and  fk_Usuario = sp_usuario);
        
        update alumn_course_info
        set Ultima_visita = now()
        where ID_Informacion = @fk_info;
    end if;
    
    if op = "Graduacion" then
		set @fk_info = (select fk_Informacion from alumn_course where fk_curso = sp_curso and  fk_Usuario = sp_usuario);
        
        update alumn_course_info
        set Fecha_graduacion = now()
        where ID_Informacion = @fk_info;
    end if;
end$$

DELIMITER $$
drop procedure if exists sp_Progreso;$$
create procedure sp_Progreso(
	sp_nivel int,
    sp_curso int,
    sp_usuario varchar(30)
)
begin
		set @fk_info = (select fk_Informacion from alumn_course where fk_curso = sp_curso and  fk_Usuario = sp_usuario);
        
		update alumn_course_info 
		set Progreso = if(Progreso >= (sp_nivel * ProgresoC(sp_curso)), Progreso, if(Progreso = 100, 100, Progreso + ProgresoC(sp_curso))),
			Fecha_graduacion = if(Progreso = 100, now(), null)
		where ID_Informacion =  @fk_info;
        
end$$

DELIMITER $$
drop procedure if exists sp_suspenderCurso;$$
create procedure sp_suspenderCurso(
	sp_id_curso int
)
begin
	update curso
	set Activo = 0
	where ID_Curso = sp_id_curso;
end$$

DELIMITER $$
DROP TRIGGER IF EXISTS delete_archivos;$$
CREATE TRIGGER delete_archivos
AFTER UPDATE
ON nivel FOR EACH ROW
BEGIN
	if(NEW.Activo = 0) THEN
		DELETE FROM media WHERE fk_id_level = NEW.ID_Nivel;
        DELETE FROM media_files WHERE fk_id_level = NEW.ID_Nivel;
        DELETE FROM media_image WHERE fk_id_level = NEW.ID_Nivel;
        DELETE FROM media_text WHERE fk_id_level = NEW.ID_Nivel;
	END IF;
END;$$

DELIMITER $$
DROP TRIGGER IF EXISTS suspender_curso;$$
CREATE TRIGGER suspender_curso
AFTER UPDATE
ON curso FOR EACH ROW
BEGIN
	if(NEW.Activo = 0) THEN
		UPDATE nivel
			SET Activo = 0
		WHERE fk_Curso = NEW.ID_Curso;	
        
        DELETE from categoria_curso where fk_Curso = NEW.ID_Curso;
	END IF;
END;$$


DELIMITER $$
DROP PROCEDURE IF EXISTS getcategoriescourse;$$
CREATE procedure getcategoriescourse(
sp_id int 
)
BEGIN
select categoria_name from categoria_curso where fk_Curso=sp_id;

END;$$


DELIMITER $$
DROP PROCEDURE IF EXISTS sp_editcurso;$$
CREATE procedure sp_editcurso(
uservalidate varchar(30),
sp_ID_Curso int(11),
sp_Titulo varchar(50),
sp_Descripcion_C varchar(100),
sp_Descripcion_L varchar(300),
sp_Costo decimal(6,2),
sp_Portada mediumblob,
sp_Video_Presentacion mediumblob
)
BEGIN
update curso
	set
        Titulo=sp_Titulo, 
		Descripcion_C=sp_Descripcion_C, 
		Descripcion_L=sp_Descripcion_L, 
		Costo=sp_Costo, 
		Portada=sp_Portada, 
		Video_Presentacion=sp_Video_Presentacion
	where fk_Creador=uservalidate and ID_Curso=sp_ID_Curso;
END;$$

DELIMITER $$
DROP PROCEDURE IF EXISTS sp_editlvl;$$
CREATE procedure sp_editlvl(
uservalidate varchar(30),
sp_ID_Nivel int(11),
sp_Nombre varchar(50),
sp_Video mediumblob)
BEGIN
	if((select fk_Creador from curso where ID_Curso in(select fk_Curso from nivel where ID_Nivel=sp_ID_Nivel))=uservalidate) then
		update nivel
			set
				Nombre=sp_Nombre,
				Video=sp_Video
				where ID_Nivel=sp_ID_Nivel;
	end if;
END;$$
68/71/825/4086

select * from curso;
select * from nivel;


select ((select fk_Creador from curso where ID_Curso in(select fk_Curso from nivel where ID_Nivel=57))="meal");