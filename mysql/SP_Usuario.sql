use compass;

DELIMITER $$
drop procedure if exists SP_Usuario; 
create procedure SP_Usuario(
	Opcion				varchar(10),
	spID_Usuario 		varchar(30),
    spNombre 			varchar(30),
    spApellido 			varchar(30),
    spFecha_Nacimiento 	date,
    spEmail 			varchar(50),
    spPass 				varchar(8),
    spFoto			 	mediumblob,
    spfk_rol 			int,
	spfkGenero 			int
)
begin
	If opcion = "Register" then
		Insert into Usuario(ID_Usuario, Nombre, Apellido, Fecha_Nacimiento, Email, Pass, fk_rol, fk_genero) 
        values(spID_Usuario,spNombre,spApellido,spFecha_Nacimiento,spEmail,spPass,spfk_rol, spfkGenero);
    end if;
    
    If opcion = "GetUser" then 
		Select
			ID_Usuario,
            Nombre,
            Apellido,
            Fecha_Nacimiento,
            Email,
            Pass,
            Foto,
            Activo,
            fk_rol,
            fk_genero
		from usuario
        where ID_Usuario = spID_Usuario;
    end if;
    
     If opcion = "EditUser" then 
		Update usuario
        set
				Nombre = spNombre,
				Apellido = spApellido,
				Fecha_Nacimiento = spFecha_Nacimiento,
				Pass = spPass,
				Foto = spFoto,
                Fecha_Mod = now(),
				fk_genero = spfkGenero
        where ID_Usuario = spID_Usuario;
    end if;
end$$