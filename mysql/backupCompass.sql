-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: localhost    Database: compass
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumn_course`
--

DROP TABLE IF EXISTS `alumn_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alumn_course` (
  `Id_alumncourse` int NOT NULL AUTO_INCREMENT COMMENT 'id generado automáticamente',
  `diploma` blob COMMENT 'diploma que se crea al terminar el curso',
  `fk_Usuario` varchar(30) NOT NULL COMMENT 'foreign key del usuario',
  `fk_curso` int NOT NULL COMMENT 'foreign key del curso',
  `fk_Informacion` int NOT NULL COMMENT 'foreign key de la informacion del alumno sobre el curso',
  `activo` bit(1) DEFAULT b'1' COMMENT 'bit para saber si esta activo',
  PRIMARY KEY (`Id_alumncourse`),
  KEY `fk_alumn_course_user` (`fk_Usuario`),
  KEY `fk_alumn_course_course` (`fk_curso`),
  KEY `fk_alumn_course_info` (`fk_Informacion`),
  CONSTRAINT `fk_alumn_course_course` FOREIGN KEY (`fk_curso`) REFERENCES `curso` (`ID_Curso`),
  CONSTRAINT `fk_alumn_course_info` FOREIGN KEY (`fk_Informacion`) REFERENCES `alumn_course_info` (`ID_Informacion`),
  CONSTRAINT `fk_alumn_course_user` FOREIGN KEY (`fk_Usuario`) REFERENCES `usuario` (`ID_Usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumn_course`
--

LOCK TABLES `alumn_course` WRITE;
/*!40000 ALTER TABLE `alumn_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `alumn_course` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER= CURRENT_USER*/ /*!50003 TRIGGER `createinfoinscripcion` BEFORE INSERT ON `alumn_course` FOR EACH ROW BEGIN
  insert into alumn_course_info(Fecha_Inscripcion, Progreso, Ultima_visita,Forma_pago, Fecha_graduacion) values(now(), 0, now(), null, null);
	set New.fk_Informacion = (select max(ID_Informacion) from alumn_course_info);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `alumn_course_info`
--

DROP TABLE IF EXISTS `alumn_course_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alumn_course_info` (
  `ID_Informacion` int NOT NULL AUTO_INCREMENT COMMENT 'id generado automáticamente',
  `Fecha_Inscripcion` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'fecha en la que un usuario se inscribió al curso',
  `Progreso` float DEFAULT '0' COMMENT 'proceso que lleva el usuario del curso',
  `Ultima_visita` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'fecha en la que accedió por ultima vez al curso',
  `Forma_pago` int NOT NULL COMMENT 'forma en la que pago el curso el usuario',
  `Fecha_graduacion` datetime DEFAULT NULL COMMENT 'fecha en la que el usuario termino el curso',
  PRIMARY KEY (`ID_Informacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumn_course_info`
--

LOCK TABLES `alumn_course_info` WRITE;
/*!40000 ALTER TABLE `alumn_course_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `alumn_course_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria_curso`
--

DROP TABLE IF EXISTS `categoria_curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categoria_curso` (
  `categoria_name` varchar(30) NOT NULL COMMENT 'categoria que tendrá el curso',
  `fk_Curso` int NOT NULL COMMENT 'foreign key del curso',
  KEY `FK_Categoria_Curso_ID_Curso` (`fk_Curso`),
  CONSTRAINT `FK_Categoria_Curso_ID_Curso` FOREIGN KEY (`fk_Curso`) REFERENCES `curso` (`ID_Curso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria_curso`
--

LOCK TABLES `categoria_curso` WRITE;
/*!40000 ALTER TABLE `categoria_curso` DISABLE KEYS */;
/*!40000 ALTER TABLE `categoria_curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `curso` (
  `ID_Curso` int NOT NULL AUTO_INCREMENT COMMENT 'id del curso, generado automáticamente',
  `Titulo` varchar(50) NOT NULL COMMENT 'título con el cual se va a mostrar el curso',
  `Descripcion_C` varchar(100) NOT NULL COMMENT 'descripción corta de que va a tratar el curso',
  `Descripcion_L` varchar(300) DEFAULT NULL COMMENT 'descripción detallada del curso',
  `Costo` decimal(6,2) NOT NULL COMMENT 'costo del curso completo',
  `Portada` mediumblob COMMENT 'foto de portada con la que se mostrará el curso',
  `Video_Presentacion` mediumblob,
  `Activo` bit(1) DEFAULT b'1' COMMENT 'bit para saber si el curso esta disponible o lo eliminaron',
  `fk_Creador` varchar(30) NOT NULL COMMENT 'foreign key del usuario que creo el curso',
  `fechaCreate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_Curso`),
  KEY `FK_usuario_creador` (`fk_Creador`),
  CONSTRAINT `FK_usuario_creador` FOREIGN KEY (`fk_Creador`) REFERENCES `usuario` (`ID_Usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=CURRENT_USER*/ /*!50003 TRIGGER `suspender_curso` AFTER UPDATE ON `curso` FOR EACH ROW BEGIN
	if(NEW.Activo = 0) THEN
		UPDATE nivel
			SET Activo = 0
		WHERE fk_Curso = NEW.ID_Curso;	
        
        DELETE from categoria_curso where fk_Curso = NEW.ID_Curso;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `genero` (
  `ID_genero` int NOT NULL AUTO_INCREMENT COMMENT 'Id del género, generado automáticamente',
  `Nombre` varchar(30) NOT NULL COMMENT 'Nombre del género con el cual se identifica una persona',
  PRIMARY KEY (`ID_genero`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero`
--

LOCK TABLES `genero` WRITE;
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
INSERT INTO `genero` VALUES (1,'Femenino'),(2,'Masculino'),(3,'Otro');
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `media` (
  `ID_Medialinks` int NOT NULL AUTO_INCREMENT COMMENT 'id generado automáticamente',
  `Titulo` varchar(50) DEFAULT NULL COMMENT 'título que tendrá el archivo',
  `Link` varchar(200) DEFAULT NULL COMMENT 'link del archivo',
  `fk_id_level` int DEFAULT NULL COMMENT 'foreign key del nivel',
  PRIMARY KEY (`ID_Medialinks`),
  KEY `fk_media` (`fk_id_level`),
  CONSTRAINT `fk_media` FOREIGN KEY (`fk_id_level`) REFERENCES `nivel` (`ID_Nivel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_files`
--

DROP TABLE IF EXISTS `media_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `media_files` (
  `ID_media_files` int NOT NULL AUTO_INCREMENT COMMENT 'id del archivo, generado automáticamente',
  `file_` mediumblob NOT NULL COMMENT 'archivo de video',
  `fk_id_level` int NOT NULL COMMENT 'foreign key del nivel',
  `tipo` varchar(30) DEFAULT NULL COMMENT 'indica el tipo de archivo que es el registro',
  `filename` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`ID_media_files`),
  KEY `fk_media_files_level` (`fk_id_level`),
  CONSTRAINT `fk_media_files_level` FOREIGN KEY (`fk_id_level`) REFERENCES `nivel` (`ID_Nivel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_files`
--

LOCK TABLES `media_files` WRITE;
/*!40000 ALTER TABLE `media_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_image`
--

DROP TABLE IF EXISTS `media_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `media_image` (
  `ID_media_image` int NOT NULL AUTO_INCREMENT COMMENT 'id del archivo de imagen, generado automáticamente',
  `image` blob NOT NULL COMMENT 'archivo de imagen',
  `descrip` varchar(150) DEFAULT NULL COMMENT 'descripcion con la que contará la imagen',
  `fk_id_level` int NOT NULL COMMENT 'foreign key del nivel',
  PRIMARY KEY (`ID_media_image`),
  KEY `fk_media_image_level` (`fk_id_level`),
  CONSTRAINT `fk_media_image_level` FOREIGN KEY (`fk_id_level`) REFERENCES `nivel` (`ID_Nivel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_image`
--

LOCK TABLES `media_image` WRITE;
/*!40000 ALTER TABLE `media_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_text`
--

DROP TABLE IF EXISTS `media_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `media_text` (
  `ID_media_text` int NOT NULL AUTO_INCREMENT COMMENT 'id generado automáticamente',
  `Titulo` varchar(50) DEFAULT NULL COMMENT 'titulo que tendrá',
  `Descripcion` varchar(600) DEFAULT NULL COMMENT 'información',
  `fk_id_level` int NOT NULL COMMENT 'foreign key del nivel',
  PRIMARY KEY (`ID_media_text`),
  KEY `fk_media_text_level` (`fk_id_level`),
  CONSTRAINT `fk_media_text_level` FOREIGN KEY (`fk_id_level`) REFERENCES `nivel` (`ID_Nivel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_text`
--

LOCK TABLES `media_text` WRITE;
/*!40000 ALTER TABLE `media_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `message` (
  `ID_message` int NOT NULL AUTO_INCREMENT COMMENT 'id del mensaje, generado automáticamente',
  `message` varchar(660) DEFAULT NULL COMMENT 'texto del mensaje',
  `fecha` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'fecha en la que se envió el mensaje',
  `remitente` varchar(30) NOT NULL COMMENT 'id del usuario quien envió el mensaje',
  `destiny` varchar(30) NOT NULL COMMENT 'id del usuario a quien va dirigido el mensaje',
  `seen` bit(1) DEFAULT b'0',
  PRIMARY KEY (`ID_message`),
  KEY `fk_message_remit` (`remitente`),
  KEY `fk_message_dest` (`destiny`),
  CONSTRAINT `fk_message_dest` FOREIGN KEY (`destiny`) REFERENCES `usuario` (`ID_Usuario`),
  CONSTRAINT `fk_message_remit` FOREIGN KEY (`remitente`) REFERENCES `usuario` (`ID_Usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nivel`
--

DROP TABLE IF EXISTS `nivel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nivel` (
  `ID_Nivel` int NOT NULL AUTO_INCREMENT COMMENT 'id del nivel, generado automáticamente',
  `Nombre` varchar(50) NOT NULL COMMENT 'nombre que tendrá el nivel',
  `Video` mediumblob COMMENT 'video con el que contará cada nivel',
  `fk_Curso` int NOT NULL COMMENT 'foreign key del curso',
  `Activo` bit(1) DEFAULT b'1',
  PRIMARY KEY (`ID_Nivel`),
  KEY `FK_Nivel_ID_Curso` (`fk_Curso`),
  CONSTRAINT `FK_Nivel_ID_Curso` FOREIGN KEY (`fk_Curso`) REFERENCES `curso` (`ID_Curso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nivel`
--

LOCK TABLES `nivel` WRITE;
/*!40000 ALTER TABLE `nivel` DISABLE KEYS */;
/*!40000 ALTER TABLE `nivel` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=CURRENT_USER*/ /*!50003 TRIGGER `delete_archivos` AFTER UPDATE ON `nivel` FOR EACH ROW BEGIN
	if(NEW.Activo = 0) THEN
		DELETE FROM media WHERE fk_id_level = NEW.ID_Nivel;
        DELETE FROM media_files WHERE fk_id_level = NEW.ID_Nivel;
        DELETE FROM media_image WHERE fk_id_level = NEW.ID_Nivel;
        DELETE FROM media_text WHERE fk_id_level = NEW.ID_Nivel;
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `review` (
  `ID_review` int NOT NULL AUTO_INCREMENT COMMENT 'id de la review que da el usuario, generado automáticamente ',
  `review` varchar(200) DEFAULT NULL COMMENT 'comentario del usuario hacía el curso',
  `score` tinyint(1) NOT NULL COMMENT 'calificación que le da el usuario al curso',
  `date_r` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'fecha en la que se hizo el comentario',
  `fk_Usuario` varchar(30) NOT NULL COMMENT 'foreign key del usuario',
  `fk_course` int NOT NULL COMMENT 'foreign key del curso',
  PRIMARY KEY (`ID_review`),
  KEY `fk_review_user` (`fk_Usuario`),
  KEY `fk_review_course` (`fk_course`),
  CONSTRAINT `fk_review_course` FOREIGN KEY (`fk_course`) REFERENCES `curso` (`ID_Curso`),
  CONSTRAINT `fk_review_user` FOREIGN KEY (`fk_Usuario`) REFERENCES `usuario` (`ID_Usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `reviewsview`
--

DROP TABLE IF EXISTS `reviewsview`;
/*!50001 DROP VIEW IF EXISTS `reviewsview`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `reviewsview` AS SELECT 
 1 AS `ID_review`,
 1 AS `review`,
 1 AS `score`,
 1 AS `date_r`,
 1 AS `fk_Usuario`,
 1 AS `fk_course`,
 1 AS `Nombre`,
 1 AS `Foto`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
  `ID_rol` int NOT NULL AUTO_INCREMENT COMMENT 'Id del rol, generado automáticamente',
  `Nombre` varchar(30) NOT NULL COMMENT 'nombre del rol que tendrá el usuario',
  PRIMARY KEY (`ID_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'Estudiante'),(2,'Escuela');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `ID_Usuario` varchar(30) NOT NULL COMMENT 'Primary key del Usuario',
  `Nombre` varchar(30) NOT NULL COMMENT 'Nombre del Usuario',
  `Apellido` varchar(30) NOT NULL COMMENT 'Apellido del Usuario',
  `Fecha_Nacimiento` date NOT NULL COMMENT 'Fecha en la que nació el usuario',
  `Email` varchar(50) NOT NULL COMMENT 'Correo electronico del Usuario',
  `Pass` varchar(8) NOT NULL COMMENT 'Contraseña con la que el usuario podrá ingresar',
  `Foto` mediumblob COMMENT 'Foto de perfil del usuario',
  `Fecha_Alta` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha en la que el usuario se registro',
  `Fecha_Mod` datetime DEFAULT NULL COMMENT 'Fecha en la que el usuario modifico sus datos',
  `Activo` bit(1) DEFAULT b'1' COMMENT 'Bit para saber si el usuario existe o elimino su cuenta',
  `fk_rol` int NOT NULL COMMENT 'Foreign key del rol que el usuario tiene en el sitio web',
  `fk_genero` int NOT NULL COMMENT 'Foreign key del género del usuario',
  PRIMARY KEY (`ID_Usuario`),
  KEY `FK_Usuario_id_rol` (`fk_rol`),
  KEY `FK_Usuario_id_genero` (`fk_genero`),
  CONSTRAINT `FK_Usuario_id_genero` FOREIGN KEY (`fk_genero`) REFERENCES `genero` (`ID_genero`),
  CONSTRAINT `FK_Usuario_id_rol` FOREIGN KEY (`fk_rol`) REFERENCES `rol` (`ID_rol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `v_coursecategory`
--

DROP TABLE IF EXISTS `v_coursecategory`;
/*!50001 DROP VIEW IF EXISTS `v_coursecategory`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_coursecategory` AS SELECT 
 1 AS `ID_Curso`,
 1 AS `Titulo`,
 1 AS `Descripcion_C`,
 1 AS `Portada`,
 1 AS `Costo`,
 1 AS `Usuario`,
 1 AS `categoria_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_coursesc`
--

DROP TABLE IF EXISTS `v_coursesc`;
/*!50001 DROP VIEW IF EXISTS `v_coursesc`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_coursesc` AS SELECT 
 1 AS `ID_Curso`,
 1 AS `Titulo`,
 1 AS `Descripcion_C`,
 1 AS `Portada`,
 1 AS `Costo`,
 1 AS `Usuario`,
 1 AS `Calificacion`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_coursesr`
--

DROP TABLE IF EXISTS `v_coursesr`;
/*!50001 DROP VIEW IF EXISTS `v_coursesr`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_coursesr` AS SELECT 
 1 AS `ID_Curso`,
 1 AS `Titulo`,
 1 AS `Descripcion_C`,
 1 AS `Portada`,
 1 AS `Costo`,
 1 AS `Usuario`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_coursesv`
--

DROP TABLE IF EXISTS `v_coursesv`;
/*!50001 DROP VIEW IF EXISTS `v_coursesv`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_coursesv` AS SELECT 
 1 AS `ID_Curso`,
 1 AS `Titulo`,
 1 AS `Descripcion_C`,
 1 AS `Portada`,
 1 AS `Costo`,
 1 AS `Usuario`,
 1 AS `Cant_Usuarios`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_detallesventasc`
--

DROP TABLE IF EXISTS `v_detallesventasc`;
/*!50001 DROP VIEW IF EXISTS `v_detallesventasc`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_detallesventasc` AS SELECT 
 1 AS `Titulo`,
 1 AS `Nombre`,
 1 AS `dia`,
 1 AS `mes`,
 1 AS `yearI`,
 1 AS `Costo`,
 1 AS `Nivel`,
 1 AS `Progreso`,
 1 AS `ID_Curso`,
 1 AS `Forma_pago`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_progresoc`
--

DROP TABLE IF EXISTS `v_progresoc`;
/*!50001 DROP VIEW IF EXISTS `v_progresoc`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_progresoc` AS SELECT 
 1 AS `Progreso`,
 1 AS `cant`,
 1 AS `ID_Curso`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'compass'
--

--
-- Dumping routines for database 'compass'
--
/*!50003 DROP FUNCTION IF EXISTS `countMsj` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER FUNCTION `countMsj`(sp_remitente varchar(30), sp_destiny varchar(30)) RETURNS int
    DETERMINISTIC
begin 
	declare countM  int;
    set countM = (select count(ID_message) from message where  (remitente = sp_remitente and destiny = sp_destiny));
	return countM;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `countMsjseen` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER FUNCTION `countMsjseen`(sp_remitente varchar(30), sp_destiny varchar(30)) RETURNS int
    DETERMINISTIC
begin 
	declare countS int;
	set countS = (select count(seen) from message where (remitente = sp_remitente and destiny = sp_destiny) and seen = 0);
	return countS;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `HasReviewed` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER FUNCTION `HasReviewed`(usuario varchar(30), curso int(11)) RETURNS int
    DETERMINISTIC
BEGIN 
 DECLARE booleano INT;
 SET booleano = (select count(ID_review) from review where fk_Usuario=usuario and fk_course=curso);
 RETURN (booleano);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProgresoC` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER FUNCTION `ProgresoC`(sp_curso int) RETURNS float
    DETERMINISTIC
begin 
	declare progreso  float;
    set progreso = (select 100/(select count(ID_Nivel) from nivel where fk_Curso = sp_curso));
	return progreso;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ProgresoporNivel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER FUNCTION `ProgresoporNivel`(sp_progreso float, sp_curso int) RETURNS float
    DETERMINISTIC
begin 
	declare progreso  float;
    set progreso = (Truncate(sp_progreso/ (100/(select count(ID_Nivel) from nivel where fk_Curso = sp_curso)), 0));
	return progreso;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ultimoMsj` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER FUNCTION `ultimoMsj`(sp_remitente varchar(30)) RETURNS int
    DETERMINISTIC
begin 

	declare fechaU datetime;
	declare id  int;
    set fechaU = (select max(fecha) from message where remitente = sp_remitente or destiny = sp_remitente);
	set id = (select id_message from message where fecha = fechaU);
	return id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `downloadfile` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `downloadfile`(
id INT	
)
begin
select file_,tipo,filename from media_files where ID_media_files=id;
 end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getcategoriescourse` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `getcategoriescourse`(
sp_id int 
)
BEGIN
select categoria_name from categoria_curso where fk_Curso=sp_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCourseInfo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `getCourseInfo`(
	opc				varchar(3),
    sp_id_curso		int,
    sp_usuario		varchar (30),
    sp_selected_lvl	int
)
begin 
	/*inscrito*/
	if opc="1" then
		select count(fk_Usuario)as AlumnoInscrito from alumn_course where fk_Usuario=sp_usuario and fk_curso=sp_id_curso;
    end if;
   
   /*info curso*/
    if opc="2" then
		select ID_Curso,Titulo,Descripcion_C,Descripcion_L,Costo,Portada,Video_Presentacion,Activo,fk_Creador, (select count(score) from review where fk_course=sp_id_curso and score=1) as likes from curso where ID_Curso=sp_id_curso;
	end if;
	/*niveles de curso*/
     if opc="3" then
		select ID_Nivel,Nombre,fk_Curso from nivel 
        where ID_Nivel in (select ID_Nivel from nivel where fk_Curso=sp_id_curso and Activo=1);
	end if;
    
    /*niveles de curso editar*/
     if opc="3ed" then
		select ID_Nivel,Nombre,Video, fk_Curso from nivel 
        where ID_Nivel in (select ID_Nivel from nivel where fk_Curso=sp_id_curso and Activo=1);
	end if;
    
    
    if opc="3IN" then
		select ID_Nivel,Nombre,Video,fk_Curso from nivel 
        where ID_Nivel=(select id_nivel from nivel where fk_curso=sp_id_curso and Activo=1 limit 1 offset sp_selected_lvl) and fk_Curso=sp_id_curso;
	end if;
    
    /*reseñas de curso*/
    if opc="4" then
		select ID_review, review, score, date_r, fk_Usuario, fk_course, Nombre, Foto from reviewsview
        where ID_review in (select ID_review from review where fk_course = sp_id_curso);
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCoursesFrom` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `getCoursesFrom`(
	opc  varchar(1),
    SP_ID_Usuario varchar(30)
)
begin 
	if opc="A" then 
		select C.ID_Curso,
        C.Titulo,
        C.Descripcion_C,
        C.Costo,
        C.Portada,
        C.fechaCreate,
        C.Activo, 
        A_C.fk_Informacion, 
        AC_I.Fecha_Inscripcion, 
        AC_I.Progreso, 
        AC_I.Ultima_visita, 
        AC_I.Fecha_graduacion,
        C.Activo
        from curso C
        join alumn_course A_C on A_C.fk_Usuario=SP_ID_Usuario AND C.ID_Curso = A_C.fk_curso
        join alumn_course_info AC_I on AC_I.ID_Informacion = A_C.fk_Informacion
        where ID_Curso in (select fk_curso from alumn_course where fk_Usuario = SP_ID_Usuario);
	end if;
    
    if opc="E" then 
		select ID_Curso,
        Titulo,
        Descripcion_C,
        Costo,
        Portada, 
        fechaCreate, 
        Activo 
        from curso where fk_Creador = SP_ID_Usuario and Activo = 1;
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getlevelmultimedia` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `getlevelmultimedia`(
	opc				varchar(3),
	sp_ID_Nivel int(11)
)
begin 
	/*files*/
	if opc="f" then
		select ID_media_files,file_,fk_id_level,tipo,filename from media_files where  fk_id_level=sp_ID_Nivel and tipo!="application/pdf";
    end if;
    
	/*pdfs*/
    if opc="pdf" then
		select ID_media_files,file_,fk_id_level,tipo,filename from media_files where fk_id_level=sp_ID_Nivel and tipo="application/pdf";
    end if;
    
	/*links*/
	if opc="url" then
		select ID_Medialinks,Titulo,Link from media  where fk_id_level=sp_ID_Nivel;
    end if;
    
        
    /*image*/
	if opc="img" then
		select ID_media_image,image,descrip from media_image where fk_id_level=sp_ID_Nivel;
    end if;
    
    /*text*/
	if opc="txt" then
		select ID_media_text,Titulo,Descripcion from media_text where fk_id_level=sp_ID_Nivel;
    end if;
    
   end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_Busqueda` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `SP_Busqueda`(
	op 			varchar(10),
	busqueda  	varchar(100),
    categoria 	varchar(50),
    usuario 	varchar(50),
    fechaI 		date,
    fechaF 		date
)
begin
	
	if op = "search" then
		select
			C.ID_Curso,
			C.Titulo,
			C.Descripcion_C,
			C.Portada,
            C.Costo,
            concat(U.Nombre, " ", U.Apellido) as "Usuario"
		from curso C
        inner join usuario U
        on U.ID_Usuario = C.fk_Creador
		where if(busqueda Is null, 1, Titulo like concat('%',busqueda,'%')) and C.Activo = 1;
	end if;
    
    if op = "avanzada" then
		select 
			C.ID_Curso,
			C.Titulo,
			C.Descripcion_C,
			C.Portada,
            C.Costo,
            concat(U.Nombre, " ", U.Apellido) as "Usuario"
		from curso C
        inner join categoria_curso CC
        on CC.fk_Curso = C.ID_Curso
        inner join usuario U
        on U.ID_Usuario = C.fk_Creador
        
		where if(busqueda Is null, 1, C.Titulo like concat('%',busqueda,'%')) 
        and if(IFNULL(categoria, -1) = -1, 1, CC.categoria_name = categoria)
        and if(IFNULL(usuario, -1) = -1, 1, U.ID_Usuario = usuario)
        and if(fechaI is null, 1, date(C.FechaCreate) >= fechaI)
        and if(fechaF is null, 1, date(C.FechaCreate) <= fechaF)
        and C.Activo = 1
        group by C.ID_Curso;
    end if;
    
    if op = 'category' then         
        select ID_Curso, Titulo, Descripcion_C, Portada, Costo, Usuario, categoria_name from v_coursecategory where categoria_name = categoria;
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_buycourse` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_buycourse`(
sp_ID_Usuario varchar(30),
sp_ID_Curso int(11),
sp_Forma_pago int(11)
)
begin 
	insert into alumn_course(fk_Usuario,fk_curso) values (sp_ID_Usuario, sp_ID_Curso);
	update alumn_course_info
		set Forma_pago=sp_Forma_pago
	where ID_Informacion=(select fk_Informacion from alumn_course where fk_Usuario=sp_ID_Usuario and fk_curso=sp_ID_Curso limit 1);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_createcourse` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_createcourse`(
	opc varchar(3),
    sp_Titulo varchar(50),
    sp_Descripcion_C varchar(100),
    sp_Descripcion_L varchar(300),
    sp_Costo decimal(6,2),
    sp_Portada mediumblob,
    sp_Video_Presentacion mediumblob,
    sp_fk_Creador varchar(30)
)
begin
	if opc='1' then
		insert into curso (Titulo,Descripcion_C,Descripcion_L,Costo,Portada,Video_Presentacion,fk_Creador)values(sp_Titulo,sp_Descripcion_C,sp_Descripcion_L,sp_Costo,sp_Portada,sp_Video_Presentacion,sp_fk_Creador);
	end if;
    
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_createlevel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_createlevel`(
	opc varchar(3),
    sp_Nombre varchar(50),
    sp_Video mediumblob
)
begin
	if opc='1' then
		insert into nivel(Nombre,Video,fk_Curso) values (sp_Nombre,sp_Video, (select max(ID_Curso) from curso));
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_createmedia` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_createmedia`(
	opc varchar(3),
    sp_Titulo varchar(50),
    sp_Link varchar(200)
)
begin
	if opc='1' then
		insert into media(Titulo,Link,fk_id_level) values(sp_Titulo,sp_Link,(select max(ID_Nivel) from nivel));
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_createmedia_files` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_createmedia_files`(
	opc varchar(3),
    sp_file_ mediumblob,
    sp_tipo varchar(30),
    sp_filename varchar(150)
)
begin
	if opc='1' then
		insert into media_files(file_, fk_id_level,tipo,filename) values(sp_file_,(select max(ID_Nivel) from nivel), sp_tipo,sp_filename);
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_createreview` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_createreview`(
sp_review varchar(200),
sp_score tinyint(1),
sp_fk_Usuario varchar(30),
sp_fk_course int(11)
)
begin
	insert into review(review,score,date_r,fk_Usuario,fk_course) values(sp_review,sp_score,now(),sp_fk_Usuario,sp_fk_course);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_cursocategoria` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_cursocategoria`(
	opcion varchar(10),
	sp_categoria varchar(30)
)
begin
	if opcion = "insert" then
		insert into categoria_curso(categoria_name,fk_Curso) values (sp_categoria, (select max(ID_Curso) from curso));
	end if;
    
    if opcion = "show" then
		select distinct
			categoria_name
		from categoria_curso;
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_DetallesC` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `SP_DetallesC`(
	sp_curso int
)
begin
	select Titulo, Nombre, dia, mes, yearI as 'year', Costo, Nivel, Forma_pago, ID_Curso from  v_detallesventasc 
	where ID_Curso = sp_curso;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_diploma` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_diploma`(
	sp_usuario varchar(30),
    sp_curso int
)
begin
	select 
		concat(U.Nombre, ' ', U.Apellido) as 'Nombre',
        C.Titulo,
        (select concat(Nombre, ' ', Apellido) from usuario where ID_Usuario = C.fk_Creador) as 'Creador',
        DATE_FORMAT(I.Fecha_graduacion, '%d  %M  %Y' ) AS 'Fecha_graduacion'
	from alumn_course A
    inner join alumn_course_info I
    on I.ID_Informacion = fk_Informacion
    inner join  usuario U
    on U.ID_Usuario = A.fk_Usuario
    inner join curso C
    on C.ID_Curso = A.fk_curso
    where A.fk_Usuario = sp_usuario and A.fk_curso = sp_curso;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_editcurso` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_editcurso`(
uservalidate varchar(30),
sp_ID_Curso int(11),
sp_Titulo varchar(50),
sp_Descripcion_C varchar(100),
sp_Descripcion_L varchar(300),
sp_Costo decimal(6,2),
sp_Portada mediumblob,
sp_Video_Presentacion mediumblob
)
BEGIN
update curso
	set
        Titulo=sp_Titulo, 
		Descripcion_C=sp_Descripcion_C, 
		Descripcion_L=sp_Descripcion_L, 
		Costo=sp_Costo, 
		Portada=sp_Portada, 
		Video_Presentacion=sp_Video_Presentacion
	where fk_Creador=uservalidate and ID_Curso=sp_ID_Curso;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_editlvl` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_editlvl`(
uservalidate varchar(30),
sp_ID_Nivel int(11),
sp_Nombre varchar(50),
sp_Video mediumblob)
BEGIN
	if((select fk_Creador from curso where ID_Curso in(select fk_Curso from nivel where ID_Nivel=sp_ID_Nivel))=uservalidate) then
		update nivel
			set
				Nombre=sp_Nombre,
				Video=sp_Video
				where ID_Nivel=sp_ID_Nivel;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_HasReviewed` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_HasReviewed`(
sp_fk_Usuario varchar(30),
sp_fk_course int(11)
)
begin 
	select HasReviewed(sp_fk_Usuario,sp_fk_course) as result;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_Historial` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `SP_Historial`(
	sp_usuario varchar(30)
)
begin
	select 
		C.ID_Curso,
		C.Titulo,
		C.Costo * (select count(Id_alumncourse) from alumn_course where fk_curso = C.ID_Curso) as 'Ganancias',
		(select count(Id_alumncourse) from alumn_course where fk_curso = C.ID_Curso) as 'CantUsuarios',
		Truncate(((select Progreso from V_ProgresoC where ID_Curso = C.ID_Curso limit 1)/ProgresoC(C.ID_Curso)),0) as 'NivelProm'
	from curso C
	where C.fk_Creador = sp_usuario;
 end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_Inicio` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `SP_Inicio`(
	opc varchar(12)
)
begin
	if opc = "recientes" then
        select ID_Curso, Titulo, Descripcion_C, Portada, Costo, Usuario from v_coursesr  ORDER BY ID_Curso DESC limit 5;
    end if;
    
    if opc = "vendidos" then    
        select ID_Curso, Titulo, Descripcion_C, Portada, Costo, Usuario, Cant_Usuarios from v_coursesv ORDER BY Cant_Usuarios DESC limit 5;
	end if;
    
    if opc = "calificados" then
		select ID_Curso, Titulo, Descripcion_C, Portada, Costo, Usuario, Calificacion from v_coursesc ORDER BY Calificacion DESC limit 5;
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_media_image` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_media_image`(
	opc varchar(3),
    sp_image blob,
    sp_descrip varchar(150)    
)
begin
	if opc='1' then
		insert into media_image(image, fk_id_level,descrip) values(sp_image,(select max(ID_Nivel) from nivel),sp_descrip);
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_media_text` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_media_text`(
	opc varchar(3),
	sp_Titulo varchar(50),
    sp_Descripcion varchar(600)
)
begin
	if opc='1' then
		insert into media_text(Titulo, Descripcion, fk_id_level) values(sp_Titulo,sp_Descripcion,(select max(ID_Nivel) from nivel));
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_Mensajes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `SP_Mensajes`(
	opc varchar(20),
	sp_message varchar(600),
    sp_remitente varchar(30),
    sp_destiny varchar(30)
)
begin
	if opc = "insert" then
		insert message(message, remitente, destiny) values(sp_message, sp_remitente, sp_destiny);
    end if;
    
    if opc = "msj" then
		Update message
			set seen = 1
		where (remitente = sp_remitente and destiny = sp_destiny ) and seen = 0;
         
		select message, fecha, seen, destiny, remitente 
        from message
        where (remitente = sp_remitente and destiny = sp_destiny) or (remitente = sp_destiny and destiny = sp_remitente)
        Order by fecha asc;
    end if;

	if opc = "preview" then
		select distinct
			(select message from message where fecha = (select max(fecha) from message where (remitente = M.remitente and destiny =  M.destiny) or (remitente =  M.destiny and destiny = M.remitente))) as 'message',
			(select max(fecha) from message where (remitente = M.remitente and destiny =  M.destiny) or (remitente =  M.destiny and destiny = M.remitente)) as 'fecha',
			if(M.destiny = sp_remitente, (select concat(Nombre, ' ', Apellido) from usuario where ID_Usuario = M.remitente), (select concat(Nombre, ' ', Apellido) from usuario where ID_Usuario = M.destiny)) as 'Nombre', 
            if(M.destiny = sp_remitente, (select foto from usuario where ID_Usuario = M.remitente), (select foto from usuario where ID_Usuario = M.destiny)) as 'foto', 
            if(M.destiny = sp_remitente, (select ID_Usuario from usuario where ID_Usuario = M.remitente), (select ID_Usuario from usuario where ID_Usuario = M.destiny)) as 'destiny', 
			M.remitente,
			if (M.remitente = sp_remitente, (select countMsjseen(M.destiny, M.remitente)), (select countMsjseen(M.remitente, M.destiny))) as 'countMsjSeen',
            (select countMsj(M.remitente, M.destiny)) as 'countMsj'
		from message M
		where M.remitente = sp_remitente or M.destiny = sp_remitente
        Order by fecha desc;
    end if;
    
    if opc = "ultimo" then 
    set @id = (select ultimoMsj(sp_remitente));
		select remitente,
			destiny,
			fecha
		from message
        where ID_message = @id;
    end if;
    
    if opc = "seen" then
		select count(seen) as 'msj' from message where destiny = sp_remitente and seen = 0;
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_Progreso` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_Progreso`(
	sp_nivel int,
    sp_curso int,
    sp_usuario varchar(30)
)
begin
		set @fk_info = (select fk_Informacion from alumn_course where fk_curso = sp_curso and  fk_Usuario = sp_usuario);
        
		update alumn_course_info 
		set Progreso = if(Progreso >= (sp_nivel * ProgresoC(sp_curso)), Progreso, if(Progreso = 100, 100, Progreso + ProgresoC(sp_curso))),
			Fecha_graduacion = if(Progreso = 100, now(), null)
		where ID_Informacion =  @fk_info;
        
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_suspenderCurso` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_suspenderCurso`(
	sp_id_curso int
)
begin
	update curso
	set Activo = 0
	where ID_Curso = sp_id_curso;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UpdateAlumnCourseInfo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `sp_UpdateAlumnCourseInfo`(
	op varchar(10),
	sp_curso int,
    sp_usuario varchar(30)
)
begin
    if op = "UVisita" then
		set @fk_info = (select fk_Informacion from alumn_course where fk_curso = sp_curso and  fk_Usuario = sp_usuario);
        
        update alumn_course_info
        set Ultima_visita = now()
        where ID_Informacion = @fk_info;
    end if;
    
    if op = "Graduacion" then
		set @fk_info = (select fk_Informacion from alumn_course where fk_curso = sp_curso and  fk_Usuario = sp_usuario);
        
        update alumn_course_info
        set Fecha_graduacion = now()
        where ID_Informacion = @fk_info;
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_Usuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `SP_Usuario`(
	Opcion				varchar(10),
	spID_Usuario 		varchar(30),
    spNombre 			varchar(30),
    spApellido 			varchar(30),
    spFecha_Nacimiento 	date,
    spEmail 			varchar(50),
    spPass 				varchar(8),
    spFoto			 	mediumblob,
    spfk_rol 			int,
	spfkGenero 			int
)
begin
	If opcion = "Register" then
		Insert into usuario(ID_Usuario, Nombre, Apellido, Fecha_Nacimiento, Email, Pass, fk_rol, fk_genero) 
        values(spID_Usuario,spNombre,spApellido,spFecha_Nacimiento,spEmail,spPass,spfk_rol, spfkGenero);
    end if;
    
    If opcion = "GetUser" then 
		Select
			ID_Usuario,
            Nombre,
            Apellido,
            Fecha_Nacimiento,
            Email,
            Pass,
            Foto,
            Activo,
            fk_rol,
            fk_genero
		from usuario
        where ID_Usuario = spID_Usuario;
    end if;
    
     If opcion = "EditUser" then 
		Update usuario
        set
				Nombre = spNombre,
				Apellido = spApellido,
				Fecha_Nacimiento = spFecha_Nacimiento,
				Pass = spPass,
				Foto = spFoto,
                Fecha_Mod = now(),
				fk_genero = spfkGenero
        where ID_Usuario = spID_Usuario;
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_login` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=CURRENT_USER PROCEDURE `user_login`(
	SP_ID_Usuario varchar(30),
    SP_pass varchar(8)
)
begin 
	select case (select count(pass) from usuario where ID_Usuario=SP_ID_Usuario and pass=SP_pass) 
		when 1 then 1 
		else 0
	end as validate; 
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `reviewsview`
--

/*!50001 DROP VIEW IF EXISTS `reviewsview`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=CURRENT_USER SQL SECURITY DEFINER */
/*!50001 VIEW `reviewsview` AS select `r`.`ID_review` AS `ID_review`,`r`.`review` AS `review`,`r`.`score` AS `score`,`r`.`date_r` AS `date_r`,`r`.`fk_Usuario` AS `fk_Usuario`,`r`.`fk_course` AS `fk_course`,concat(`u`.`Nombre`,' ',`u`.`Apellido`) AS `Nombre`,`u`.`Foto` AS `Foto` from (`review` `r` join `usuario` `u` on((`r`.`fk_Usuario` = `u`.`ID_Usuario`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_coursecategory`
--

/*!50001 DROP VIEW IF EXISTS `v_coursecategory`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=CURRENT_USER SQL SECURITY DEFINER */
/*!50001 VIEW `v_coursecategory` AS select `c`.`ID_Curso` AS `ID_Curso`,`c`.`Titulo` AS `Titulo`,`c`.`Descripcion_C` AS `Descripcion_C`,`c`.`Portada` AS `Portada`,`c`.`Costo` AS `Costo`,concat(`u`.`Nombre`,' ',`u`.`Apellido`) AS `Usuario`,`cc`.`categoria_name` AS `categoria_name` from ((`curso` `c` join `categoria_curso` `cc` on((`cc`.`fk_Curso` = `c`.`ID_Curso`))) join `usuario` `u` on((`u`.`ID_Usuario` = `c`.`fk_Creador`))) where (`c`.`Activo` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_coursesc`
--

/*!50001 DROP VIEW IF EXISTS `v_coursesc`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=CURRENT_USER SQL SECURITY DEFINER */
/*!50001 VIEW `v_coursesc` AS select `c`.`ID_Curso` AS `ID_Curso`,`c`.`Titulo` AS `Titulo`,`c`.`Descripcion_C` AS `Descripcion_C`,`c`.`Portada` AS `Portada`,`c`.`Costo` AS `Costo`,concat(`u`.`Nombre`,' ',`u`.`Apellido`) AS `Usuario`,(select count(`review`.`ID_review`) from `review` where ((`review`.`fk_course` = `c`.`ID_Curso`) and (`review`.`score` = 1))) AS `Calificacion` from ((`curso` `c` join `usuario` `u` on((`u`.`ID_Usuario` = `c`.`fk_Creador`))) join `review` `r` on((`r`.`fk_course` = `c`.`ID_Curso`))) where (`c`.`Activo` = 1) group by `c`.`ID_Curso` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_coursesr`
--

/*!50001 DROP VIEW IF EXISTS `v_coursesr`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=CURRENT_USER SQL SECURITY DEFINER */
/*!50001 VIEW `v_coursesr` AS select `c`.`ID_Curso` AS `ID_Curso`,`c`.`Titulo` AS `Titulo`,`c`.`Descripcion_C` AS `Descripcion_C`,`c`.`Portada` AS `Portada`,`c`.`Costo` AS `Costo`,concat(`u`.`Nombre`,' ',`u`.`Apellido`) AS `Usuario` from (`curso` `c` join `usuario` `u` on((`u`.`ID_Usuario` = `c`.`fk_Creador`))) where ((`c`.`Activo` = 1) and (`c`.`fechaCreate` <= now())) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_coursesv`
--

/*!50001 DROP VIEW IF EXISTS `v_coursesv`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=CURRENT_USER SQL SECURITY DEFINER */
/*!50001 VIEW `v_coursesv` AS select `c`.`ID_Curso` AS `ID_Curso`,`c`.`Titulo` AS `Titulo`,`c`.`Descripcion_C` AS `Descripcion_C`,`c`.`Portada` AS `Portada`,`c`.`Costo` AS `Costo`,concat(`u`.`Nombre`,' ',`u`.`Apellido`) AS `Usuario`,(select count(`alumn_course`.`Id_alumncourse`) from `alumn_course` where (`alumn_course`.`fk_curso` = `c`.`ID_Curso`)) AS `Cant_Usuarios` from ((`curso` `c` join `usuario` `u` on((`u`.`ID_Usuario` = `c`.`fk_Creador`))) join `alumn_course` `ac` on((`ac`.`fk_curso` = `c`.`ID_Curso`))) where (`c`.`Activo` = 1) group by `c`.`ID_Curso` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_detallesventasc`
--

/*!50001 DROP VIEW IF EXISTS `v_detallesventasc`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=CURRENT_USER SQL SECURITY DEFINER */
/*!50001 VIEW `v_detallesventasc` AS select `c`.`Titulo` AS `Titulo`,concat(`u`.`Nombre`,' ',`u`.`Apellido`) AS `Nombre`,date_format(`i`.`Fecha_Inscripcion`,'%d') AS `dia`,date_format(`i`.`Fecha_Inscripcion`,'%M') AS `mes`,date_format(`i`.`Fecha_Inscripcion`,'%Y') AS `yearI`,`c`.`Costo` AS `Costo`,`ProgresoporNivel`(`i`.`Progreso`,`c`.`ID_Curso`) AS `Nivel`,`i`.`Progreso` AS `Progreso`,`c`.`ID_Curso` AS `ID_Curso`,`i`.`Forma_pago` AS `Forma_pago` from (((`alumn_course` `a` join `curso` `c` on((`c`.`ID_Curso` = `a`.`fk_curso`))) join `alumn_course_info` `i` on((`i`.`ID_Informacion` = `a`.`fk_Informacion`))) join `usuario` `u` on((`u`.`ID_Usuario` = `a`.`fk_Usuario`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_progresoc`
--

/*!50001 DROP VIEW IF EXISTS `v_progresoc`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=CURRENT_USER SQL SECURITY DEFINER */
/*!50001 VIEW `v_progresoc` AS select `i`.`Progreso` AS `Progreso`,(select count(`ai`.`Progreso`) from (`alumn_course_info` `ai` join `alumn_course` `ac` on((`ai`.`ID_Informacion` = `ac`.`fk_Informacion`))) where ((`ai`.`Progreso` = `i`.`Progreso`) and (`ac`.`fk_curso` = `a`.`fk_curso`))) AS `cant`,`c`.`ID_Curso` AS `ID_Curso` from ((`alumn_course` `a` join `curso` `c` on((`c`.`ID_Curso` = `a`.`fk_curso`))) join `alumn_course_info` `i` on(((`i`.`ID_Informacion` = `a`.`fk_Informacion`) and (`i`.`Progreso` > 0)))) order by (select count(`ai`.`Progreso`) from (`alumn_course_info` `ai` join `alumn_course` `ac` on((`ai`.`ID_Informacion` = `ac`.`fk_Informacion`))) where ((`ai`.`Progreso` = `i`.`Progreso`) and (`ac`.`fk_curso` = `a`.`fk_curso`))) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-26 19:00:10
