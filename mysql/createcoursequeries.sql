DELIMITER $$
drop procedure if exists sp_createcourse;$$
create procedure sp_createcourse(
	opc varchar(3),
    sp_Titulo varchar(50),
    sp_Descripcion_C varchar(100),
    sp_Descripcion_L varchar(300),
    sp_Costo decimal(6,2),
    sp_Portada mediumblob,
    sp_Video_Presentacion mediumblob,
    sp_fk_Creador varchar(30)
)
begin
	if opc='1' then
		insert into curso (Titulo,Descripcion_C,Descripcion_L,Costo,Portada,Video_Presentacion,fk_Creador)values(sp_Titulo,sp_Descripcion_C,sp_Descripcion_L,sp_Costo,sp_Portada,sp_Video_Presentacion,sp_fk_Creador);
	end if;
    
end;$$

delimiter $$
drop procedure if exists sp_cursocategoria;
create procedure sp_cursocategoria(
	opcion varchar(10),
	sp_categoria varchar(30)
)
begin
	if opcion = "insert" then
		insert into categoria_curso(categoria_name,fk_Curso) values (sp_categoria, (select max(ID_Curso) from curso));
	end if;
    
    if opcion = "show" then
		select distinct
			categoria_name
		from categoria_curso;
    end if;
end;$$

DELIMITER $$
drop procedure if exists sp_createlevel;$$
create procedure sp_createlevel(
	opc varchar(3),
    sp_Nombre varchar(50),
    sp_Video mediumblob
)
begin
	if opc='1' then
		insert into nivel(Nombre,Video,fk_Curso) values (sp_Nombre,sp_Video, (select max(ID_Curso) from curso));
    end if;
end;$$

DELIMITER $$
drop procedure if exists sp_createmedia;$$
create procedure sp_createmedia(
	opc varchar(3),
    sp_Titulo varchar(50),
    sp_Link varchar(200)
)
begin
	if opc='1' then
		insert into media(Titulo,Link,fk_id_level) values(sp_Titulo,sp_Link,(select max(ID_Nivel) from nivel));
    end if;
end;$$


DELIMITER $$
drop procedure if exists sp_createmedia_files;$$
create procedure sp_createmedia_files(
	opc varchar(3),
    sp_file_ mediumblob,
    sp_tipo varchar(30),
    sp_filename varchar(150)
)
begin
	if opc='1' then
		insert into media_files(file_, fk_id_level,tipo,filename) values(sp_file_,(select max(ID_Nivel) from nivel), sp_tipo,sp_filename);
    end if;
end;$$

DELIMITER $$
drop procedure if exists sp_media_image;$$
create procedure sp_media_image(
	opc varchar(3),
    sp_image blob,
    sp_descrip varchar(150)    
)
begin
	if opc='1' then
		insert into media_image(image, fk_id_level,descrip) values(sp_image,(select max(ID_Nivel) from nivel),sp_descrip);
    end if;
end;$$

DELIMITER $$
drop procedure if exists sp_media_text;$$
create procedure sp_media_text(
	opc varchar(3),
	sp_Titulo varchar(50),
    sp_Descripcion varchar(600)
)
begin
	if opc='1' then
		insert into media_text(Titulo, Descripcion, fk_id_level) values(sp_Titulo,sp_Descripcion,(select max(ID_Nivel) from nivel));
    end if;
end;$$

