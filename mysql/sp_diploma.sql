DELIMITER $$
drop procedure if exists sp_diploma;$$
create procedure sp_diploma(
	sp_usuario varchar(30),
    sp_curso int
)
begin
	select 
		concat(U.Nombre, ' ', U.Apellido) as 'Nombre',
        C.Titulo,
        (select concat(Nombre, ' ', Apellido) from usuario where ID_Usuario = C.fk_Creador) as 'Creador',
        DATE_FORMAT(I.Fecha_graduacion, '%d  %M  %Y' ) AS 'Fecha_graduacion'
	from alumn_course A
    inner join alumn_course_info I
    on I.ID_Informacion = fk_Informacion
    inner join  usuario U
    on U.ID_Usuario = A.fk_Usuario
    inner join curso C
    on C.ID_Curso = A.fk_curso
    where A.fk_Usuario = sp_usuario and A.fk_curso = sp_curso;
end$$