Delimiter $$
drop procedure if exists SP_Historial;$$
create procedure SP_Historial(
	sp_usuario varchar(30)
)begin
	select 
		C.ID_Curso,
		C.Titulo,
		C.Costo * (select count(Id_alumncourse) from alumn_course where fk_curso = C.ID_Curso) as 'Ganancias',
		(select count(Id_alumncourse) from alumn_course where fk_curso = C.ID_Curso) as 'CantUsuarios',
		Truncate(((select Progreso from V_ProgresoC where ID_Curso = C.ID_Curso limit 1)/ProgresoC(C.ID_Curso)),0) as 'NivelProm'
	from curso C
	where C.fk_Creador = sp_usuario;
 end$$
 
DELIMITER $$
drop function if exists ProgresoC;$$
Create function ProgresoC(sp_curso int) 
Returns float deterministic
begin 
	declare progreso  float;
    set progreso = (select 100/(select count(ID_Nivel) from Nivel where fk_Curso = sp_curso));
	return progreso;
end;$$

select Progreso, cant, ID_Curso from V_ProgresoC where ID_Curso = 79

drop view if exists V_ProgresoC;
create view V_ProgresoC
as
select 
	I.Progreso, 
	(select count(Progreso) from alumn_course_info AI inner join alumn_course AC on AI.ID_Informacion = AC.fk_Informacion where Progreso = I.Progreso and AC.fk_curso = A.fk_curso) as 'cant',
    C.ID_Curso
from  alumn_course A
inner join curso C
on C.ID_Curso = A.fk_curso
inner join alumn_course_info I
on I.ID_Informacion = A.fk_Informacion and I.Progreso > 0
Order by cant desc;
                    

DELIMITER $$
drop procedure if exists SP_DetallesC;$$
create procedure SP_DetallesC(
	sp_curso int
)
begin
	select Titulo, Nombre, dia, mes, yearI as 'year', Costo, Nivel, Forma_pago, ID_Curso from  V_DetallesVentasC 
	where ID_Curso = sp_curso;
end$$

drop view if exists V_DetallesVentasC;
create view V_DetallesVentasC
as
select
	C.Titulo,
	concat(U.Nombre, ' ', U.Apellido) as 'Nombre',
	Date_Format(I.Fecha_Inscripcion, "%d") as 'dia',
	Date_Format(I.Fecha_Inscripcion, "%M") as 'mes',
	Date_Format(I.Fecha_Inscripcion, "%Y") as 'yearI',
	C.Costo,
	ProgresoporNivel(I.Progreso, C.ID_Curso) as "Nivel",
	I.Progreso,
	C.ID_Curso,
	I.Forma_pago
from alumn_course A
inner join curso C
on C.ID_Curso = A.fk_curso
inner join alumn_course_info I
on I.ID_Informacion = A.fk_Informacion
inner join usuario U
on U.ID_Usuario = A.fk_Usuario;

DELIMITER $$
drop function if exists ProgresoporNivel;$$
Create function ProgresoporNivel(sp_progreso float, sp_curso int) 
Returns float deterministic
begin 
	declare progreso  float;
    set progreso = (Truncate(sp_progreso/ (100/(select count(ID_Nivel) from Nivel where fk_Curso = sp_curso)), 0));
	return progreso;
end;$$