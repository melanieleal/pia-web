DELIMITER $$
CREATE TRIGGER createinfoinscripcion
before INSERT ON alumn_course FOR EACH ROW
BEGIN
  insert into alumn_course_info(Fecha_Inscripcion, Progreso, Ultima_visita,Forma_pago, Fecha_graduacion) values(now(), 0, now(), null, null);
	set New.fk_Informacion = (select max(ID_Informacion) from alumn_course_info);
END;$$


DELIMITER $$
DROP PROCEDURE IF EXISTS sp_buycourse;$$
CREATE PROCEDURE sp_buycourse(
sp_ID_Usuario varchar(30),
sp_ID_Curso int(11),
sp_Forma_pago int(11)
)
begin 
	insert into alumn_course(fk_Usuario,fk_curso) values (sp_ID_Usuario, sp_ID_Curso);
	update alumn_course_info
		set Forma_pago=sp_Forma_pago
	where ID_Informacion=(select fk_Informacion from alumn_course where fk_Usuario=sp_ID_Usuario and fk_curso=sp_ID_Curso limit 1);
end;$$


DELIMITER $$
DROP PROCEDURE IF EXISTS sp_createreview;$$
CREATE PROCEDURE sp_createreview(
sp_review varchar(200),
sp_score tinyint(1),
sp_fk_Usuario varchar(30),
sp_fk_course int(11)
)
begin
	insert into review(review,score,date_r,fk_Usuario,fk_course) values(sp_review,sp_score,now(),sp_fk_Usuario,sp_fk_course);
end;

DELIMITER $$
DROP FUNCTION IF EXISTS HasReviewed;
CREATE FUNCTION HasReviewed (usuario varchar(30), curso int(11)) 
RETURNS INT deterministic
BEGIN 
 DECLARE booleano INT;
 SET booleano = (select count(ID_review) from review where fk_Usuario=usuario and fk_course=curso);
 RETURN (booleano);
END;$$

DELIMITER $$
DROP PROCEDURE IF EXISTS sp_HasReviewed;$$
create procedure sp_HasReviewed(
sp_fk_Usuario varchar(30),
sp_fk_course int(11)
)
begin 
	select HasReviewed(sp_fk_Usuario,sp_fk_course) as result, Progreso from alumn_course_info where ID_Informacion in (select fk_Informacion from alumn_course where fk_Usuario=sp_fk_Usuario and fk_curso=sp_fk_course);
end;
