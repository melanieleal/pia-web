Delimiter $$
drop procedure if exists SP_Mensajes;$$
create procedure SP_Mensajes(
	opc varchar(20),
	sp_message varchar(600),
    sp_remitente varchar(30),
    sp_destiny varchar(30)
)
begin
	if opc = "insert" then
		insert message(message, remitente, destiny) values(sp_message, sp_remitente, sp_destiny);
    end if;
    
    if opc = "msj" then
		Update message
			set seen = 1
		where (remitente = sp_remitente and destiny = sp_destiny ) and seen = 0;
         
		select message, fecha, seen, destiny, remitente 
        from message
        where (remitente = sp_remitente and destiny = sp_destiny) or (remitente = sp_destiny and destiny = sp_remitente)
        Order by fecha asc;
    end if;

	if opc = "preview" then
		select distinct
			(select message from message where fecha = (select max(fecha) from message where (remitente = M.remitente and destiny =  M.destiny) or (remitente =  M.destiny and destiny = M.remitente))) as 'message',
			(select max(fecha) from message where (remitente = M.remitente and destiny =  M.destiny) or (remitente =  M.destiny and destiny = M.remitente)) as 'fecha',
			if(M.destiny = sp_remitente, (select concat(Nombre, ' ', Apellido) from Usuario where ID_Usuario = M.remitente), (select concat(Nombre, ' ', Apellido) from Usuario where ID_Usuario = M.destiny)) as 'Nombre', 
            if(M.destiny = sp_remitente, (select foto from Usuario where ID_Usuario = M.remitente), (select foto from Usuario where ID_Usuario = M.destiny)) as 'foto', 
            if(M.destiny = sp_remitente, (select ID_Usuario from Usuario where ID_Usuario = M.remitente), (select ID_Usuario from Usuario where ID_Usuario = M.destiny)) as 'destiny', 
			M.remitente,
			if (M.remitente = sp_remitente, (select countMsjseen(M.destiny, M.remitente)), (select countMsjseen(M.remitente, M.destiny))) as 'countMsjSeen',
            (select countMsj(M.remitente, M.destiny)) as 'countMsj'
		from message M
		where M.remitente = sp_remitente or M.destiny = sp_remitente
        Order by fecha desc;
    end if;
    
    if opc = "ultimo" then 
    set @id = (select ultimoMsj(sp_remitente));
		select remitente,
			destiny,
			fecha
		from message
        where ID_message = @id;
    end if;
    
    if opc = "seen" then
		select count(seen) as 'msj' from message where destiny = sp_remitente and seen = 0;
    end if;
end$$

DELIMITER $$
drop function if exists countMsjseen;$$
Create function countMsjseen(sp_remitente varchar(30), sp_destiny varchar(30)) 
Returns int deterministic
begin 
	declare countS int;
	set countS = (select count(seen) from message where (remitente = sp_remitente and destiny = sp_destiny) and seen = 0);
	return countS;
end;$$

DELIMITER $$
drop function if exists ultimoMsj;$$
Create function ultimoMsj(sp_remitente varchar(30)) 
Returns int deterministic
begin 

	declare fechaU datetime;
	declare id  int;
    set fechaU = (select max(fecha) from message where remitente = sp_remitente or destiny = sp_remitente);
	set id = (select id_message from message where fecha = fechaU);
	return id;
end;$$

DELIMITER $$
drop function if exists countMsj;$$
Create function countMsj(sp_remitente varchar(30), sp_destiny varchar(30)) 
Returns int deterministic
begin 
	declare countM  int;
    set countM = (select count(ID_message) from message where  (remitente = sp_remitente and destiny = sp_destiny));
	return countM;
end;$$

