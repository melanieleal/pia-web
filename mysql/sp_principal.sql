Delimiter $$
drop procedure if exists SP_Inicio;
create procedure SP_Inicio(
	opc varchar(12)
)
begin
	if opc = "recientes" then
        select ID_Curso, Titulo, Descripcion_C, Portada, Costo, Usuario from V_coursesR  ORDER BY ID_Curso DESC limit 5;
    end if;
    
    if opc = "vendidos" then    
        select ID_Curso, Titulo, Descripcion_C, Portada, Costo, Usuario, Cant_Usuarios from V_coursesV ORDER BY Cant_Usuarios DESC limit 5;
	end if;
    
    if opc = "calificados" then
		select ID_Curso, Titulo, Descripcion_C, Portada, Costo, Usuario, Calificacion from V_coursesC ORDER BY Calificacion DESC limit 5;
    end if;
end$$

drop view if exists V_coursesR;
create view V_coursesR
as
select
	C.ID_Curso,
	C.Titulo,
	C.Descripcion_C,
	C.Portada,
	C.Costo,
	concat(U.Nombre, " ", U.Apellido) as "Usuario"
from curso C
inner join usuario U
on U.ID_Usuario = C.fk_Creador
where C.Activo = 1 and C.fechaCreate <= now();

drop view if exists V_coursesV;
create view V_coursesV
as 
select
	C.ID_Curso,
	C.Titulo,
	C.Descripcion_C,
	C.Portada,
	C.Costo,
	concat(U.Nombre, " ", U.Apellido) as "Usuario",
	(select count(Id_alumncourse) from alumn_course where fk_Curso = C.ID_Curso) as "Cant_Usuarios"
from curso C
inner join usuario U
on U.ID_Usuario = C.fk_Creador
inner join alumn_course AC
on AC.fk_curso = C.ID_Curso
where C.Activo = 1
GROUP BY C.ID_Curso;

drop view if exists V_coursesC;
create view V_coursesC
as
select
	C.ID_Curso,
	C.Titulo,
	C.Descripcion_C,
	C.Portada,
	C.Costo,
	concat(U.Nombre, " ", U.Apellido) as "Usuario",
	(select count(ID_review) from review where fk_course = C.ID_Curso and score = 1) as "Calificacion"
from curso C
inner join usuario U
on U.ID_Usuario = C.fk_Creador
inner join review R
on R.fk_course = C.ID_Curso
where C.Activo = 1
GROUP BY C.ID_Curso;
        